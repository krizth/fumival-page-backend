<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::prefix('user')->middleware(['auth'])->group(function () {
    Route::get('/list','UserController@index')->name('read-users');
    Route::post('/delete','UserController@delete')->name('delete-users');
    Route::post('/update','UserController@update')->name('update-users');
    Route::post('/store',"UserController@store")->name('write-users');
    Route::get('/create',"UserController@create")->name('create-users');
    Route::get('/list/all', 'UserController@allUsers');
    Route::get('/search','UserController@find');
    Route::post('/notifications/update-token', 'UserController@updateFCMToken');
    Route::get('/notifications/{quantity}', "NotificationsController@index");
    Route::delete('/notifications/{id}', "NotificationsController@destroy");
});

Route::prefix('tipo_cliente')->middleware(["auth"])->group(function (){
    Route::get('/list',"TipoClienteController@index")->name('read-tipo_cliente');
    Route::post('/delete',"TipoClienteController@destroy")->name('delete-tipo_cliente');
    Route::post('/update',"TipoClienteController@update")->name('update-tipo_cliente');
    Route::post('/store',"TipoClienteController@store")->name('write-tipo_cliente');
});

Route::prefix('trampas')->middleware(["auth"])->group(function (){
    Route::get('/list',"TrampasController@index")->name('read-trampas');
    Route::get('/show',"TrampasController@show")->name('read-trampa')->where('id', '[0-9]+');
    Route::post('/delete',"TrampasController@destroy")->name('delete-trampas');
    Route::post('/update',"TrampasController@update")->name('update-trampas');
    Route::post('/store',"TrampasController@store")->name('write-trampas');
});



Route::prefix('roles')->middleware(["auth"])->group(function (){
    Route::get('/list',"RoleController@index")->name('read-roles');
    Route::post('/delete',"RoleController@delete")->name('delete-roles');
    Route::post('/update',"RoleController@update")->name('update-roles');
    Route::get('/create',"RoleController@create")->name('create-roles');
    Route::post('/store',"RoleController@store")->name('write-roles');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('glosa-evaluacion')->middleware(["auth"])->group(function (){
    Route::get('/list',"GlosaEvaluacionController@index")->name('read-glosa_evaluacion');
    Route::post('/delete',"GlosaEvaluacionController@destroy")->name('delete-glosa_evaluacion');
    Route::post('/update',"GlosaEvaluacionController@update")->name('update-glosa_evaluacion');
    Route::get('/create',"GlosaEvaluacionController@create")->name('create-glosa_evaluacion');
    Route::post('/store',"GlosaEvaluacionController@store")->name('write-glosa_evaluacion');

});

Route::prefix('medio_pago')->group(function () {
    Route::get('/', 'MedioPagoController@index')->middleware('auth');
});

Route::get('/mail','CertificadosController@sendCert')->middleware(['auth']);
Route::prefix('conversion')->middleware(["auth"])->group(function (){
    Route::get('/list',"FactorConversionController@index")->name('read-factor_conversion');
    Route::post('/delete',"FactorConversionController@destroy")->name('delete-factor_conversion');
    Route::match(['GET','POST'],'/update',"FactorConversionController@update")->name('update-factor_conversion');
    Route::get('/create',"FactorConversionController@create")->name('create-factor_conversion');
    Route::post('/store',"FactorConversionController@store")->name('write-factor_conversion');
});

Route::prefix('evaluacion')->middleware(["auth"])->group(function (){
    Route::get('/list',"EvaluacionController@index")->name('read-evaluacion');
    Route::post('/delete',"EvaluacionController@destroy")->name('delete-evaluacion');
    Route::post('/update',"EvaluacionController@update")->name('update-evaluacion');
    Route::get('/create',"EvaluacionController@create")->name('create-evaluacion');
    Route::post('/store',"EvaluacionController@store")->name('write-evaluacion');
});

Route::prefix('unidad-medida')->middleware(["auth"])->group(function (){
    Route::get('/list',"UnidadMedidaController@index")->name('read-unidad_medida');
    Route::post('/delete',"UnidadMedidaController@destroy")->name('delete-unidad_medida');
    Route::post('/update',"UnidadMedidaController@update")->name('update-unidad_medida');
    Route::get('/create',"UnidadMedidaController@create")->name('create-unidad_medida');
    Route::post('/store',"UnidadMedidaController@store")->name('write-unidad_medida');
    Route::get('/list?producto={id}', "UnidadMedidaController@index");
});

Route::prefix('producto')->middleware(["auth"])->group(function (){
    Route::post('/show',"ProductoController@show");
    Route::get('/list',"ProductoController@index")->name('read-producto');
    Route::post('/delete',"ProductoController@destroy")->name('delete-producto');
    Route::post('/update',"ProductoController@update")->name('update-producto');
    Route::get('/create',"ProductoController@create")->name('create-producto');
    Route::post('/store',"ProductoController@store")->name('write-producto');
    Route::get('/{id}', "ProductoController@getProducto");
});

Route::prefix('medio-pago')->middleware(["auth"])->group(function (){
    Route::get('/list',"MedioPagoController@index")->name('read-medio_pago');
    Route::post('/delete',"MedioPagoController@destroy")->name('delete-medio_pago');
    Route::post('/update',"MedioPagoController@update")->name('update-medio_pago');
    Route::post('/store',"MedioPagoController@store")->name('write-medio_pago');
});

Route::prefix('tipo-tratamiento')->middleware(["auth"])->group(function (){
    Route::get('/list',"TipoTratamientoController@index")->name('read-tipo_tratamiento');
    Route::post('/delete',"TipoTratamientoController@destroy")->name('delete-tipo_tratamiento');
    Route::post('/update',"TipoTratamientoController@update")->name('update-tipo_tratamiento');
    Route::get('/create',"TipoTratamientoController@create")->name('create-tipo_tratamiento');
    Route::post('/store',"TipoTratamientoController@store")->name('write-tipo_tratamiento');
});

Route::prefix('empresa')->middleware(["auth"])->group(function (){
    Route::get('/list',"EmpresaController@index")->name('read-empresa');
    Route::post('/delete',"EmpresaController@destroy")->name('delete-empresa');
    Route::post('/update',"EmpresaController@update")->name('update-empresa');
    Route::get('/create',"EmpresaController@create")->name('create-empresa');
    Route::post('/store',"EmpresaController@store")->name('write-empresa');
});

Route::prefix('clientes')->middleware(["auth"])->group(function (){
    Route::get('/list',"ClientesController@index")->name('read-clientes');
    Route::post('/delete',"ClientesController@destroy")->name('delete-clientes');
    Route::post('/update',"ClientesController@update")->name('update-clientes');
    Route::get('/create',"ClientesController@create")->name('create-clientes');
    Route::post('/store',"ClientesController@store")->name('write-clientes');
    Route::get('/list/{id}', "ClientesController@show");
    Route::get('/search', "ClientesController@find");
    Route::get('/informe/{id}',"ClientesController@exportExcel");
    Route::get('tipo/{id?}',"ClientesController@tipoCliente");
});

Route::get('images/{filename}',"ImagenesController@index")->middleware(["auth"])->name('images');

Route::prefix('productos')->middleware(["auth"])->group(function (){
    Route::get('/list',"ProductoController@index")->name('read-productos');
    Route::post('/delete',"ProductoController@destroy")->name('delete-productos');
    Route::post('/update',"ProductoController@update")->name('update-productos');
    Route::get('/create',"ProductoController@create")->name('create-productos');
    Route::post('/store',"ProductoController@store")->name('write-productos');
});

Route::prefix('preset')->middleware(["auth"])->group(function () {
    Route::post('/delete', "PresetController@destroy")->name('delete-preset');
    Route::post('/update', "PresetController@update")->name('update-preset');
    Route::get('/create', "PresetController@create")->name('create-preset');
    Route::post('/store', "PresetController@store")->name('write-preset');
    Route::get('/componentes/{id}',"PresetController@getComponentes");
    Route::get('/factores/{id}', "PresetController@getFactores");
});

Route::prefix('stocks')->middleware(["auth"])->group(function (){
    Route::get('/list',"StockController@index")->name('read-stock');
    Route::post('/delete',"StockController@destroy")->name('delete-stock');
    Route::post('/update',"StockController@update")->name('update-stock');
    Route::get('/create',"StockController@create")->name('create-stock');
    Route::post('/store',"StockController@store")->name('write-stock');
    Route::get('/por-expirar',"StockController@porExpirar");
});

Route::prefix('stock_usuarios')->middleware(["auth"])->group(function () {
    Route::get('/list', "StockUsuariosController@index")->name('read-stock_usuarios');
    Route::post('/delete', "StockUsuariosController@destroy")->name('delete-stock_usuarios');
    Route::post('/update', "StockUsuariosController@update")->name('update-stock_usuarios');
    Route::get('/create', "StockUsuariosController@create")->name('create-stock_usuarios');
    Route::post('/store', "StockUsuariosController@store")->name('write-stock_usuarios');
    Route::get('/show', "StockUsuariosController@show");
});

Route::prefix('bodega')->middleware(["auth"])->group(function (){
    Route::get('/list',"BodegaController@index")->name('read-bodega');
    Route::post('/delete',"BodegaController@destroy")->name('delete-bodega');
    Route::post('/update',"BodegaController@update")->name('update-bodega');
    Route::get('/create',"BodegaController@create")->name('create-bodega');
    Route::post('/store',"BodegaController@store")->name('write-bodega');
});

Route::get('certificados/pdf/{id}', "CertificadosController@imprimible")->name('print-certificado');
Route::prefix('certificados')->middleware(["auth"])->group(function (){
    Route::get('/list',"CertificadosController@index")->name('read-certificado');
    Route::get('/pendientes','CertificadosController@pendientesFactura');
    Route::post('/save','CertificadosController@store')->name('write-certificado');
    Route::post('/uploadImages','CertificadosController@store');
    Route::post('/delete',"CertificadosController@destroy")->name('delete-certificado');

});

Route::prefix('inmuebles')->middleware(["auth"])->group(function () {
    Route::get('/list', "InmueblesController@index")->name('read-inmuebles');
    Route::post('/delete', "InmueblesController@destroy")->name('delete-inmuebles');
    Route::post('/update', "InmueblesController@update")->name('update-inmuebles');
    Route::get('/create', "InmueblesController@create")->name('create-inmuebles');
    Route::post('/store', "InmueblesController@store")->name('write-inmuebles');
    Route::get('/list/{id}', "InmueblesController@show");
    Route::get('/search', "InmueblesController@find");
    Route::get('/propietario/{id}', "InmueblesController@inmueblesPropietario")->where('id', '[0-9]+');
});

Route::prefix('factura')->middleware(["auth"])->group(function () {
    Route::post('/delete', "FacturaController@destroy")->name('delete-facturas');
    Route::post('/update', "FacturaController@update")->name('update-facturas');
    Route::get('/create', "FacturaController@create")->name('create-facturas');
    Route::post('/store', "FacturaController@store")->name('write-facturas');
    Route::get('/list', "FacturaController@index")->name('read-facturas');
    Route::get('/list/{id}', "FacturaController@show");
    Route::get('/pendientes', "FacturaController@pendientes")->name('pendientes');
});

Route::prefix('rutas')->middleware(["auth"])->group(function () {
    Route::get('/list', "RutasController@index")->name('read-rutas');
    Route::get('/pendientes', "RutasController@pendientes");//no debe moverse de aqui
    Route::post('/delete/{id}', "RutasController@destroy")->name('delete-rutas');
    Route::post('/update', "RutasController@update")->name('update-rutas');
    Route::get('/create', "RutasController@create")->name('create-rutas');
    Route::get('/show/{id}', "RutasController@showInMap");//no debe moverse de aqui
    Route::get('/last/{fecha}/{idUser}', "RutasController@showByDateLast");//no debe moverse de aqui
    Route::post('/store', "RutasController@store")->name('write-rutas');
});

Route::prefix('movimientos')->middleware(["auth"])->group(function () {
    Route::get('/list', "MovimientosController@index")->name('read-movimientos');
    Route::get('/', "MovimientosController@create")->name('read-motivo_movimiento');

});

Route::prefix('notifications')->middleware(["auth"])->group(function () {
    Route::get('/check-permisions', "NotificationsController@checkPermisions");

});

Route::resource('contactos',"ClienteContactosController")->middleware(["auth"]);
Route::resource('tipo-contacto',"TipoContactoController")->middleware(["auth"]);
