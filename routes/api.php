<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//Auth::routes();

Route::middleware('auth:api')->get('/user','UserController@getUser');
Route::prefix('productos')->group(function () {
    Route::get('/', 'StockUsuariosController@show')->middleware('auth:api');
    Route::post('/buscar', 'ProductoController@buscar')->middleware('auth:api');
});
Route::prefix('tratamiento')->group(function () {
    Route::get('/', 'TipoTratamientoController@index')->middleware('auth:api');
    Route::put('/', 'TipoTratamientoController@store')->middleware('auth:api');
});
Route::prefix('rutas')->group(function () {
    Route::get('/', 'RutasController@index')->middleware('auth:api');
});

Route::post('/uploads', 'CertificadosController@upload')->middleware('api');

Route::prefix('certificado')->group(function () {
    Route::get('/', 'CertificadosController@index')->middleware('auth:api');
    Route::post('/', 'CertificadosController@store')->middleware('auth:api');
    Route::post('/send-cert/{id}', 'CertificadosController@sendCert')->middleware('auth:api')->where('id', '[0-9]+');
    Route::get('/pdf/{id}', "CertificadosController@imprimible")->middleware('auth:api')->where('id', '[0-9]+');
    Route::post('/pdf/preview', "CertificadosController@preview")->middleware('auth:api');
});

Route::prefix('clientes')->group(function () {
    Route::get('/', 'ClientesController@indexApi')->middleware('auth:api');
    Route::post('/', 'ClientesController@find')->middleware('auth:api');
    Route::put("/","ClientesController@store")->middleware('auth:api');
});
Route::prefix('inmuebles')->group(function () {
    Route::get('/', 'InmueblesController@index')->middleware('auth:api');
    Route::post('/', 'InmueblesController@find')->middleware('auth:api');
    Route::put("/","InmueblesController@store")->middleware('auth:api');
});

Route::prefix('medio_pago')->group(function () {
    Route::get('/', 'MedioPagoController@index')->middleware('auth:api');
});

Route::prefix('trampas')->group(function(){
    Route::get('/','TrampasController@index')->middleware('auth:api');
});
Route::prefix('stock')->group(function () {
    Route::get('/', 'StockUsuariosController@index')->middleware('auth:api');
    Route::post('/', 'StockUsuariosController@find')->middleware('auth:api');
    Route::put("/","StockUsuariosController@UserToUser")->middleware('auth:api');
    Route::put("/return","StockUsuariosController@UserToBodega")->middleware('auth:api');
    Route::prefix('solicitudes')->group(function () {
        Route::put("/","SolicitudTraspasoController@store")->middleware('auth:api');
        Route::put("/unauthorize","SolicitudTraspasoController@unauthorizeTransfer")->middleware('auth:api');
        Route::get('/', 'SolicitudTraspasoController@index')->middleware('auth:api');
        Route::delete('/', 'SolicitudTraspasoController@destroy')->middleware('auth:api');
    });

});

Route::prefix('usuarios')->group(function () {
    Route::post('/', 'UserController@index')->middleware('auth:api');
    Route::put('/',"UserController@EditUser")->middleware("auth:api");
});

Route::prefix('evaluation-criteria')->group(function () {
    Route::get('/', 'GlosaEvaluacionController@index')->middleware('auth:api');
    Route::put('/', 'GlosaEvaluacionController@store')->middleware('auth:api');
    //Route::put("/","ClientesController@store")->middleware('auth:api');
});
Route::prefix('evaluation')->group(function () {
    Route::get('/', 'EvaluacionController@index')->middleware('auth:api');
});
Route::prefix('bodegas')->group(function () {
    Route::get('/', 'BodegaController@index')->middleware('auth:api');
});
Route::prefix('location')->group(function () {
    Route::post('/', 'UserController@update_location')->middleware('auth:api');
});

Route::prefix('cliente-contactos')->group(function () {
    Route::post('/{id}', 'ClienteContactosController@show')->middleware('auth:api')->where('id', '[0-9]+');
});

