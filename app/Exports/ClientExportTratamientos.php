<?php

namespace App\Exports;

use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Database\Eloquent\Collection;

class ClientExportTratamientos implements FromCollection, WithTitle, ShouldAutoSize, WithHeadings,WithMapping
{
    private $cert;
    private $out;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($cert){
        $this->cert=$cert;
        $out=new Collection();
        $this->cert->map(function($element)use ($out){
            $tratinfo=collect();

            foreach ($element->tratamientos as $tratamiento){
                $el=[];
                foreach ($tratamiento as $item=>$value){
                    if(is_array($value)){
                            if($item==='producto'){
                                $value["nombre_producto"]=$tratamiento[$item]["nombre"];
                            }
                        $el=array_merge($el,$value);
                        Log::info("value array: ",$value);
                    }else{
                        Log::info("value not array: ",[$item=>$value]);
                        $el=array_merge($el,[$item=>$value]);
                    }

                }
                $allowed=["nombre_producto","dosis","total","lugares","lote","marca","nombre","ing_activo","id_unidad_medida","vigencia","concentracion"];
                $el=array_filter($el,fn ($key)=>in_array($key,$allowed),ARRAY_FILTER_USE_KEY);
                $tratinfo->push(array_merge(['Folio'=>$element->folio],$el));
            }
            $out->push(...$tratinfo);

        });
        $this->out=$out;
    }

    public function collection()
    {
      return $this->out;
    }

    public function title(): string
    {
        return 'Tratamientos';
    }


    public function headings(): array
    {
        return[
            "Folio",
            "Nombre Tratamiento",
            "Lugares",
            "Nombre Producto",
            "Dosis",
            "Concentracion",
            "Total",
            "Unidad de Medida",
            "Vigencia Tratamiento"
        ];

    }

    public function map($row): array
    {
        return [
            "Folio"=>$row["Folio"],
            "Nombre Tratamiento"=>$row["nombre"],
            "Lugares"=>$row["lugares"],
            "Nombre Producto"=>$row["nombre_producto"],
            "Dosis"=>$row["dosis"],
            "Concentracion"=>$row["concentracion"],
            "Total"=>$row["total"],
            "Unidad de Medida"=>$row["id_unidad_medida"],
            "Vigencia Tratamiento"=>$row["vigencia"],

        ];
    }
}
