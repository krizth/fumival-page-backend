<?php

namespace App\Exports;

use App\Models\Certificado;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
class ClientExport implements  ShouldAutoSize, WithMultipleSheets
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    private $cert;
    public function __construct($id){
        $this->cert=Certificado::
        select('folio','fecha','inmueble','cliente','tratamientos','evaluaciones','trampas')
            ->where('id_cliente',$id)->get();

    }

    public function sheets(): array
    {
        return [new ClientExportCert($this->cert),new ClientExportTratamientos($this->cert),new ClientExportTrampas($this->cert)];
    }
}
