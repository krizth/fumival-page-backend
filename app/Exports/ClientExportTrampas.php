<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class ClientExportTrampas implements FromCollection, WithTitle, ShouldAutoSize
{
    private $cert;
    private $out;
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct($cert){
        $this->cert=$cert;
        $out=new Collection();

        $this->cert->map(function($element)use ($out){
            $current="";
            foreach ($element->trampas as $fields){
                $sort=[];
            foreach ($fields as $field){
                if(($field["inCert"]&&$field["visible"])){
                    if(!array_key_exists($field["_trap_name"],$sort)){
                        $sort=array_merge($sort,[$field["_trap_name"]=>[]]);
                    }
                    $mergeGroupValues=null;
                    if($field['type']==='select'){
                        if($field["multiple"]){
                            $mergeGroupValues=[];
                            foreach ($field["value"] as $optionsSelected){
                                array_push($mergeGroupValues,$optionsSelected["label"]);
                            }
                            $mergeGroupValues=implode(',',$mergeGroupValues);
                        }else{
                            $mergeGroupValues="";
                            $mergeGroupValues= $field["value"]["label"];
                        }

                    }else{
                        if($field['type']==='switch'){
                            $mergeGroupValues="";
                            if($field["value"]!==null){
                                $mergeGroupValues= $field["value"]?"Si":"No";
                            }else{
                                $mergeGroupValues="No";
                            }
                        }else{
                            $mergeGroupValues="";
                            if(array_key_exists('value',$field)){
                                $mergeGroupValues= $field["value"];
                            }
                        }
                    }
                    $sort[$field["_trap_name"]]=array_merge($sort[$field["_trap_name"]],["Folio"=>$element->folio,$field["label"]=> $mergeGroupValues]);
                }
            }
                if(sizeof($sort)>0){
                    foreach ($sort as $key=>$value){
                        if($current!=$key){
                            $current=$key;
                            $out->push([$key]);
                            $out->push([array_keys($value)]);
                        }
                            $out->push(array_values($value));


                    }
                }
            }

        });
        $this->out=$out;
    }

    public function collection()
    {
        return $this->out;
    }

    public function title(): string
    {
        return 'Trampas';
    }

}
