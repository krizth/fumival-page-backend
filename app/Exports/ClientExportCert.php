<?php

namespace App\Exports;

use App\Models\Certificado;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class ClientExportCert implements FromCollection , WithMapping, WithHeadings, WithTitle, ShouldAutoSize
{
    private $cert;
    public function __construct($cert){
        $this->cert=$cert;
    }
    public function map($row): array
    {
        return [
            $row->folio,
            $row->fecha,
            $row->inmueble["direccion"],
            $row->cliente["nombre"],
            $row->inmueble["propietario"]["nombre"],
            $row->inmueble["propietario"]["rut"],
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function title(): string
    {
        return 'Certificados';
    }

    public function collection()
    {
        return $this->cert;
    }
    public function headings(): array
    {
        return [
            "Folio",
            "Fecha Tratamiento",
            "Inmueble tratado",
            "Nombre Cliente",
            "Propietario",
            "RUT Propietario",
        ];
    }
}
