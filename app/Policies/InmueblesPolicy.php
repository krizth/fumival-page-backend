<?php

namespace App\Policies;

use App\Models\Inmuebles;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InmueblesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess(['read-inmuebles']);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Inmuebles  $inmuebles
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->hasAccess(['read-inmuebles']);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['write-inmuebles']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Inmuebles  $inmuebles
     * @return mixed
     */
    public function update(User $user, Inmuebles $inmuebles)
    {
        return $user->hasAccess(['update-inmuebles']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Inmuebles  $inmuebles
     * @return mixed
     */
    public function delete(User $user, Inmuebles $inmuebles)
    {
        return $user->hasAccess(['delete-inmuebles']);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Inmuebles  $inmuebles
     * @return mixed
     */
    public function restore(User $user, Inmuebles $inmuebles)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Inmuebles  $inmuebles
     * @return mixed
     */
    public function forceDelete(User $user, Inmuebles $inmuebles)
    {
        //
    }
}
