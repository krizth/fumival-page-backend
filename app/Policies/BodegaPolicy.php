<?php

namespace App\Policies;

use App\Models\Bodega;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BodegaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess(['read-bodega']);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Bodega  $bodega
     * @return mixed
     */
    public function view(User $user, Bodega $bodega)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['write-bodega']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Bodega  $bodega
     * @return mixed
     */
    public function update(User $user, Bodega $bodega)
    {
        return $user->hasAccess(['update-bodega']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Bodega  $bodega
     * @return mixed
     */
    public function delete(User $user, Bodega $bodega)
    {
        return $user->hasAccess(['delete-bodega']);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Bodega  $bodega
     * @return mixed
     */
    public function restore(User $user, Bodega $bodega)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Bodega  $bodega
     * @return mixed
     */
    public function forceDelete(User $user, Bodega $bodega)
    {
        //
    }
}
