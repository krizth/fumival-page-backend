<?php

namespace App\Policies;

use App\Models\StockUsuarios;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StockUsuariosPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess(['read-stock_usuarios']);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->hasAccess(['read-stock_usuario']);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['write-stock_usuario']);
    }

    public function createAny(User $user)
    {
        return $user->hasAccess(['write-stock_usuarios']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAccess(['update-stock_usuario']);
    }
    public function updateAny(User $user)
    {
        return $user->hasAccess(['update-stock_usuarios']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAccess(['delete-stock_usuario']);
    }
    public function deleteAny(User $user)
    {
        return $user->hasAccess(['delete-stock_usuarios']);
    }
    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return mixed
     */
    public function restore(User $user)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
    }
}
