<?php

namespace App\Policies;

use App\Models\TipoTratamiento;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TipoTratamientoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess(['read-tipo_tratamiento']);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\TipoTratamiento  $tipoTratamiento
     * @return mixed
     */
    public function view(User $user, TipoTratamiento $tipoTratamiento)
    {
        return $user->hasAccess(['read-tipo_tratamiento']);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['write-tipo_tratamiento']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\TipoTratamiento  $tipoTratamiento
     * @return mixed
     */
    public function update(User $user, TipoTratamiento $tipoTratamiento)
    {
        return $user->hasAccess(['update-tipo_tratamiento']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\TipoTratamiento  $tipoTratamiento
     * @return mixed
     */
    public function delete(User $user, TipoTratamiento $tipoTratamiento)
    {
        return $user->hasAccess(['delete-tipo_tratamiento']);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\TipoTratamiento  $tipoTratamiento
     * @return mixed
     */
    public function restore(User $user, TipoTratamiento $tipoTratamiento)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\TipoTratamiento  $tipoTratamiento
     * @return mixed
     */
    public function forceDelete(User $user, TipoTratamiento $tipoTratamiento)
    {
        //
    }
}
