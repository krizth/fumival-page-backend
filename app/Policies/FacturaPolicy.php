<?php

namespace App\Policies;

use App\Models\Factura;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FacturaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasAccess(['read-facturas']);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Factura  $factura
     * @return mixed
     */
    public function view(User $user, Factura $factura)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['write-facturas']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Factura  $factura
     * @return mixed
     */
    public function update(User $user, Factura $factura)
    {
        return $user->hasAccess(['update-facturas']);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Factura  $factura
     * @return mixed
     */
    public function delete(User $user, Factura $factura)
    {
        return $user->hasAccess(['delete-facturas']);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Factura  $factura
     * @return mixed
     */
    public function restore(User $user, Factura $factura)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Factura  $factura
     * @return mixed
     */
    public function forceDelete(User $user, Factura $factura)
    {
        //
    }
}
