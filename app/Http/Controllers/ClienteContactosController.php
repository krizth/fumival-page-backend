<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\ClienteContactos;
use App\Models\Contactos;
use App\Models\TipoContacto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteContactosController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacto=Contactos::create([
           'id_tipo_contacto'=>$request->id_tipo_contacto,
           'contacto'=>$request->contacto,
        ]);

        ClienteContactos::create([
           'id_cliente'=>$request->id_cliente,
            'id_contacto'=>$contacto->id,
            'predeterminado'=>0

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->wantsJson()){
            $mails=DB::select('SELECT
                    contactos.contacto,
                    contactos.id
                    FROM
                    cliente_contactos
                    INNER JOIN contactos ON cliente_contactos.id_contacto = contactos.id
                    where cliente_contactos.id_cliente=:id and contactos.id_tipo_contacto=:id_tipo_contacto;',["id"=>$id,"id_tipo_contacto"=>$request->tipo_contacto]);
            if (sizeof($mails)==0){
                $mails=Cliente::find($id);
                if($mails->email===null){
                    return $this->responseOK([['contacto'=>'fumival@gmail.com']]);
                }else{
                    $mails=[['contacto'=>$mails->email]];
                }
            }
           return $this->responseOK($mails);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Contactos::find($id)->update([
            'contacto'=>$request->contacto,
            'id_tipo_contacto'=>$request->id_tipo_contacto
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ClienteContactos::where('id_contacto',$id)->delete();
        Contactos::find($id)->delete();
    }
}
