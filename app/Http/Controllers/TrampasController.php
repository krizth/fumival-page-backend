<?php

namespace App\Http\Controllers;

use App\Models\Trampas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TrampasController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return Trampas[]|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if($request->wantsJson()){
         return Trampas::with(['tipoTratamiento'])->get();
        }
        return view('mantenedores.trampas',["trampas"=>Trampas::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Trampas::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function show(Request $request)
    {
        return Trampas::with(['tipoTratamiento'])->where('id',$request->id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Trampas::where('id',$request->id)->update($request->except("id"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator=  $validator = Validator::make(["id"=>$request->id], [
            'id'=>'numeric|required|exists:trampas,id',
        ]);
        if(!$validator->fails()){
            Trampas::destroy($request->id);
            return $this->responseOK([]);
        }else{
            return $this->respondFailedParametersValidation();
        }

    }
}
