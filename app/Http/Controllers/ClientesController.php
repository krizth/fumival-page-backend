<?php

namespace App\Http\Controllers;

use App\Exports\ClientExport;
use App\Models\Certificado;
use App\Models\Cliente;
use App\Models\ClienteContactos;
use App\Models\Contactos;
use App\Models\Inmuebles;
use App\Models\TipoCliente;
use Exception;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\InmueblesCliente;
use Maatwebsite\Excel\Facades\Excel;

class ClientesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Cliente::class);

        if ($request->wantsJson()) {

            if (!$request->has('inmueble')) {
                return $this->responseOK(Cliente::with(['tipoCliente'])->get());
            }
            return $this->responseOK(Cliente::with(['tipoCliente','inmuebles' => function ($query) {
                return $query->with(['propietario']);
            }])->get());
        }
        $clientes = Cliente::with(['tipoCliente'])->get();
        return view('clientes.index', compact('clientes'));
    }

    public function indexApi(Request $request)
    {
        if ($request->wantsJson()) {
            return $this->responseOK(Cliente::with(['tipoCliente','inmuebles' => function ($query) {
                return $query->with(['propietario']);
            }])->get());
        }
    }

    /**
     * Find a listing of the resource.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required',
        ]);
        if ((!$validator->fails()) || $request->wantsJson()) {
            return $this->responseOK(Cliente::with(['tipoCliente','inmuebles' => function ($query) {
                return $query->with(['propietario']);
            }])->where('cliente.' . $request->field, 'like', '%' . $request->search . '%')->get());
        } else {
            return $this->responseOK(Cliente::all());
        }
    }

    /**
     * Show the form for creating a new resource.
     * ONLY API METHOD
     * @return TipoCliente[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function create()
    {
        return TipoCliente::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Cliente::class);
        $cliente=null;
        if ($request->has('inmuebles')) {
            try {
                DB::beginTransaction();
                    $cliente = Cliente::create($request->except('inmuebles'));

                    if (!empty($request->inmuebles)) {
                        $cliente->inmuebles()->attach($request->inmuebles);
                    }
                if($request->has("contactos")){
                    if(sizeof($request->contactos)>0){
                        foreach ($request->contactos as $contacto){
                            $contactos= Contactos::create([
                                'id_tipo_contacto'=>$contacto["id_tipo_contacto"],
                                'contacto'=>$contacto["contacto"]
                            ]);
                            ClienteContactos::create([
                                'id_cliente'=>$cliente->id,
                                'id_contacto'=>$contactos->id,
                                'predeterminado'=>0
                            ]);
                        }
                    }
                }
                    $cliente->save();
                    DB::commit();
                    return $this->responseOK($cliente);

            } catch (Exception $th) {
                logger($th->getMessage());
                return $this->respondInternalError();
            }
        }else{
            return $this->responseOK(Cliente::updateOrCreate(
                ["id" => $request->id],
                [
                    "ciudad" => $request->ciudad,
                    "direccion_principal" => $request->direccion_principal,
                    "nombre_fantasia" => $request->nombre_fantasia,
                    "rut" => $request->rut,
                    "fono" => $request->fono,
                    "email" => $request->email,
                    "nombre" => $request->nombre,
                    "id_tipo_cliente"=>$request->id_tipo_cliente
                ]
            ));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('viewAny', Cliente::class);
        return Cliente::with(['inmuebles:id','clienteContactos'=>function($query){
            return $query->with(['contacto'=>function($query){
                return $query->with("tipoContacto");
            }]);
        }])->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function tipoCliente(Request $request,$idCliente=null){

        if($idCliente!=null){
            return Cliente::with('tipoCliente')->find($idCliente);
        }else{
            if($request->folios){
                return Certificado::select('cliente.id','certificado.id_cliente','folio',"tipo_cliente.impuesto")
                    ->join('cliente','certificado.id_cliente','=','cliente.id')
                    ->join('tipo_cliente',"cliente.id_tipo_cliente","=","tipo_cliente.id")
                    ->whereIn('certificado.folio',$request->folios)->get()->pluck('impuesto');
            }

        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $cliente = Cliente::find($request->get('id'));
        $this->authorize('update', $cliente);
        $cliente->update($request->all());
        $cliente->inmuebles()->sync($request->inmuebles);
        $cliente->save();
        $this->responseOK($cliente);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $cliente = Cliente::find($request->get('id'));
        ClienteContactos::where('id_cliente',$cliente->id)->delete();
        $inmuebles=InmueblesCliente::where('id_cliente',$cliente->id);
        $inmuebles->delete();
        $this->authorize('delete', $cliente);
        $this->responseOK($cliente->delete());
    }


    public function exportExcel(Request $request){
        $cliente=Cliente::find(1);
        return (new ClientExport(1))->download($cliente->nombre.'.xlsx');
    }
}
