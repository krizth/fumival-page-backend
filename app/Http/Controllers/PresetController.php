<?php

namespace App\Http\Controllers;

use App\Models\Preset;
use App\Models\Producto;
use App\Models\Stock;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PresetController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $producto = Producto::create($request->except('componentes'));

                foreach ($request->get('componentes') as $componente) {

                    Preset::create([
                        'id_producto' => $producto->id,
                        'id_compuesto' => $componente['id_producto'],
                        'cantidad' => $componente['cantidad']
                    ]);

                }
            });
            return $this->responseOK("Listo");
        } catch (Exception $e) {
            return $this->respondBadRequest();
        }
    }
    public function getComponentes($id)
    {
        return Preset::where(['id_producto'=>$id])->with(['compuesto'])->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {

            DB::transaction(function () use ($request) {

                $producto = Producto::find($request->id);

                if (!$producto) {
                    return $this->respondNotFound("Producto no encontrado.");
                }

                $producto->update(array_merge($request->except('componentes'), ['maquinaria'=>0]));
                $presets = Preset::where(['id_producto'=> $producto->id]);
                $presets->delete();

                foreach ($request->get('componentes') as $componente) {
                    //$stock = Stock::where(['id_producto' => $componente['id_producto']])->where('cantidad', '>', 0)->first();

                    // if (empty($stock)) {
                    //     throw new Exception("Sin stock disponible");
                    // }

                    Preset::create([
                        'id_producto' => $producto->id,
                        'id_compuesto' => $componente['id_producto'],
                        'cantidad' => $componente['cantidad']
                    ]);

                    // $stock->cantidad = $stock->cantidad - $preset->cantidad;

                    // if ($stock->cantidad < 0) {
                    //     throw new Exception("Sin saldo disponible");
                    // }

                    // $stock->save();
                }
            });

            return $this->responseOK("Listo");
        } catch (Exception $e) {
            return $this->respondBadRequest();
        }
    }

    public function getFactores($id)
    {
        $producto = Producto::with(['factorConversions'])->where(['id'=>$id])->first();

        return $producto->toArray()['factor_conversions'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
