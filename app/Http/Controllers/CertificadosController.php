<?php

namespace App\Http\Controllers;

use App\Mail\sendCert;
use App\Models\Certificado;
use App\Models\Cliente;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use File;


class CertificadosController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Certificado::class);
        $cantidad = 5;
        if (isset($request->cantidad)) {
            $cantidad = $request->cantidad;
        }
        if ($request->wantsJson()) {
            return $this->responseOK(Certificado::select(
                "id",
                "folio",
                "fecha",
                "hora_inicio",
                "hora_termino",
                "valor",
                "cond_pago",
                "observaciones",
                "inmueble",
                "cliente"
            )->where('id_usuario',Auth::id())->orderBy('created_at', 'desc')->take($cantidad)->get());
        }
        $certificados = Certificado::all();
        return view('certificados.index', compact('certificados'));
    }

    /**
     * Upload a newly file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function upload(Request $request)
    {
        $path = collect();
        foreach ($request->file() as $file) {
            $path->put($file->getClientOriginalName(), $file->store("images"));
        }
        return ["images" => $path];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|string
     */
    public function store(Request $request)
    {
        $this->authorize('create', Certificado::class);
        DB::beginTransaction();
        $user=User::find(Auth::id());
        if(!Hash::check($request->password,$user->password)){
            return $this->respondFailedParametersValidation("La contraseña es incorrecta");
        }
        $date = Carbon::now('America/Santiago');

        $empresa = Empresa::select()->first();

        if(!$empresa){
           return $this->respondWithError("No se pudo encontrar una empresa valida para crear el certificado.
           Debe ir a la pagina web del sistema y crear una empresa");
        }
        $tecnico=User::find(Auth::id());
        $firmas=$request->firma;
        $inmuebClient=Cliente::find($request->cliente["id"]);
        $attachedId = $inmuebClient->inmuebles()->where('id',$request->inmueble["id"])->pluck("id");
        if(count($attachedId)==0){
            $inmuebClient->inmuebles()->attach($request->inmueble["id"]);
        }

        if(!$request->evaluaciones){
            return $this->respondFailedParametersValidation("Las evaluaciones no se ingresaron");
        }

            if($tecnico->firma==null){
                $tecnico->firma=$firmas["tecnico"];
                $tecnico->save();
            }else{
                $firmas["tecnico"]=$tecnico->firma;
            }
            if($tecnico->tec_a_cargo==null){
                $firmas["responsable"]=$firmas["tecnico"];
            }else{
                $responsable=User::find($tecnico->tec_a_cargo);
                $firmas["responsable"]=$responsable->firma;
            }
            try{
               $certificado=Certificado::StoreCert($firmas,$date,$empresa,$request);
                DB::commit();
            }catch(\Exception $e){
                DB::rollBack();
                return $this->respondWithError("Ocurrio un error: ".$e->getMessage());
            }
        /* send notification */
        $Devices=User::whereNotNull('fcm_token')->get();
        foreach($Devices as $device){
            if($device->hasAccess(['receive-cert-notifications'])){
                try{
                    $this->sendPush(
                        ['title'=>'Nuevo Certificado creado',
                            'body'=>'Folio: '.$certificado->folio.', Haga click aqui para mas informacion'],
                        ["folio"=>$certificado->folio,"id_certificado"=>$certificado->id],
                        route('print-certificado',$certificado->id),
                        $device->fcm_token);
                }catch (\Exception $e){
                    continue;
                }
            }
        }
        /* send notification */

        return ['cert'=>$this->imprimible($request,$certificado->id)->render(),'id_cert'=>$certificado->id];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function sendCert(Request $request,$id)
    {
        $mail=$request->mail;
        $client=Cliente::find($request->id_cliente);
        if(!$mail){
            $mail=$client->email;
        }
        $certificado=Certificado::find($id);
        $filename=$certificado->folio.'.pdf';
        Mail::to($mail)->send(new sendCert(["nombre"=>$client->nombre,"fecha"=>Carbon::createFromFormat('Y-m-d',$certificado->fecha)->format('d/m/Y')],File::get($request->file('cert')) ,$filename));

    }

    public function pendientesFactura(Request $request){

        $folio=$request->folio;
        $certs= Certificado::select('id','folio','valor','cliente->nombre as client','inmueble->direccion as direccion','fecha','usuario->nombre as user')->whereDoesntHave('facturas')->orderBy('folio','desc');
            if(!$folio){
                if($request->cantidad){
                    return $certs->take($request->cantidad)->get();
                }
                return $certs->get();
            }
        if($request->cantidad) {
            return $certs->where('folio', $folio)->take($request->cantidad)->get();
        }
        return $certs->where('folio', $folio)->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $certificado=Certificado::find($request->id);
        $this->responseOK($certificado->delete());
    }

    public function imprimible(Request $request,int $id)
    {

        if($request->wantsJson()){
            $this->authorize('view', Certificado::class);
        }else{
            $this->authorize('viewAny', Certificado::class);
        }
        $certificado = Certificado::with('imagenes:imagen')->find($id);

        if (empty($certificado)) {
            return $this->responseNoContent();
        }

        $tecnico = $certificado->usuario['tec_a_cargo'] == null ? $certificado->usuario['nombre'] : User::find($certificado->usuario['tec_a_cargo'])->nombre;
        $emp = Empresa::find(1);
        $emp->logo = $this->handleImages(true, $emp->logo);
        $imagenes=collect();
        if($certificado->imagenes){
            array_map(function($imagen)use($imagenes){
                $imagenes->push($this->handleImages(true, $imagen));
            },$certificado->imagenes);
            $certificado->imagenes=$imagenes;
        }

        return view('certificados.imprimible', compact('certificado'), ['tecnico_cargo' => $tecnico, "emp" => $emp]);
    }

    public function preview(Request $request)
    {

        if($request->wantsJson()){
            $this->authorize('view', Certificado::class);
        }else{
            $this->authorize('viewAny', Certificado::class);
        }
        $certificado = $request;

        if (empty($certificado)) {
            return $this->responseNoContent();
        }
        $certificado->fecha=Carbon::now('America/Santiago')->format('Y-m-d');
        $tecnico_a_cargo = User::with('tecnicoACargo')->find(Auth::id())->tecnicoACargo;
        $emp = Empresa::find(1);
        $emp->logo = $this->handleImages(true, $emp->logo);
        $imagenes=collect();
        if($certificado->imagenes){
            array_map(function($imagen)use($imagenes){
                $imagenes->push($this->handleImages(true, $imagen));
            },$certificado->imagenes);
            $certificado->imagenes=$imagenes;
        }
        if(sizeof($certificado->tratamientos)>0){
            $tratamientos=collect();
            foreach($certificado->tratamientos as $el ){
                $vigencia=Carbon::now('America/Santiago');
                if(strpos($el["vigencia"],':')===false){
                    $vigencia->addDays($el["vigencia"]);
                    $el["vigencia"]=$vigencia->format('d/m/Y');
                }
                $tratamientos->push($el);
            }
            $certificado->hora_inicio=$certificado->horario["hora_inicio"];
            $certificado->hora_termino=$certificado->horario["hora_fin"];
            $certificado->merge(['tratamientos'=>$tratamientos]);
            $firmas=collect($certificado->firma);
            $firmas["responsable"]=$tecnico_a_cargo["firma"];
            $certificado->merge(['firmas'=> $firmas]);

        }
        return view('certificados.imprimible', compact('certificado'), ['tecnico_cargo' => $tecnico_a_cargo->nombre, "emp" => $emp]);
    }
}
