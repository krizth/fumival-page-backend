<?php

namespace App\Http\Controllers;

use App\Http\Resources\FactorConversionResource;
use App\Models\FactorConversion;
use App\Models\Producto;
use App\Models\UnidadMedida;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FactorConversionController extends ApiController
{
    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', FactorConversion::class);

        $factor = FactorConversion::select("id", "id_unidad_medida_origen", "id_unidad_medida_destino", "factor_conversion", "descripcion")
            ->with(["unidadMedidaDestino:id,nombre", "unidadMedidaOrigen:id,nombre"]);

        if ($request->has('id_unidad_minima')) {
            $factor =  $factor->where([
                ['id_unidad_medida_origen',"=", $request->get('id_unidad_minima')],
                ["forProduct","=",0]
            ])->get();
        } else {
            $factor = $factor->get();
        }

        $factor_conversion = FactorConversionResource::collection($factor);

        if ($request->wantsJson()) {
            return $factor_conversion;
        }

        return view('mantenedores.conversion', compact("factor_conversion"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', FactorConversion::class);

        try {
            DB::transaction(function () use($request) {
                $factor = FactorConversion::create($request->only('id_unidad_medida_origen', 'id_unidad_medida_destino', 'factor_conversion', 'descripcion','forProduct'));
                if($request->id_producto){
                    $factor->factorConversionProds()->attach($request->id_producto);
                }
                return $this->responseOK($factor);
            });
        } catch (Exception $th) {
            logger($th->getMessage());
            return $this->respondInternalError();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('anyView', FactorConversion::class);
        return FactorConversion::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->authorize('update', FactorConversion::class);
        if ($request->isMethod('POST')) {
            $data = $request->only('id_unidad_medida_origen', 'id_unidad_medida_destino', 'factor_conversion', 'descripcion');
            try {
                if (FactorConversion::where('id', $request->id)->update($data)) {
                    return $this->responseOK(FactorConversion::where('id', $request->id)->get());
                } else {
                    return $this->respondWithError("No se realizo ningun cambio");
                }
            } catch (\Illuminate\Database\QueryException $e) {
                return $this->respondFailedParametersValidation();
            }
        }
        if ($request->isMethod("GET")) {
            return $this->responseOK(UnidadMedida::all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $factorConversion = FactorConversion::find($request->get('id'));
        $this->authorize('delete', $factorConversion);

        $factorConversion->factorConversionProds()->detach();

        return $this->responseOK($factorConversion->delete());
    }
}
