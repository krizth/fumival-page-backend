<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductosResource;
use App\Http\Resources\StockUsuariosResource;
use App\Models\FactorConversion;
use App\Models\MotivoMovimiento;
use App\Models\Movimientos;
use App\Models\MovimientosStockUsuarios;
use App\Models\Producto;
use App\Models\SolicitudTraspaso;
use App\Models\Stock;
use App\Models\StockUsuarios;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StockUsuariosController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', StockUsuarios::class);

        $stockUsuario= StockUsuarios::with(['producto' => function ($query) {
            return $query->with(['unidadMedida', 'factorConversions']);
        }])->where([['id_usuario',"=", Auth::id()],["cantidad",">",0]])->get();

        if ($request->wantsJson()) {
            return $this->responseOK(
                StockUsuariosResource::collection(
                    $stockUsuario
                )
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('createAny', StockUsuarios::class);

        $stock = Stock::where($request->all(['id_producto', 'lote']))->where('cantidad', '>', 0)->first();
        $cantidad = $request->get('cantidadConvertida') != 0 ? $request->get('cantidadConvertida') : $request->get('cantidad');

        if (!$stock) {
            return $this->respondNotFound("No existe stock.");
        }

        $saldo = $stock->cantidad - $cantidad;

        if ($saldo < 0) {
            return $this->respondWithError("Saldo insuficiente.");
        }
        try {

            DB::transaction(function () use ($stock, $saldo, $request, $cantidad) {
                $stock->cantidad = $saldo;

                $stock->save();

                $stockUsuario = StockUsuarios::where($request->all(['id_producto', 'id_usuario', 'lote']))->first();

                $movimientos = Movimientos::create([
                    'id_producto' => $request->get('id_producto'),
                    'id_usuario_origen' => Auth::id(),
                    'id_motivo' => MotivoMovimiento::$TRASPASO,
                    'recuento' => $cantidad
                ]);

                if (!$stockUsuario) {
                    $stockUsuario = StockUsuarios::create(array_merge($request->all(['id_usuario', 'id_producto', 'cantidad', 'lote', 'caducidad']), ['cantidad' => $cantidad]));

                    return $this->responseOK(MovimientosStockUsuarios::create([
                        'id_movimiento' => $movimientos->id,
                        'id_usuario_destino' => $request->get('id_usuario'),
                        'id_stock_usuarios' => $stockUsuario->id
                    ]));
                }

                MovimientosStockUsuarios::create(
                    [
                        'id_movimiento' => $movimientos->id,
                        'id_usuario_destino' => $request->get('id_usuario'),
                        'id_stock_usuarios' => $stockUsuario->id
                    ]
                );

                $stockUsuario->cantidad += $cantidad;

                $stockUsuario->save();
            });

            return $this->responseOK(true);
        } catch (Exception $e) {

            return $this->respondInternalError($e->getMessage());
        }
    }
    /**
     * Store a newly created resource in storage.
     * for api user
     * @param  \Illuminate\Http\Request  $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */


    public function UserToUser(Request $request)
    {
        $this->authorize('update', StockUsuarios::class);
        $user=User::find(Auth::id());
        if(!Hash::check($request->password,$user->password)){
            return $this->respondFailedParametersValidation("La contraseña es incorrecta");
        }
        $solicitud= SolicitudTraspaso::where([['token',"=",$request->token],["id_estado","=",SolicitudTraspaso::$EN_TRAMITE]])->first();
        if(!$solicitud){
            return $this->respondWithError("Ya se proceso esta solicitud");
        }

        $data= new Request();
        $data->replace([
            'factor_conversion'=>collect(['id'=>$solicitud->id_unidad_medida,'minima'=>$solicitud->minima]),
            'id_usuario'=>$solicitud->id_solicitante,
            'cantidad'=>$solicitud->cantidad,
            'id_producto'=>$solicitud->id_producto,
            'token'=>$solicitud->token,
            'lote'=>$request->lote
        ]);
        DB::beginTransaction();
        $stock = StockUsuarios::where([
            ['id_producto', "=", $data->id_producto],
            ["id_usuario", "=", Auth::id()],
            ["lote", "=", $data->lote]
        ])->first();
        if ($data->factor_conversion["minima"]) {
            $saldo = $stock->cantidad - $data->cantidad;
            $recuento = $data->cantidad;
        } else {
            $conversion = FactorConversion::find($data->factor_conversion["id"]);
            $saldo = $stock->cantidad - ($conversion->factor_conversion * $data->cantidad);
            $recuento = ($conversion->factor_conversion * $data->cantidad);
        }

        if ($saldo < 0) {
            DB::rollBack();
            return $this->respondWithError("Saldo insuficiente.");
        }
        try {

                $stock->cantidad = $saldo;

                $stock->save();
                $stockUsuario = StockUsuarios::where([['id_usuario' ,"=", $data->id_usuario],['id_producto' ,'=' ,$data->id_producto],["lote","=",$data->lote] ])->first();

                $movimientos = Movimientos::create(['id_producto' => $data->id_producto, 'id_usuario_origen' => Auth::id(), 'id_motivo' => MotivoMovimiento::$TRASPASO, 'recuento' => $recuento]);

                if (!$stockUsuario) {
                    $stockUsuario = StockUsuarios::create($data->only(["id_usuario", "id_producto", "cantidad","lote"]));
                    MovimientosStockUsuarios::create(['id_movimiento' => $movimientos->id, 'id_usuario_destino' => $data->id_usuario, 'id_stock_usuarios' => $stockUsuario->id]);
                    DB::commit();
                    return $this->responseOK(StockUsuariosResource::collection(
                        StockUsuarios::with(['producto' => function ($query) {
                            return $query->with(['unidadMedida']);
                        }])
                            ->where('id_usuario', Auth::id())->get()
                    ));
                }
                $stockUsuario->cantidad += $data->cantidad;
                MovimientosStockUsuarios::create(['id_movimiento' => $movimientos->id, 'id_usuario_destino' => $data->id_usuario, 'id_stock_usuarios' => $stockUsuario->id]);

                $stockUsuario->save();
                SolicitudTraspaso::autorizarTraspaso($data->token);
            DB::commit();
            /* send notification */

            $Devices=User::whereNotNull('fcm_token')->get();
            foreach($Devices as $device){
                if($device->hasAccess(['receive-cert-notifications'])){
                    try{
                        $this->sendPush(
                            ['title'=>'Traspaso de Stock Entre Usuarios',
                                'body'=>'N° Movimiento: '.$movimientos->id.', Haga click aqui para mas informacion'],
                            ["movimiento"=>$movimientos->id,"id_usuario"=>$data->id_usuario], '',
                            $device->fcm_token);
                    }catch (\Exception $e){
                        continue;
                    }
                }
            }
            /* send notification */
            return $this->responseOK(
                StockUsuariosResource::collection(
                    StockUsuarios::with(['producto' => function ($query) {
                        return $query->with(['unidadMedida']);
                    }])
                        ->where('id_usuario', Auth::id())->get()
                )
            );
        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required',
        ]);
        if ((!$validator->fails()) && $request->wantsJson()) {
            $this->authorize('view', StockUsuarios::class);
            return $this->responseOK(
                ProductosResource::collection(
                   StockUsuarios::with(["producto"=>function($query)use ($request){
                       return $query->select("id","id_unidad_minima")->with(["factorConversions",'unidadMedida']);
            }])  ->join('producto', "stock_usuarios.id_producto", "=", "producto.id")->where([
                        ['stock_usuarios.id_usuario',"=", Auth::id()],
                        ["stock_usuarios.cantidad", ">", 0],
                        ['producto.' . $request->field, 'like', '%' . $request->search . '%']
                    ])->get()
                )
            );
        } else {
            $this->authorize('viewAny', StockUsuarios::class);
            return $this->responseOK(
                ProductosResource::collection(
                    $this->paginate(  StockUsuarios::with(["producto"=>function($query)use ($request){
                        return $query->select("id","id_unidad_minima")->with(["factorConversions",'unidadMedida']);
                    }])  ->join('producto', "stock_usuarios.id_producto", "=", "producto.id")->where([
                        ['stock_usuarios.id_usuario',"=", Auth::id()],
                        ["stock_usuarios.cantidad", ">", 0],
                    ])->get(), 10, $request->page)
                )
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(StockUsuarios $stockUsuarios)
    {
        //
    }

    public function UserToBodega(Request $request)
    {
        $this->authorize('create', StockUsuarios::class);
        $stock = StockUsuarios::where([
            ['id_producto', "=", $request->id_producto],
            ["id_usuario", "=", Auth::id()]
        ])->first();
        $this->authorize('create', $stock);
        if ($request->factor_conversion["minima"]) {
            $Unidad=$request->factor_conversion["descripcion"];
            $saldo = $stock->cantidad - $request->cantidad;
            $recuento = $request->cantidad;
        } else {
            $conversion = FactorConversion::find($request->factor_conversion["id"]);
            $Unidad=$conversion->descripcion;
            $saldo = $stock->cantidad - ($conversion->factor_conversion * $request->cantidad);
            $recuento = ($conversion->factor_conversion * $request->cantidad);
        }

        if ($saldo < 0) {
            return $this->respondWithError("Saldo insuficiente.");
        }
        try {
            DB::beginTransaction();
                $stock->cantidad = $saldo;
                $stock->save();
                $stockBodega = Stock::where(['id_bodega' => $request->bodega["id"], 'id_producto' => $request->id_producto])->first();
                $movimientos = Movimientos::create(['id_producto' => $request->id_producto, 'id_usuario_origen' => Auth::id(), 'id_motivo' => MotivoMovimiento::$DEVOLUCION, 'recuento' => $recuento]);
                if (!$stockBodega) {
                    Stock::create(['id_producto' => $request->id_producto, "id_bodega" => $request->bodega["id"]]);
                    return $this->responseOK(StockUsuariosResource::collection(
                        StockUsuarios::with(['producto' => function ($query) {
                            return $query->with(['unidadMedida']);
                        }])
                            ->where('id_usuario', Auth::id())->get()
                    ));
                }
                $stockBodega->cantidad += $request->cantidad;
                $stockBodega->save();
            DB::commit();

            if($movimientos){
                /* send notification */
                $Devices=User::whereNotNull('fcm_token')->get();
                $Producto=Producto::find($request->id_producto);
                foreach($Devices as $device){
                    if($device->hasAccess(['receive-cert-notifications'])){
                        try{
                            $this->sendPush(
                                ['title'=>'Traspaso de Stock de '.Auth::user()->nombre.' Hacia Bodega',
                                    'body'=>'N° Movimiento: '.$movimientos->id.', Producto: '.$Producto->nombre.
                                        ', Cantidad: '.$recuento.' '.$Unidad],
                                ["producto"=>$Producto->id,"movimiento"=>$movimientos->id,"id_usuario"=>Auth::id()], '',
                                $device->fcm_token);
                        }catch (\Exception $e){
                            continue;
                        }
                    }
                }
                /* send notification */
            }

            return $this->responseOK(
                StockUsuariosResource::collection(
                    StockUsuarios::with(['producto' => function ($query) {
                        return $query->with(['unidadMedida']);
                    }])
                        ->where('id_usuario', Auth::id())->get()
                )
            );
        } catch (Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $stock = StockUsuarios::find($request->get('id'));
        $cantidad = $request->get('cantidadConvertida') != 0 ? $request->get('cantidadConvertida') : $request->get('cantidad');
        $this->authorize('update', $stock);
        if (!$stock) {
            return $this->respondNotFound();
        }

        if (!$request->has('motivo_movimiento')) {
            return $this->respondBadRequest();
        }

        try {
            DB::transaction(function () use ($request, $stock, $cantidad) {
                switch ($request->get('motivo_movimiento')) {
                    case MotivoMovimiento::$INGRESO:
                        throw new Exception("Acción no permitida");
                    case MotivoMovimiento::$APLICACION:
                        $stock->cantidad -= $cantidad;

                        $stock->save();

                        $movimiento = Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => $stock->id_usuario,
                            'id_motivo' => MotivoMovimiento::$APLICACION,
                            'recuento' => $cantidad,
                            'lote' => $request->get('lote')
                        ]);

                        MovimientosStockUsuarios::create([
                            'id_movimiento' => $movimiento->id,
                            'id_usuario_destino' => $stock->id_usuario,
                            'id_stock_usuarios' => $stock->id
                        ]);
                        return $this->responseOK("Actualizado con exito.");
                    case MotivoMovimiento::$AJUSTE: {
                            $stock->cantidad = $cantidad;

                            $stock->save();

                            $movimiento = Movimientos::create([
                                'id_producto' => $request->get('id_producto'),
                                'id_usuario_origen' => Auth::id(),
                                'id_motivo' => MotivoMovimiento::$AJUSTE,
                                'recuento' => $cantidad,
                                'lote' => $request->get('lote')
                            ]);

                            MovimientosStockUsuarios::create([
                                'id_movimiento' => $movimiento->id,
                                'id_usuario_destino' => Auth::id(),
                                'id_stock_usuarios' => $stock->id
                            ]);

                            return $this->responseOK("Actualizado con exito.");
                        }
                    case MotivoMovimiento::$DEVOLUCION:

                        if (!$request->has('id_bodega')) {
                            throw new Exception("Sin bodega seleccionada", 1);
                        }

                        $stock->cantidad -= $cantidad;

                        $stock->save();

                        $stockDevolucion = Stock::where($request->all(['id_bodega', 'id_producto', 'lote']))->first();

                        if (!$stockDevolucion) {
                            throw new Exception("Stock para devolucion no encontrado.");
                        }

                        $stockDevolucion->cantidad += $cantidad;

                        $stockDevolucion->save();

                        $movimiento = Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => Auth::id(),
                            'id_motivo' => MotivoMovimiento::$DEVOLUCION,
                            'recuento' => $cantidad,
                            'lote' => $request->get('lote')
                        ]);

                        MovimientosStockUsuarios::create([
                            'id_movimiento' => $movimiento->id,
                            'id_usuario_destino' => Auth::id(),
                            'id_stock_usuarios' => $stock->id
                        ]);

                        return $this->responseOK("Actualizado con exito.");

                    case MotivoMovimiento::$DESCOMPOSICION:

                        $stock->cantidad -= $cantidad;

                        $stock->save();

                        $movimiento = Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => Auth::id(),
                            'id_motivo' => MotivoMovimiento::$DESCOMPOSICION,
                            'recuento' => $cantidad,
                            'lote' => $request->get('lote')
                        ]);

                        MovimientosStockUsuarios::create([
                            'id_movimiento' => $movimiento->id,
                            'id_usuario_destino' => Auth::id(),
                            'id_stock_usuarios' => $stock->id
                        ]);

                        return $this->responseOK("Actualizado con exito.");
                    case MotivoMovimiento::$PERDIDA:

                        $stock->cantidad -= $cantidad;

                        $stock->save();

                        $movimiento = Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => Auth::id(),
                            'id_motivo' => MotivoMovimiento::$PERDIDA,
                            'recuento' => $cantidad,
                            'lote' => $request->get('lote')
                        ]);

                        MovimientosStockUsuarios::create([
                            'id_movimiento' => $movimiento->id,
                            'id_usuario_destino' => Auth::id(),
                            'id_stock_usuarios' => $stock->id
                        ]);

                        return $this->responseOK("Actualizado con exito.");
                    case MotivoMovimiento::$PRESTAMO:
                        if (!$request->has('id_usuario')) {
                            throw new Exception("Usuario no encontrado para el prestamo");
                        }

                        $stock->cantidad -= $cantidad;

                        $stock->save();

                        $stockPrestamo =StockUsuarios::where([['id_producto','=',$request->id_producto], ['lote',"=",$request->lote], ['id_usuario',"=",$request->id_usuario]])->first();
                        if (!$stockPrestamo) {
                            StockUsuarios::create([
                                'id_producto'=>$request->id_producto,
                                'lote'=>$request->lote,
                                "cantidad"=>$cantidad,
                                'id_usuario'=>$request->id_usuario,

                            ]);
                        }else{
                            $stockPrestamo->cantidad += $cantidad;
                            $stockPrestamo->save();
                        }

                        $stockPrestamo->cantidad += $cantidad;

                        $movimiento = Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => $stock->id_usuario,
                            'id_motivo' => MotivoMovimiento::$PRESTAMO,
                            'recuento' => $cantidad,
                            'lote' => $request->get('lote')
                        ]);

                        MovimientosStockUsuarios::create([
                            'id_movimiento' => $movimiento->id,
                            'id_usuario_destino' => $request->id_usuario,
                            'id_stock_usuarios' => $stock->id
                        ]);

                        return $this->responseOK("Actualizado con exito.");

                    case MotivoMovimiento::$TRASPASO:
                        if (!$request->has('id_usuario')) {
                            throw new Exception("Usuario no encontrado para el prestamo");
                        }
                        $stock->cantidad -= $cantidad;
                        $stock->save();
                        $stockPrestamo = StockUsuarios::where([
                            ['id_producto','=',$request->id_producto],
                            ['lote',"=",$request->lote],
                            ['id_usuario',"=",$request->id_usuario]
                        ])->first();

                        if (!$stockPrestamo) {
                            StockUsuarios::create([
                                'id_producto'=>$request->id_producto,
                                'lote'=>$request->lote,
                                "cantidad"=>$cantidad,
                                'id_usuario'=>$request->id_usuario,

                            ]);
                        }else{
                            $stockPrestamo->cantidad += $cantidad;
                            $stockPrestamo->save();
                        }
                        $movimiento = Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => $stock->id_usuario,
                            'id_motivo' => MotivoMovimiento::$TRASPASO,
                            'recuento' => $cantidad,
                            'lote' => $request->get('lote')
                        ]);

                        MovimientosStockUsuarios::create([
                            'id_movimiento' => $movimiento->id,
                            'id_usuario_destino' => $request->users["id"],
                            'id_stock_usuarios' => $stock->id
                        ]);

                        return $this->responseOK("Actualizado con exito.");

                    default:
                        throw new Exception("Error interno.");
                }
            });
        } catch (Exception $e) {
            logger($e->getMessage());
            return $this->respondInternalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StockUsuarios  $stockUsuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $stock = StockUsuarios::find($request->get('id'));

        $this->authorize('deleteAny', $stock);

        return $this->responseOK($stock->delete());
    }
}
