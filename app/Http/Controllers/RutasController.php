<?php

namespace App\Http\Controllers;

use App\Http\Resources\RutasResource;
use App\Models\Empresa;
use App\Models\EstadoRuta;
use App\Models\Inmuebles;
use App\Models\MotivoVisita;
use App\Models\Rutas;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class RutasController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Resources\Json\AnonymousResourceCollection|\Illuminate\Http\Response|\Illuminate\View\View
     */

    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return $this->responseOK(
                RutasResource::collection(Rutas::with(['cliente' => function ($query) {
                    return $query->with(['tipoCliente']);
                }, 'inmueble' => function ($query) {
                    return $query->with(['propietario']);
                }, 'motivoVisita', 'estadoRuta', 'user' => function ($query) {
                    return $query->with(['tecnicoACargo']);
                }])->where([['id_usuario', "=", Auth::id()], ["id_estado", "=", EstadoRuta::$VIGENTE]])->whereDate("horario_inicio", "=", Carbon::today()->toDateString())->get())
            );
        } else {
            $rutas = RutasResource::collection(Rutas::with(['cliente' => function ($query) {
                return $query->with(['tipoCliente']);
            }, 'inmueble' => function ($query) {
                return $query->with(['propietario']);
            }, 'motivoVisita', 'estadoRuta', 'user' => function ($query) {
                return $query->with(['tecnicoACargo']);
            }])->orderBy('horario_inicio', 'asc')->get());
            return view('rutas.index', compact("rutas"));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return MotivoVisita[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function create()
    {
        return MotivoVisita::all();
    }

    public function pendientes(Request $request)
    {
        return $this->paginate(Rutas::with(['cliente', 'inmueble' => function ($query) {
            return $query->with(['propietario']);
        }, 'motivoVisita', 'estadoRuta', 'user' => function ($query) {
            return $query->with(['tecnicoACargo']);
        }])->where("rutas.id_estado", "=", EstadoRuta::$VIGENTE)->get(), 10, $request->page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        if ($request->inmueble["id"] !== null) {
            Inmuebles::find($request->inmueble["id"])
                ->update([
                    'latitude' => $request->inmueble["latitude"],
                    'longitude' => $request->inmueble["longitude"]
                ]);
        }
        if (!is_array($request->horario["fecha"])) {
            $ValidateFields = Validator::make($request->all(), Rutas::Rules());
        } else {
            $ValidateFields = Validator::make($request->all(), Rutas::RulesArray());
        }

        if (!$ValidateFields->fails()) {
            $validateId = Validator::make($request->only("id"), Rutas::validateID());
            if ($validateId->fails()) {
                DB::beginTransaction();
                if (!is_array($request->horario["fecha"])) {
                    $topes= Rutas::where([
                        ['horario_inicio','=', $request->horario_inicio],
                        ['horario_fin','=',$request->horario_fin],
                        ['id_usuario',"=",$request->id_usuario]
                    ])->get();
                    if($topes->count()<=0){
                        $createValues = $request->except('id');
                        $createValues['codigo'] = Str::uuid();
                        $ruta = Rutas::create($createValues);
                        DB::commit();
                        return $this->show($ruta->id);
                    }else{
                        DB::rollBack();
                        return $this->respondWithError("Hay tope de horario en las fechas: ".implode(",", $topes->pluck('horario_inicio')->toArray()));
                    }

                } else {
                    $codigo = Str::uuid();
                    foreach ($request->horario["fecha"] as $fecha) {
                        $topes= Rutas::where([
                            ['horario_inicio','=',$fecha . ' ' . $request->horario_inicio],
                            ['horario_fin','=',$fecha . ' ' . $request->horario_fin],
                            ['id_usuario',"=",$request->id_usuario]
                        ])->get();
                        if($topes->count()<=0){
                            $ruta = Rutas::create(
                                ['id_usuario' => $request->id_usuario,
                                    'codigo' => $codigo,
                                    'id_cliente' => $request->id_cliente,
                                    'id_inmueble' => $request->id_inmueble,
                                    'id_motivo_visita' => $request->id_motivo_visita,
                                    'horario_inicio' => $fecha . ' ' . $request->horario_inicio,
                                    'horario_fin' => $fecha . ' ' . $request->horario_fin,
                                    'frecuency' => $request->frecuency,
                                    'id_estado' => $request->id_estado]);
                        }else{
                            DB::rollBack();
                            return $this->respondWithError("Hay tope de horario en las fechas: ".implode(",", $topes->pluck('horario_inicio')->toArray()));

                        }

                    }
                    DB::commit();
                    return $this->show($ruta->id);
                }

            } else {

                $this->update($request, $request->id);
                DB::commit();
                return $this->show($request->id);
            }
        } else {
            return $this->respondWithError($ValidateFields->errors());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->responseOK(
            RutasResource::collection(Rutas::with(['cliente', 'inmueble' => function ($query) {
                return $query->with(['propietario']);
            }, 'motivoVisita', 'estadoRuta', 'user' => function ($query) {
                return $query->with(['tecnicoACargo']);
            }])->where('id', $id)->get()));
    }

    public function showInMap($id)
    {
        $route = Rutas::with('inmueble')->find($id);
        $inicio = Rutas::with('inmueble')
            ->where('id_usuario', $route->id_usuario)
            ->where(DB::raw("(DATE_FORMAT(horario_inicio,'%Y-%m-%d'))"), Carbon::parse($route->horario_inicio)->format('Y-m-d'))
            ->where(DB::raw("(DATE_FORMAT(horario_inicio,'%H:%i:%s'))"), '<', Carbon::parse($route->horario_inicio)->format('H:i:s'))
            ->orderBy('horario_inicio', 'desc')
            ->limit(1)
            ->first();
        if (!$inicio) {
            $inicio = Empresa::find(1);
            $inicio = $inicio->location;
        }
        return ["from" => $inicio, "to" => $route];
    }

    public function showByDateLast($fecha, $idUser)
    {
        $ValidateFields = Validator::make(["id" => $idUser], User::validateID());
        if (!$ValidateFields->fails()) {
            $route = Rutas::with('inmueble')
                ->where('id_usuario', $idUser)
                ->where(DB::raw("(DATE_FORMAT(horario_inicio,'%Y-%m-%d'))"), Carbon::parse($fecha)->format('Y-m-d'))
                ->orderBy('horario_inicio', 'desc')
                ->first();
            if (!$route) {
                return ["from" => Empresa::find(1)->location];
            }
            return ["from" => ["inmueble" => ["direccion" => $route->inmueble->direccion, "ciudad" => $route->inmueble->ciudad]]];
        } else {
            return ["from" => Empresa::find(1)->location];
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->inmueble["id"] !== null) {
            Inmuebles::find($request->inmueble["id"])
                ->update([
                    'latitude' => $request->inmueble["latitude"],
                    'longitude' => $request->inmueble["longitude"]
                ]);
        }
        DB::beginTransaction();
        $ruta = Rutas::find($id);

            if ($request->toAllRoutes&&is_array($request->horario["fecha"])) {

                $rutas = Rutas::where([['codigo', "=", $request->codigo], ['id_estado', '=', 1]])
                    ->orderBy('horario_inicio', 'asc')->get();
                $dif = count($request->horario["fecha"]) - Rutas::where([['codigo', "=", $request->codigo], ['id_estado', '=', 1]])->count();
                $fechas = new Collection($request->horario["fecha"]);

                foreach ($rutas as $key => $val) {
                    if ($fechas->has($key)) {
                        $topes= Rutas::where([
                            ['horario_inicio','=',$fechas[$key] . ' ' . $request->horario_inicio],
                            ['horario_fin','=',$fechas[$key] . ' ' . $request->horario_fin],
                            ['id_usuario',"=",$request->id_usuario]
                        ])->get();
                        if($topes->count()<=0){
                            Rutas::find($val->id)->update([
                                'horario_inicio' => $fechas[$key] . ' ' . $request->horario_inicio,
                                'horario_fin' => $fechas[$key] . ' ' . $request->horario_fin,
                                'frecuency' => $request->horario["frecuency"],
                                'updated_at' => Carbon::now()
                            ]);
                            $fechas->forget($key);
                        }else{
                           DB::rollBack();
                           return $this->respondWithError("Hay tope de horario en las fechas: ".implode(",", $topes->pluck('horario_inicio')->toArray()));
                        }

                    } else {
                        Rutas::destroy($val->id);
                    }
                }
                if ($dif > 0) {
                    foreach ($fechas as $fecha) {
                        $topes= Rutas::where([
                            ['horario_inicio','=',$fecha . ' ' . $request->horario_inicio],
                            ['horario_fin','=',$fecha . ' ' . $request->horario_fin],
                            ['id_usuario',"=",$request->id_usuario]
                        ])->get();
                        if($topes->count()<=0){
                            Rutas::create([
                                'id_usuario' => $request->id_usuario,
                                'codigo' => $ruta->codigo,
                                'id_cliente' => $request->id_cliente,
                                'id_inmueble' => $request->id_inmueble,
                                'id_motivo_visita' => $request->id_motivo_visita,
                                'horario_inicio' => $fecha . ' ' . $request->horario_inicio,
                                'horario_fin' => $fecha . ' ' . $request->horario_fin,
                                'frecuency' => $request->frecuency,
                                'id_estado' => $request->id_estado,
                                'created_at' => Carbon::now()
                            ]);
                        }else{
                            DB::rollBack();
                            return $this->respondWithError("Hay tope de horario en las fechas: ".implode(",", $topes->pluck('horario_inicio')->toArray()));
                        }

                    }
                }

            } else {
                if ($request->horario["frecuency"] != $ruta->frecuency&&is_array($request->horario["fecha"])) {
                    $codigo = Str::uuid();
                    foreach ($request->horario["fecha"] as $fecha) {
                        $topes= Rutas::where([
                            ['horario_inicio','=',$fecha . ' ' . $request->horario_inicio],
                            ['horario_fin','=',$fecha . ' ' . $request->horario_fin],
                            ['id_usuario',"=",$request->id_usuario]
                        ])->get();
                        if($topes->count()<=0){
                            $ruta = Rutas::create(
                                ['id_usuario' => $request->id_usuario,
                                    'codigo' => $codigo,
                                    'id_cliente' => $request->id_cliente,
                                    'id_inmueble' => $request->id_inmueble,
                                    'id_motivo_visita' => $request->id_motivo_visita,
                                    'horario_inicio' => $fecha . ' ' . $request->horario_inicio,
                                    'horario_fin' => $fecha . ' ' . $request->horario_fin,
                                    'frecuency' => $request->frecuency,
                                    'id_estado' => $request->id_estado]);
                        }else{
                            DB::rollBack();
                            return $this->respondWithError("Hay tope de horario en las fechas: ".implode(",", $topes->pluck('horario_inicio')->toArray()));

                        }

                    }
                }else{
                    if (!is_array($request->horario["fecha"])) {
                        $topes= Rutas::where([
                            ['horario_inicio','=',$request->horario["fecha"] . ' ' . $request->horario_inicio],
                            ['horario_fin','=',$request->horario["fecha"] . ' ' . $request->horario_fin],
                            ['id_usuario',"=",$request->id_usuario]
                        ])->get();
                        if($topes->count()<=0){
                            $ruta->update($request->except('id', 'horario', 'estado_ruta', 'inmueble', 'motivo_visita', 'user', 'cliente', 'withFrecuency', 'toAllRoutes'));

                        }else{
                            DB::rollBack();
                            return $this->respondWithError("Hay tope de horario en las fechas: ".implode(",", $topes->pluck('horario_inicio')->toArray()));

                        }
                    } else {
                        $topes= Rutas::where([
                            ['horario_inicio','=',$request->horario["fecha"][0] . ' ' . $request->horario_inicio],
                            ['horario_fin','=',$request->horario["fecha"][0] . ' ' . $request->horario_fin],
                            ['id_usuario',"=",$request->id_usuario]
                        ])->get();
                        if($topes->count()<=0) {
                            $ruta->update(
                                ['id_usuario' => $request->id_usuario,
                                    'id_cliente' => $request->id_cliente,
                                    'id_inmueble' => $request->id_inmueble,
                                    'id_motivo_visita' => $request->id_motivo_visita,
                                    'horario_inicio' => $request->horario["fecha"][0] . ' ' . $request->horario_inicio,
                                    'horario_fin' => $request->horario["fecha"][0] . ' ' . $request->horario_fin,
                                    'frecuency' => $request->frecuency,
                                    'id_estado' => $request->id_estado]
                            );
                        }else{
                            DB::rollBack();
                            return $this->respondWithError("Hay tope de horario en las fechas: ".implode(",", $topes->pluck('horario_inicio')->toArray()));

                        }
                    }
                }
            }

        DB::commit();
        return $this->show($ruta->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rutas::destroy($id);
    }
}
