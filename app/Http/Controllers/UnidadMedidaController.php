<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\UnidadMedida;
use Illuminate\Http\Request;

class UnidadMedidaController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', UnidadMedida::class);

        if ($request->has('producto') && $request->wantsJson()) {
            return $this->responseOK(UnidadMedida::with('productos')->where('producto.id','=',$request->get('producto')));
        }

        $unidad_medida = UnidadMedida::all();
        return $request->wantsJson() ?
            $this->responseOK($unidad_medida) :
            view('mantenedores.unidad_medida', compact('unidad_medida'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', UnidadMedida::class);
        if (UnidadMedida::create($request->only('nombre', 'abreviatura'))) {
            return $this->responseOK([]);
        } else {
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unidadMedida = UnidadMedida::find($id);
        $this->authorize('view', $unidadMedida);
        return $unidadMedida;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $unidadMedida = UnidadMedida::find($request->id);

        $this->authorize('update', $unidadMedida);

        try {
            if ($unidadMedida->update($request->only('nombre', 'abreviatura'))) {
                return $this->responseOK($unidadMedida->get());
            } else {
                return $this->respondFailedParametersValidation();
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $unidadMedida = UnidadMedida::find($request->id);
        $this->authorize('delete', $unidadMedida);
        return $this->responseOK($unidadMedida->delete());
    }
}
