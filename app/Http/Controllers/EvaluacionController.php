<?php

namespace App\Http\Controllers;

use App\Models\Evaluacion;
use Illuminate\Http\Request;

class EvaluacionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return Evaluacion[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Evaluacion::class);
        $valor_evaluacion=Evaluacion::all();
        if($request->wantsJson()){
            return $this->responseOK(Evaluacion::all());
        }
        return view('mantenedores.valor_evaluacion',compact('valor_evaluacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Evaluacion::class);

        return $this->responseOK(Evaluacion::create($request->only('evaluacion_textual', 'peso')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('viewAny', Evaluacion::class);
        return Evaluacion::where('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request)
    {
        $evaluacion = Evaluacion::find($request->get('id'));

        $this->authorize('update', $evaluacion);

        return $this->responseOK($evaluacion->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $evaluacion = Evaluacion::find($request->get('id'));
        $this->authorize('delete', $evaluacion);
        return $this->responseOK($evaluacion->delete());

    }
}
