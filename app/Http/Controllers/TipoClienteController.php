<?php

namespace App\Http\Controllers;

use App\Models\TipoCliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TipoClienteController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return TipoCliente[]|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if($request->wantsJson()){
            return TipoCliente::all();
        }
        return view('mantenedores.tipo_cliente',['tipo_cliente'=>TipoCliente::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate=Validator::make($request->all(),['restrict_payment'=>'required',"tipo"=>'required',"color"=>'unique:tipo_cliente,color',"impuesto"=>'required|numeric']);
        if(!$validate->fails()){
            TipoCliente::create($request->all());
        }else{
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return TipoCliente::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validate=Validator::make($request->all(),["impuesto"=>'required|numeric','id'=>'exists:tipo_cliente,id','restrict_payment'=>'required',"tipo"=>'required',"color"=>'unique:tipo_cliente,color']);
        if(!$validate->fails()){
            TipoCliente::where('id',$request->id)
                ->update([
                    'color'=>$request->color,
                    'restrict_payment'=>$request->restrict_payment,
                    'tipo'=>$request->tipo,
                    'impuesto'=>$request->impuesto
                ]);
        }else{
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator=  $validator = Validator::make(["id"=>$request->id], [
            'id'=>'numeric|required|exists:tipo_cliente,id',
        ]);
        if(!$validator->fails()){
            TipoCliente::destroy($request->id);
            return $this->responseOK([]);
        }else{
            return $this->respondFailedParametersValidation();
        }

    }
}
