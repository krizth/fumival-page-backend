<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return Empresa[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {

        $this->authorize('viewAny', Empresa::class);
        $empresa = Empresa::all();
        foreach ($empresa as $emp){
            if(!empty($emp->logo)){
                $emp->logo=$this->handleImages(true,$emp->logo);
            }
        }
        return view('mantenedores.empresa', compact('empresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Empresa::class);
        $logo = [];
        if ($request->file('logo')) {
            $logo = ["logo" => $request->file('logo')->store("images")];
        }
        return $this->responseOK(Empresa::create(array_merge($request->only(
            'nombre',
            'rut',
            'direccion',
            'correo',
            'fono',
            'pagina_web',
            'otros'
        ), $logo)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('anyView', Empresa::class);
        return Empresa::where('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request)
    {
        $logo = [];
        if ($request->file('logo')) {
            $logo = ["logo" => $request->file('logo')->store("images")];
        }
        $empresa = Empresa::find($request->id);

        $this->authorize('update', $empresa);

        $this->responseOK($empresa->update(array_merge($request->only(
            'nombre',
            'rut',
            'direccion',
            'correo',
            'fono',
            'pagina_web',
            'otros'
        ), $logo)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $empresa = Empresa::find($request->get('id'));

        $this->authorize('delete', $empresa);

        return $this->responseOK($empresa->delete());
    }
}
