<?php

namespace App\Http\Controllers;

use App\Models\Configuracion;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return Role[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $this->authorize('viewAny', Role::class);
        $roles = Role::all();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Role::class);
        return Configuracion::first()->default_permissions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Role::class);
        $role = Role::create($request->except(["id"]));
        if ($role) {
            return $this->responseOK([$role]);
        } else {
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->authorize('update', Role::class);

        try {

            if (Role::where('id', $request->id)->update($request->except('id'))) {
                return $this->responseOK([Role::where('id', $request->id)->get()]);
            } else {
                return $this->respondFailedParametersValidation();
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request)
    {
        $this->authorize('delete', Role::class);
        return $this->responseOK(["id" => $request->id, "dbResponse" => Role::destroy($request->id)]);
    }
}
