<?php

namespace App\Http\Controllers;

use App\Models\GlosaEvaluacion;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GlosaEvaluacionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return GlosaEvaluacion[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', GlosaEvaluacion::class);

        if($request->wantsJson()){
            return $this->responseOK( GlosaEvaluacion::all());
        }
        $glosa_evaluacion=  GlosaEvaluacion::all();
        return view('mantenedores.glosas_evaluacion',compact('glosa_evaluacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', GlosaEvaluacion::class);
        if($request->wantsJson()){
            return $this->responseOK(GlosaEvaluacion::create($request->only('criterio')));
        }
        if( GlosaEvaluacion::create($request->only('criterio'))){

            return $this->responseOK([]);
        }else{
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('anyView', GlosaEvaluacion::class);
        $glosa_evaluacion= GlosaEvaluacion::find($id);
        return view('mantenedores.glosas_evaluacion',compact('glosa_evaluacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request)
    {
        $glosaEvaluacion = GlosaEvaluacion::find($request->id);

        $this->authorize('update', $glosaEvaluacion);
        try{
            if($glosaEvaluacion->update($request->only('criterio'))){
                return $this->responseOK(GlosaEvaluacion::where('id', $request->id)->get());
            }else{
                return $this->respondFailedParametersValidation();
            }
        }catch(\Illuminate\Database\QueryException $e){
            return $this->respondFailedParametersValidation();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $globalEvaluacion=GlosaEvaluacion::find($request->id);
        $this->authorize('delete', $globalEvaluacion);
        return $this->responseOK($globalEvaluacion->delete());
    }
}
