<?php

namespace App\Http\Controllers;

use App\Models\MotivoMovimiento;
use App\Models\Movimientos;
use Illuminate\Http\Request;

class MovimientosController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
         return
             Movimientos::with(['motivo_movimiento','movimiento_stock_usuarios'=>function($query){
                 return $query->with("users:nombre,id,rut");
             },'users'=>function($query){
                 return $query->select("id","nombre","rut");
             },'producto'=>function($query){
                 return $query->select("id","nombre","id_unidad_minima")->with('unidadMedida');
             }])->orderBy("movimientos.created_at","desc")->get()
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return MotivoMovimiento::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
