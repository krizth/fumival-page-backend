<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

class ImagenesController extends ApiController
{
   public function index (Request $request,$filename)
    {
       return $this->handleImages($request->wantsJson(),$filename);
    }
}
