<?php

namespace App\Http\Controllers;

use App\Http\Resources\InmueblesResource;
use App\Models\Inmuebles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InmueblesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $this->authorize('viewAny', Inmuebles::class);

        $inmuebles = InmueblesResource::collection(
            Inmuebles::with(['propietario'])->get()
        );

        if ($request->wantsJson()) {
            return $this->responseOK($inmuebles);
        }

        return view('inmuebles.index', compact('inmuebles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Inmuebles::class);

        if (isset($request->id)) {
            $inmueble = Inmuebles::updateOrCreate(
                ["id" => $request->id],
                [
                    "ciudad" => $request->ciudad,
                    "direccion" => $request->direccion,
                    "nombre" => $request->nombre,
                    "id_propietario" => $request->propietario["id"]
                ]
            );
        } else {
            $inmueble = Inmuebles::updateOrCreate(
                ["id" => $request->id],
                [
                    "ciudad" => $request->ciudad,
                    "direccion" => $request->direccion,
                    "nombre" => $request->nombre,
                    "id_propietario" => $request->id_propetario==null?$request->propietario["id"]:$request->id_propetario
                ]
            );
        }
        if (!empty($inmueble)) {
            try {
                return $this->responseOK(InmueblesResource::collection(
                    Inmuebles::select("id", "id_propietario", "nombre", "direccion", "ciudad")->with(['propietario' => function ($query) {
                        return $query->select("id", "nombre", "rut", "direccion_principal", "ciudad", "nombre_fantasia");
                    }])->where('id', $inmueble->id)->get()
                )[0]);
            } catch (\Exception $e) {
                return $this->respondWithError("Error al Actualizar o Guardar");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Inmuebles::class);
        return Inmuebles::with('propietario')->find($id);
    }

    public function inmueblesPropietario(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required',
        ]);
        if (((!$validator->fails()) || $request->wantsJson())&&Inmuebles::where('id_propietario',$id)->first()!==null) {
            return $this->responseOK(
                InmueblesResource::collection(
                    Inmuebles::select("id", "id_propietario", "nombre", "direccion", "ciudad")->with(['propietario' => function ($query) {
                        return $query->select("id", "nombre", "rut", "direccion_principal", "ciudad", "nombre_fantasia");
                    }])->where([['inmuebles.' . $request->field, 'like', '%' . $request->search . '%'],['inmuebles.id_propietario',"=",$id]])->get()
                )
            );
        } else {
            if(Inmuebles::where('id_propietario',$id)->first()!==null){
                return $this->responseOK(Inmuebles::where('id_propietario',$id));
            }
            return $this->find($request);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function find(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required',
        ]);
        if ((!$validator->fails()) || $request->wantsJson()) {
            return $this->responseOK(
                InmueblesResource::collection(
                    Inmuebles::select("id", "id_propietario", "nombre", "direccion", "ciudad")->with(['propietario' => function ($query) {
                        return $query->select("id", "nombre", "rut", "direccion_principal", "ciudad", "nombre_fantasia");
                    }])->where('inmuebles.' . $request->field, 'like', '%' . $request->search . '%')->get()
                )
            );
        } else {
            return $this->responseOK(Inmuebles::all());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $inmueble = Inmuebles::find($request->id);

        $this->authorize('update', $inmueble);

        return $this->responseOK($inmueble->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $inmueble = Inmuebles::find($request->id);

        $this->authorize('delete', $inmueble);

        return $this->responseOK($inmueble->delete());
    }
}
