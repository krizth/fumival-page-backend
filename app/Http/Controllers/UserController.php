<?php

namespace App\Http\Controllers;

use App\Models\Configuracion;
use App\Models\Notifications;
use App\Models\Role;
use App\User;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends ApiController
{


    public function allUsers(){
        $this->authorize('viewAny', User::class);

        return User::all();
    }

    public function EditUser(Request $request)
    {
        $this->authorizeResource(User::class, 'update');
        try {

            $user = User::where('id', Auth::id())->first();
            $user->nombre = $request->nombre;
            $user->email = $request->email;
            if ($request->firma != "") {
                $user->firma = $request->firma;
            }
            if ($request->password != "") {
                if ($request->password == $request->password_confirm) {
                    $check  = Auth::guard('web')->attempt(
                        [
                            'rut' => Auth::user()->rut,
                            'password' => $request->old
                        ],
                        false
                    );

                    if ($check) {
                        $user->password = Hash::make($request->password);
                    } else {
                        return $this->respondFailedParametersValidation();
                    }
                } else {
                    return $this->respondFailedParametersValidation();
                }
            }
            $user->save();

            return $this->responseOK(User::where('id', Auth::id())->first());
        } catch (\Illuminate\Database\QueryException $e) {

            return $this->respondFailedParametersValidation();
        }
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);

        $this->authorize('update', $user);
        if($user->id!==$request->tec_a_cargo){
            $user->update($request->only(['nombre', 'email', 'firma', 'rut', 'patente', 'tec_a_cargo']));
            return $this->responseOK($user);
        }
        return $this->respondFailedParametersValidation();
    }

    public function store(Request $request)
    {
        $this->authorize('create', User::class);

        try {
            DB::transaction(function () use ($request) {
                $request->merge(['password' => Hash::make($request->input('password'))]);
                if(!$request->tec_a_cargo){
                    //default tec_a_cargo
                    $request->merge(["tec_a_cargo"=>1]);
                }
                $user = User::create($request->only(['nombre', 'email', 'firma', 'rut', 'patente', 'password', 'tec_a_cargo']));

                $user->roles()->attach($request->get('role'));

                return $this->responseOK($user);
            });
        } catch (Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function delete(Request $request)
    {
        $user = User::find($request->id);

        $this->authorize('delete', $user);

        return $this->responseOK(["id" => $request->id, "dbResponse" => $user->delete()]);
    }

    public function create()
    {
        $this->authorize('create', User::class);
        return $this->responseOK([Role::all()]);
    }

    public function index(Request $request)
    {
        $this->authorize('viewAny', User::class);

        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required|min:2',
        ]);

        if ($request->wantsJson() || !$validator->fails()) {
          if(!$request->all){
              return $this->responseOK(User::select("id", "nombre", "rut", "email")
                  ->where([
                      [$request->field, 'like', '%' . $request->search . '%'],
                      ["id", "<>", Auth::id()]
                  ])->get());
          }else{
              return $this->responseOK(User::select("id", "nombre", "rut", "email")
                  ->where([
                      [$request->field, 'like', '%' . $request->search . '%']
                  ])->get());
          }
        }
        $configPermissions=Configuracion::first();
        $configPermissions=$configPermissions->default_permissions;
        $roles=Role::all();
        foreach ($roles as $role){
           $permissionDiff= array_diff_key($configPermissions,$role->permissions);
           foreach ($permissionDiff as $key=>$dif)$permissionDiff[$key]['allowed']="false";
            Role::where('id',$role->id)->update(['permissions'=>array_merge($role->permissions,$permissionDiff)]);
        }
        return view('users.index', ["users" => User::selectRaw("users.id,
        users.nombre,
        users.email,
        users.rut,
        users.patente,
        users.firma,
        case users.isAdmin
        when 1 then 'Si'
        when 0 then 'No'
        end as isAdmin,
        users.tec_a_cargo")->with(['roles', 'tecnicoACargo'])->get()]);
    }

    public function getUser(Request $request)
    {
        return $request->user();
    }
    public function updateFCMToken(Request $request){
        $this->authorize('view',Notifications::class);
        $validation_token=Validator::make(['fcm_token'=>$request->fcm_token],[
            'fcm_token'=>'required'
        ]);
        if($validation_token->failed()){
            return $this->respondBadRequest();
        }else{
           return $this->saveToken($request);
        }

    }
    public function find(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required',
        ]);
        if ((!$validator->fails()) || $request->wantsJson()) {
            return $this->responseOK(User::where('users.' . $request->field, 'like', '%' . $request->search . '%')->get());
        } else {
            return $this->responseOK(User::all());
        }
    }
    public function update_location(Request $request)
    {
        try {
            User::where('id', Auth::id())->update(['location' => $request->all()]);
            return $this->responseCreated();
        } catch (\Exception $ex) {
            return $this->respondFailedParametersValidation();
        }
    }
}
