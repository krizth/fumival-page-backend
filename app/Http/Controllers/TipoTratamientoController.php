<?php

namespace App\Http\Controllers;

use App\Models\TipoTratamiento;
use Illuminate\Http\Request;

class TipoTratamientoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return TipoTratamiento[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', TipoTratamiento::class);
        $tipo_tratamiento=TipoTratamiento::all();
        return $request->wantsJson()?$this->responseOK($tipo_tratamiento):
            view('mantenedores.tipo_tratamiento',compact('tipo_tratamiento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', TipoTratamiento::class);
        if($request->wantsJson()){
            $tipo_trat=TipoTratamiento::create($request->only('nombre', 'abreviatura','comentario_fijo'));
            return $this->responseOK($tipo_trat);
        }
        if(TipoTratamiento::create($request->only('nombre', 'abreviatura')) ){
            return $this->responseOK([]);
        }else{
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('anyView', TipoTratamiento::class);
        return TipoTratamiento::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $tipoTratamiento = TipoTratamiento::find($request->get('id'));
        $this->authorize('update', $tipoTratamiento);
        return $this->responseOK($tipoTratamiento->update($request->all()));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $tipoTratamiento = TipoTratamiento::find($request->get('id'));
        $this->authorize('delete', $tipoTratamiento);
        return $this->responseOK($tipoTratamiento->delete());
    }
}
