<?php

namespace App\Http\Controllers;

use App\Models\SolicitudTraspaso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SolicitudTraspasoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return SolicitudTraspaso[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->wantsJson()){
            return $this->responseOK(["user_request_to_autorize"=>SolicitudTraspaso::pendientesAutorizacionUsuarioActual(),"current_user_request"=>SolicitudTraspaso::solicitudesUsuarioActual()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function unauthorizeTransfer(Request $request)
    {
        if(SolicitudTraspaso::desautorizarTraspaso($request->token)){
            return $this->responseOK([]);
        }else{
            return $this->respondWithError("Ocurrio un error al desautorizar");
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->responseOK(SolicitudTraspaso::solicitarTraspaso($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SolicitudTraspaso  $solicitudTraspaso
     * @return \Illuminate\Http\Response
     */
    public function show(SolicitudTraspaso $solicitudTraspaso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SolicitudTraspaso  $solicitudTraspaso
     * @return \Illuminate\Http\Response
     */
    public function edit(SolicitudTraspaso $solicitudTraspaso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SolicitudTraspaso  $solicitudTraspaso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SolicitudTraspaso $solicitudTraspaso)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SolicitudTraspaso  $solicitudTraspaso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(SolicitudTraspaso::cancelarSolicitud($request->tokens)){
            $this->responseOK([]);
        }else{
            $this->respondWithError("No se pudo completar la transaccion");
        }

    }
}
