<?php

namespace App\Http\Controllers;

use App\Models\Bodega;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BodegaController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Bodega::class);

        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required|min:2',
        ]);
        if (!$validator->fails()||$request->wantsJson()) {
            return $this->responseOK(Bodega::where($request->field,'like','%'.$request->search.'%')->get());
        }

        return view('mantenedores.bodega', ['bodegas' => Bodega::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Bodega::class);

        print_r($request->all());

        return $this->responseOK(Bodega::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function show(Bodega $bodega)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function edit(Bodega $bodega)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $bodega = Bodega::find($request->get('id'));

        $this->authorize('update', $bodega);

        return $this->responseOK($bodega->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bodega  $bodega
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $bodega = Bodega::find($request->get('id'));

        $this->authorize('delete', $bodega);

        return $this->responseOK($bodega->delete());
    }
}
