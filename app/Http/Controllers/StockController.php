<?php

namespace App\Http\Controllers;

use App\Models\FactorConversion;
use App\Models\MotivoMovimiento;
use App\Models\Movimientos;
use App\Models\Producto;
use App\Models\Stock;
use App\Models\StockUsuarios;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StockController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Stock::class);
        //$stock = Stock::with(['productos'])->get();
        //$unidadMedida = UnidadMedida::all();
        $stock['productos'] = Stock::with(['bodega', 'producto' => function ($query) {
            return $query->selectRaw("
                                id,
                                id_unidad_minima,
                                nombre,
                                codigo,
                                precio_compra,
                                precio_distribucion,
                                ing_activo,
                                compuesto,
                                regISP,
                                marca,
                                CASE maquinaria
                                WHEN 0 THEN
                                    'No'
                                WHEN 1 THEN
                                    'Si'
                                ELSE
                                    'Error'
                                END as maquinaria,
                                descripcion,
                                ubicacion")->with('factorConversions');
        }, 'producto.unidadMedida'])->get();
        $stock['usuarios'] = StockUsuarios::with(['users', 'producto' => function ($query) {
            return $query->with('factorConversions');
        }, 'producto.unidadMedida'])->get();

        if ($request->wantsJson()) {
            return $this->responseOK(compact('stock'));
        }

        return view('stock.index', compact('stock'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Stock::class);
        $stock = Stock::where($request->all(['id_bodega', 'id_producto', 'lote']))->first();
        $cantidad = $request->get('cantidadConvertida') != 0 ? $request->get('cantidadConvertida') : $request->get('cantidad');
        $prod=Producto::with('presets')->find($request->id_producto);

        try {
            DB::beginTransaction();
                if (!$stock) {
                    Stock::create(array_merge($request->all(), ['cantidad' => $cantidad]));


                } else {
                    $stock->cantidad += $cantidad;
                    $stock->save();
                }
                if($prod->compuesto){
                    foreach ($prod->presets as $componente) {
                        $compuesto = Stock::where([
                            ['id_producto' ,"=", $componente['id_compuesto']],
                            ["id_bodega","=",$request->id_bodega]
                        ])->where('cantidad', '>', 0)->orderBy('created_at','desc')->first();
                        if (empty($compuesto)) {
                            throw new Exception("Sin stock disponible: ".Producto::find($componente["id_compuesto"])->nombre);
                        }

                        $compuesto->cantidad-= $componente->cantidad*$request->cantidad;
                        $compuesto->save();

                    }
                }
            $mov=Movimientos::create([
                'id_producto' => $request->get('id_producto'),
                'id_usuario_origen' => Auth::id(),
                'id_motivo' => MotivoMovimiento::$INGRESO,
                'recuento' => $request->get('cantidad')
            ]);
                if($mov->id){
                    DB::commit();
                    return $this->responseOK("Creado y/o actualizado con exito.");
                }else{
                    throw new Exception("Ocurrio un error al guardar el movimiento ");
                }


          //  });
        } catch (Exception $e) {
            logger($e->getMessage());
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function porExpirar(){
        return Stock::with('producto')->where('caducidad',"<>",null)->orderBy('stock.caducidad','asc')->get();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $stock = Stock::find($request->get('id'));

        if (!$stock) {
            return $this->respondNotFound("No existe el stock.");
        }

        if($request->factor){
            $factor=FactorConversion::find($request->factor);
            $factor=$factor->factor_conversion;
        }else{
            $factor=1;
        }

        if (!$request->motivo_movimiento) {
            return $this->respondBadRequest("Debe tener al menos un motivo.");
        }

        try {
            DB::transaction(function () use($request, $stock,$factor){
                switch ($request->get('motivo_movimiento')) {
                    case MotivoMovimiento::$INGRESO:
                        return $this->respondBadRequest();
                    case MotivoMovimiento::$TRASPASO: {
                            if ($request->get('id_bodega') == $stock->id_bodega) {
                                return $this->respondBadRequest("En un traspaso las bodegas no pueden ser iguales");
                            }

                            $saldo = $stock->cantidad - $request->get('cantidad')*$factor;

                            if ($saldo < 0) {
                                return $this->respondWithError("Saldo insuficiente.");
                            }

                            $stock->cantidad = $saldo;

                            $stock->save();

                            $stockTraspaso = Stock::where($request->all(['id_bodega', 'id_producto', 'lote']))->first();

                            $cantidad = $request->get('cantidad')*$factor;

                            if (!$stockTraspaso) {
                                Stock::create(array_merge($request->all(['id_producto', 'id_bodega', 'cantidad', 'lote', 'caducidad']), ['cantidad' => $cantidad]));
                            } else {
                                $stockTraspaso->cantidad += $cantidad;
                                $stockTraspaso->save();
                            }

                            Movimientos::create([
                                'id_producto' => $request->get('id_producto'),
                                'id_usuario_origen' => Auth::id(),
                                'id_motivo' => MotivoMovimiento::$TRASPASO,
                                'recuento' => $request->get('cantidad')*$factor,
                                'lote' => $request->get('lote')
                            ]);

                            return $this->responseOK("Actualizado con exito.");
                        }
                    case MotivoMovimiento::$AJUSTE: {
                            if ($request->get('id_bodega') != $stock->id_bodega) {
                                return $this->respondBadRequest("En un ajuste las bodegas deben ser iguales");
                            }

                            $stock->cantidad = $request->cantidad*$factor;

                            $stock->save();

                            Movimientos::create([
                                'id_producto' => $request->get('id_producto'),
                                'id_usuario_origen' => Auth::id(),
                                'id_motivo' => MotivoMovimiento::$AJUSTE,
                                'recuento' => $request->get('cantidad')*$factor,
                                'lote' => $request->get('lote')
                            ]);

                            return $this->responseOK("Actualizado con exito.");
                        }
                    case MotivoMovimiento::$DEVOLUCION:
                        if ($request->get('id_bodega') == $stock->id_bodega) {
                            return $this->respondBadRequest("En una devolución las bodegas no pueden ser iguales");
                        }

                        $stock->cantidad -= $request->get('cantidad');

                        $stock->save();

                        $stockDevolucion = Stock::where($request->all(['id_bodega', 'id_producto', 'lote']))->first();

                        if (!$stockDevolucion) {
                            return $this->respondNotFound("Stock para devolucion no encontrado");
                        }

                        $stockDevolucion->cantidad += $request->get('cantidad')*$factor;

                        $stockDevolucion->save();

                        Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => Auth::id(),
                            'id_motivo' => MotivoMovimiento::$DEVOLUCION,
                            'recuento' => $request->get('cantidad')*$factor,
                            'lote' => $request->get('lote')
                        ]);

                        return $this->responseOK("Actualizado con exito.");

                    case MotivoMovimiento::$DESCOMPOSICION:
                        if ($request->get('id_bodega') !== $stock->id_bodega) {
                            return $this->respondBadRequest("En una descomposicion las bodegas no pueden ser distintas");
                        }

                        $stock->cantidad -= $request->get('cantidad')*$factor;

                        $stock->save();

                        Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => Auth::id(),
                            'id_motivo' => MotivoMovimiento::$DESCOMPOSICION,
                            'recuento' => $request->get('cantidad')*$factor,
                            'lote' => $request->get('lote')
                        ]);


                        return $this->responseOK("Actualizado con exito.");
                    case MotivoMovimiento::$PERDIDA:
                        if ($request->get('id_bodega') !== $stock->id_bodega) {
                            return $this->respondBadRequest("En una perdida las bodegas no pueden ser distintas");
                        }

                        $stock->cantidad -= $request->get('cantidad')*$factor;

                        $stock->save();

                        Movimientos::create([
                            'id_producto' => $request->get('id_producto'),
                            'id_usuario_origen' => Auth::id(),
                            'id_motivo' => MotivoMovimiento::$PERDIDA,
                            'recuento' => $request->get('cantidad')*$factor,
                            'lote' => $request->get('lote')
                        ]);


                        return $this->responseOK("Actualizado con exito.");
                    default:
                        return $this->respondInternalError();
                }
            });
        } catch (Exception $e) {
            logger($e->getMessage());
            return $this->respondInternalError();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $stock = Stock::find($request->get('id'));
        $this->authorize('delete', $stock);

        return $this->responseOK($stock->delete());
    }
}
