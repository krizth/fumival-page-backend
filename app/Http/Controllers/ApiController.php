<?php

namespace App\Http\Controllers;

use App\Models\Notifications;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use File;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    /**
     * @var int
     */
    protected $statusCode = 200;


    /**
     * @var string
     */
    private  $GoogleNotificationCenter = 'https://fcm.googleapis.com/fcm/send';

    /**
     * @return mixed
     */
    private $serverKey = '';


    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param $data
     * @param array $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function respond($data, $headers = [])
    {
        return response($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function respondWithError($message)
    {
        return $this->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR)
            ->respond([
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]);
    }

    /**
     * @param $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function responseCreated($data = [], $message = 'Created')
    {
        return $this->setStatusCode(\Symfony\Component\HttpFoundation\Response::HTTP_CREATED)
            ->respond([
                'message' => $message,
                'data'    => $data
            ]);
    }

    /**
     * @param string $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function responseOK($data)
    {
        return $this->setStatusCode(Response::HTTP_OK)
            ->respond(['data' => $data]);
    }

    /**
     * @param $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function responseNoContent()
    {
        return $this->setStatusCode(Response::HTTP_NO_CONTENT)->respond([]);
    }

    /**
     * @param string $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function respondBadRequest($message = 'Se ha producido un error.')
    {
        return $this->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function respondFailedParametersValidation($message = 'Los parámetros ingresados no son válidos.')
    {
        return $this->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function respondNotFound($message = 'No Encontrado!')
    {
        return $this->setStatusCode(Response::HTTP_NOT_FOUND)
            ->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function respondInternalError($message = 'Se ha producido un error interno.!')
    {
        return $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
            ->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function respondForbidden($message = 'Acceso prohibido.')
    {
        return $this->setStatusCode(Response::HTTP_FORBIDDEN)
            ->respondWithError($message);
    }

    public  function handleImages($isBase64, $filename)
    {

        if(empty($filename)){
            return "";
        }

        if (!$isBase64) {
            $path = storage_path('app/images/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = \Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        } else {
            $path = storage_path('app/' . $filename);
            $type = File::mimeType($path);
            return 'data:' . $type . ';base64,' . base64_encode(file_get_contents($path));
        }
    }
    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function getNotifications($quantity)
    {
        if(is_numeric($quantity)){
            if ($quantity == 0) return $this->responseOK(Notifications::where('id_user', Auth::id())->orderBy('id','desc')->get());
            if ($quantity > 0) return $this->responseOK(Notifications::where('id_user', Auth::id())->take($quantity)->orderBy('id','desc')->get());
        }
        if($quantity=='all') return $this->responseOK(Notifications::withTrashed()->where('id_user', Auth::id())->orderBy('id','desc')->get());

    }
    protected function storeNotification($data)
    {
        Notifications::create($data);
    }
    public function saveToken(Request $request)
    {
        $user = User::find(Auth::id());
        $user->fcm_token = $request->fcm_token;
        $user->save();
        if ($user) {
            return $this->responseCreated();
        }
        return $this->respondWithError("No se han activado las notificaciones");
    }
    protected function sendPush($Notification, $Data,$url, $DeviceToken)
    {
        $this->serverKey = env('FIREBASE_SERVER_KEY');
        if (!is_array($Data)) return $this->respondWithError("No Hay Datos");
        if (!isset($DeviceToken)) return $this->respondWithError("No hay un token de dispositivo para notificar");
        if (!is_array($Notification)) return $this->respondWithError("No se ha podido crear la notificacion");

        $Data = array_merge($Data, ["userOriginId" => Auth::id()]);
        $Notification=array_merge($Notification,[
            'image'=>'https://sistema.fumival.cl/img.png',
            "click_action"=>$url]);
        $data = [
            "to" => $DeviceToken,
            "notification" =>
            $Notification,
            "data" => $Data,

        ];
        $registred_notification=$this->registerNotification($data);
        if (!$registred_notification) {
            return false;
        }
        $data["notification"]=array_merge($data["notification"],["id"=>$registred_notification->id]);
        $dataString = json_encode($data);
        $headers = [
            'Authorization: key=' . $this->serverKey,
            'Content-Type: application/json',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->GoogleNotificationCenter);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_exec($ch);
        if (!curl_errno($ch)) {
            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case 200:
                    curl_close($ch);
                    return true;
                    break;
                default:
                    curl_close($ch);
                    return false;
            }
        }else{
            return false;
        }

    }
    protected function registerNotification($data)
    {
        $user = User::where('fcm_token', $data['to'])->first();
        $notify = Notifications::create([
            'id_user' => $user->id,
            'notification' => $data,
        ]);
        return $notify;
    }
}
