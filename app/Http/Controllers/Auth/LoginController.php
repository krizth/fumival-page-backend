<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Configuracion;
use App\Models\Empresa;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return "rut";
    }
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }
        $this->setMenu();
        return $request->wantsJson()
            ? new Response('', 204)
            : redirect()->intended($this->redirectPath());
    }
    public function showLoginForm()
    {

        return view('welcome');
    }
    protected function setMenu()
    {
        if (Auth::user()) {
            $user = User::where('id', Auth::id())->first();

            $roles = $user->roles()->get()->first();

            if ($roles == null)
                return;

            $permissions = $roles->permissions;
            $config = Configuracion::get()->first()->menus;
            $menuOut = collect();
            foreach ($permissions as $permName => $permVal) {
                foreach ($config as $menuItem => $menuAllowed) {
                    if ($menuAllowed === $permName && $permVal["allowed"] == "true") {
                        try {
                            $menuOut->push(["menuText" => $menuItem, "route" => route($menuAllowed)]);
                        } catch (RouteNotFoundException $e) {

                            $menuOut->push(["menuText" => $menuItem, "route" => "/"]);
                        }
                    }
                }
            }
            $Mantenedores = [
                "Glosas de evaluacion",
                "Valor de evaluaciones",
                "Conversiones",
                "Tipos de tratamiento",
                "Datos de empresa",
                "Tipos de inmueble",
                "Unidad de Medida",
                "Bodega",
                "Medios de pago",
                "Tipos de Cliente",
                "Trampas"
                //todo:Medio de pago
            ];
            $subMenuOut = collect();
            foreach ($menuOut as $subMenu => $values) {
                foreach ($Mantenedores as $MantenItem) {
                    if ($values["menuText"] === $MantenItem) {
                        $subMenuOut->push($values);
                        $menuOut->forget($subMenu);
                    }
                }
            }

            $menuOut->push(["Mantenedores"=>$subMenuOut]);

            Session::put("menu",$menuOut);
            $empresa=Empresa::find(1);
            $apiController=new ApiController();
            Session::put("logo",$apiController->handleImages(true,$empresa->logo));
        }
    }
}
