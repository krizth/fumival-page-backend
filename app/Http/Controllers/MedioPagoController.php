<?php

namespace App\Http\Controllers;

use App\Models\MedioPago;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MedioPagoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return MedioPago[]|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if($request->wantsJson()){
            return MedioPago::all();
        }else{
            return view('mantenedores.medios_pago',['medio_pago'=>MedioPago::all()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=  $validator = Validator::make($request->all(), [
            'medio' => 'required|min:5',
        ]);
        if(!$validator->fails()){
            return $this->responseOK([MedioPago::create($request->all())]);
        }else{
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $validator=  $validator = Validator::make(["id"=>$id], [
            'id'=>'numeric|required|exists:medio_pago,id',
        ]);
        if(!$validator->fails()){
            return $this->responseOK(MedioPago::find($id));
        }else{
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator=  $validator = Validator::make($request->all(),
            ['medio' => 'required|min:5',
            'id'=>'numeric|required|exists:medio_pago,id',]);
        if(!$validator->fails()){
            return $this->responseOK([MedioPago::where('id',$request->id)->update(["medio"=>$request->medio])]);
        }else{
            return $this->respondFailedParametersValidation();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator=  $validator = Validator::make(["id"=>$request->id], [
            'id'=>'numeric|required|exists:medio_pago,id',
        ]);
        if(!$validator->fails()){
            MedioPago::destroy($request->id);
            return $this->responseOK([]);
        }else{
            return $this->respondFailedParametersValidation();
        }
    }
}
