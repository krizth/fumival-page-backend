<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductosResourceWeb;
use App\Models\FactorConversion;
use App\Models\Producto;
use App\Models\UnidadMedida;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return $this->show($request);
        }
       $this->authorize('viewAny', Producto::class);
         $productos = ProductosResourceWeb::collection(Producto::with(['unidadMedida', 'factorConversions'])->get(["producto.*","producto.id as id_producto"]));
        return view('productos.index', compact('productos'));
    }

    public function buscar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'search' => 'required',
        ]);
        if ((!$validator->fails()) && $request->wantsJson()) {
            return $this->responseOK(Producto::with(['unidadMedida', 'factorConversions'])->where('producto.' . $request->field, 'like', '%' . $request->search . '%')->get());
        } else {
            return $this->responseOK(Producto::with(['unidadMedida', 'factorConversions'])->get());
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {
        $this->authorize('create', Producto::class);
        return $this->responseOK(UnidadMedida::all());

    }

    public function store(Request $request)
    {
        $this->authorize('create', Producto::class);
        $producto = Producto::create($request->except(['unidad_medida', 'factor_conversion']));

        if ($request->has('id_factor_conversion') && $request->get('id_factor_conversion') !== 0) {
            $producto->factorConversions()->attach($request->get('id_factor_conversion'));
        }


        $this->responseOK($producto);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $products=Producto::select("id", "nombre", "codigo");
        if ($request->has("search") && !empty($request->search)) {

            $products->whereRaw("`nombre` like '%$request->search%'");
        $this->authorize('viewAny', Producto::class);
        if($request->forPreset){
            $products->doesntHave('presets');
        }

            return $this->responseOK($products->take(3)->get());
        }

        return $this->responseOK(Producto::take(3)->get());
    }

    public function getProducto($id)
    {
        $this->authorize('viewAny', Producto::class);
        return Producto::with(['unidadMedida', 'factorConversions'])->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $producto = Producto::find($request->get('id'));

        $this->authorize('update', $producto);

        $request->merge(['maquinaria' => $request->get('maquinaria') === 'si']);

        return $this->responseOK($producto->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $producto = Producto::find($request->get('id'));

        $this->authorize('delete', $producto);

        return $this->responseOK($producto->delete());
    }
}
