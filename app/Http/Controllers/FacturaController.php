<?php

namespace App\Http\Controllers;

use App\Models\Certificado;
use App\Models\Factura;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FacturaController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Factura::class);

        $facturas = Factura::with(['certificados'=>function($q){
            return $q->with(["cliente"=>function($q){
                return $q->select('id','id_tipo_cliente','nombre_fantasia')->with('tipoCliente');
            }])->select('id','folio','valor','id_cliente');
        }])->get();

        if ($request->wantsJson()) {
            return $facturas;
        }

        return view('factura.index', compact('facturas'));
    }

    public function pendientes(Request $request){
        return $this->paginate(Certificado::select("id","folio","cliente")->doesntHave('facturas')->get(),5,$request->page);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Factura::class);

        try {
            DB::transaction(function () use ($request) {
                $factura = Factura::create($request->except('certificados'));

                $factura->save();

                if (!empty($request->certificados)) {

                    $certificados = DB::table('certificado')
                        ->whereIn('folio', $request->certificados)
                        ->get();

                    foreach ($certificados as $certificado) {
                        $factura->certificados()->attach($certificado->id);
                    }
                }

                return $this->responseOK($factura);
            });
        } catch (Exception $e) {
            logger($e->getMessage());
            return $this->respondInternalError();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('viewAny', Factura::class);

        return Factura::with('certificados:folio')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $factura = Factura::with('certificados')->find($request->get('id'));
        $certIds=Certificado::select('id')->whereIn('folio',$request->certificados)->get();
        $certs=[];
        $this->authorize('update', $factura);
        if($certIds){
            foreach ($certIds as $cert){
                array_push($certs,$cert->id);
            }
        }
        if (empty($factura)) {
            return $this->respondNotFound();
        }

        try {
            DB::transaction(function () use ($request, $factura,$certs) {
                $factura->certificados()->detach($certs);
                if (!empty($request->certificados)) {
                        $factura->certificados()->attach($certs);
                }
                $factura->save();

                return $this->responseOK($factura);
            });
        } catch (Exception $e) {
            logger($e->getMessage());
            return $this->respondWithError($e->getMessage());
        }

        //$this->responseOK($factura->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $factura = Factura::find($request->get('id'));

        $this->authorize('delete', $factura);

        if (empty($factura)) {
            return $this->responseNoContent();
        }

        try{
            DB::transaction(function () use($factura) {
                $factura->certificados()->detach();

                $factura->delete();
            });

            $this->responseOK('Listo');
        }catch(Exception $e){
            logger($e->getMessage());
            return $this->respondInternalError();
        }
    }
}
