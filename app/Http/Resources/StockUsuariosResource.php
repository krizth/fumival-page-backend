<?php

namespace App\Http\Resources;

use App\Models\Producto;
use Illuminate\Http\Resources\Json\JsonResource;
use Productos;

class StockUsuariosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $min_to_factor=$this->producto->factorConversions->map(function($el){
            return [
                "factor_conversion"=>$el["factor_conversion"],
                "id"=>$el["id"],
                "minima"=>false,
                "descripcion"=>$el["descripcion"]

            ];
        });
        return [
            "id"=> $this->producto['id'],
            "cantidad" =>$this->cantidad,
            "nombre" => $this->producto['nombre'],
            "unidad_medida" => $this->producto['unidadMedida']['abreviatura'],
            "marca" => $this->producto['marca']===null?'':$this->producto['marca'],
            "factor_conversion"=>$min_to_factor->push(collect(
                ["factor_conversion"=>1,
                    "id"=>$this->producto['unidadMedida']["id"],
                    "minima"=>true,
                    "descripcion"=>$this->producto['unidadMedida']['abreviatura']]
            )),
        ];
    }
}
