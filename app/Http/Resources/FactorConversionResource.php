<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FactorConversionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "id_producto"=>$this->id_producto,
            "factor_conversion"=>$this->factor_conversion,
            "descripcion"=>$this->descripcion,
            "unidad_medida_origen"=>$this->unidadMedidaOrigen->nombre,
            "unidad_medida_destino"=>$this->unidadMedidaDestino->nombre
        ];
    }
}
