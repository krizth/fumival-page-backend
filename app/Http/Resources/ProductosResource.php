<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $min_to_factor=$this->producto->factorConversions->map(function($el){
            return [
                "factor_conversion"=>$el["factor_conversion"],
                "id"=>$el["id"],
                "minima"=>false,
                "descripcion"=>$el["descripcion"]

            ];
        });
        return [
            "id"=>$this->id_producto,
            "codigo"=>$this->codigo,
            "lote"=>$this->lote,
            "marca"=>$this->marca,
            "nombre"=>$this->nombre,
            "regISP"=>$this->regISP,
            "maquinaria"=>$this->maquinaria?"si":"no",
            "precio_compra"=>$this->precio_compra,
            "precio_distribucion"=>$this->precio_distribucion,
            "ing_activo"=>$this->ing_activo,
            "id_unidad_medida"=>$this->producto->unidadMedida->abreviatura,
             "factor_conversion"=>$min_to_factor->push(collect(
                 ["factor_conversion"=>1,
                   "id"=>$this->producto->unidadMedida->id,
                   "minima"=>true,
                   "descripcion"=>$this->producto->unidadMedida->abreviatura]
             )),
            "descripcion"=>$this->producto->descripcion
        ];
    }
}
