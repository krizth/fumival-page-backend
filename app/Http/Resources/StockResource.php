<?php

namespace App\Http\Resources;

use App\Models\Producto;
use Illuminate\Http\Resources\Json\JsonResource;
use Productos;

class StockResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=> $this->id,
            "id_producto"=> $this->id_producto,
            "id_bodega" => $this->id_bodega,
            "bodega"=> $this->bodega['nombre'],
            "cantidad" =>$this->cantidad,
            "nombre" => $this->producto['nombre'],
            "id_unidad_medida" => $this->producto['unidadMedida']['abreviatura'],
            "codigo" => $this->producto['codigo'],
            "caducidad" => $this->producto['caducidad']
        ];



        //$producto = ProductosResource::collection(Producto::with(['unidadMedida'])->get())->toArray($request);

        //return array_merge($stock, $producto[0]);
    }
}
