<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class RutasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "codigo" => $this->codigo,
            "id_usuario" => $this->id_usuario,
            "id_cliente" => $this->id_cliente,
            "id_inmueble" => $this->id_inmueble,
            "id_motivo_visita" => $this->id_motivo_visita,
            "horario_inicio" => $this->horario_inicio,
            "horario_fin" => $this->horario_fin,
            "horario_llegada" => $this->horario_llegada,
            "id_estado" => $this->id_estado,
            "comentario" => $this->comentario,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,

            'horario'=>[
                "withFrecuency"=>$this->frecuency!=null&&$this->codigo!=null,
                "frecuency"=>$this->frecuency,
                'hora_inicio'=>Carbon::parse($this->horario_inicio)->format('H:i'),
                'hora_fin'=> Carbon::parse($this->horario_fin)->format('H:i'),
                'fecha'=>Carbon::parse($this->horario_inicio)->format('Y-m-d')
            ],
            'inmueble'=>$this->inmueble,
            'cliente' =>$this->cliente,
            'motivo_visita'=> $this->motivoVisita,
            'estado_ruta'=> $this->estadoRuta,
            'user'=>$this->user
        ];
    }
}
