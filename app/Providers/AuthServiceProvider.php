<?php

namespace App\Providers;

use App\Models\Bodega;
use App\Models\Certificado;
use App\Models\Cliente;
use App\Models\Empresa;
use App\Models\Evaluacion;
use App\Models\FactorConversion;
use App\Models\Factura;
use App\Models\GlosaEvaluacion;
use App\Models\Inmuebles;
use App\Models\Notifications;
use App\Models\Producto;
use App\Models\Role;
use App\Models\Rutas;
use App\Policies\CertificadoPolicy;
use App\Policies\ClientePolicy;
use App\Policies\RolePolicy;
use App\Policies\RutasPolicy;
use App\Policies\StockPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use App\Models\Stock;
use App\Models\StockUsuarios;
use App\Models\TipoTratamiento;
use App\Models\UnidadMedida;
use App\Policies\BodegaPolicy;
use App\Policies\EmpresaPolicy;
use App\Policies\EvaluacionPolicy;
use App\Policies\FactorConversionPolicy;
use App\Policies\FacturaPolicy;
use App\Policies\GlosaEvaluacionPolicy;
use App\Policies\InmueblesPolicy;
use App\Policies\ProductoPolicy;
use App\Policies\StockUsuariosPolicy;
use App\Policies\TipoTratamientoPolicy;
use App\Policies\UnidadMedidaPolicy;
use App\Policies\NotificationsPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Cliente::class => ClientePolicy::class,
        Role::class => RolePolicy::class,
        Stock::class => StockPolicy::class,
        UnidadMedida::class => UnidadMedidaPolicy::class,
        Producto::class => ProductoPolicy::class,
        Bodega::class => BodegaPolicy::class,
        StockUsuarios::class => StockUsuariosPolicy::class,
        GlosaEvaluacion::class => GlosaEvaluacionPolicy::class,
        FactorConversion::class => FactorConversionPolicy::class,
        Empresa::class => EmpresaPolicy::class,
        TipoTratamiento::class => TipoTratamientoPolicy::class,
        Evaluacion::class => EvaluacionPolicy::class,
        Inmuebles::class => InmueblesPolicy::class,
        Factura::class => FacturaPolicy::class,
        Certificado::class=>CertificadoPolicy::class,
        Notifications::class=>NotificationsPolicy::class,
        Rutas::class=>RutasPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes(null,['prefix'=>'api/v1/oauth']);
        //
    }
}
