<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $descripcion
 * @property Contacto[] $contactos
 */
class TipoContacto extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public static $EMAIL=1;
    public static $FONO=2;
    protected $table = 'tipo_contacto';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['descripcion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactos()
    {
        return $this->hasMany('App\Models\Contactos', 'id_tipo_contacto');
    }
}
