<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_tipo_contacto
 * @property string $contacto
 * @property TipoContactoController $tipoContacto
 * @property ClienteContacto[] $clienteContactos
 */
class Contactos extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    public $timestamps =false;
    protected $fillable = ['id_tipo_contacto', 'contacto'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoContacto()
    {
        return $this->belongsTo('App\Models\TipoContacto', 'id_tipo_contacto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clienteContactos()
    {
        return $this->hasMany('App\Models\ClienteContactos', 'id_contacto');
    }
}
