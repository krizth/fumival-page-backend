<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_cliente
 * @property integer $id_contacto
 * @property boolean $predeterminado
 * @property Cliente $cliente
 * @property Contacto $contacto
 */
class ClienteContactos extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    public $timestamps =false;
    protected $fillable = ['id_cliente', 'id_contacto', 'predeterminado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contacto()
    {
        return $this->belongsTo('App\Models\Contactos', 'id_contacto');
    }
}
