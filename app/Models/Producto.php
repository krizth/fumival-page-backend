<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_unidad_minima
 * @property string $nombre
 * @property string $codigo
 * @property int $precio_compra
 * @property int $precio_distribucion
 * @property string $ing_activo
 * @property boolean $compuesto
 * @property string $regISP
 * @property string $marca
 * @property string $lote
 * @property string $caducidad
 * @property boolean $perecible
 * @property string $descripcion
 * @property string $ubicacion
 * @property string $created_at
 * @property string $updated_at
 * @property UnidadMedida $unidadMedida
 * @property FactorConversion[] $factorConversions
 * @property Movimiento[] $movimientos
 * @property Preset[] $presets
 * @property Stock[] $stocks
 * @property StockUsuario[] $stockUsuarios
 * @property Tratamiento[] $tratamientos
 */
class Producto extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'producto';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_unidad_minima', 'nombre', 'codigo', 'precio_compra', 'precio_distribucion', 'ing_activo', 'compuesto', 'regISP', 'marca',  'maquinaria', 'descripcion', 'ubicacion', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unidadMedida()
    {
        return $this->belongsTo('App\Models\UnidadMedida', 'id_unidad_minima');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function factorConversions()
    {
        return $this->belongsToMany('App\Models\FactorConversion', 'factor_conversion_prod', 'id_producto', 'id_factor_conversion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movimientos()
    {
        return $this->hasMany('App\Models\Movimiento', 'id_producto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function presets()
    {
        return $this->hasMany('App\Models\Preset', 'id_producto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stocks()
    {
        return $this->hasMany('App\Models\Stock', 'id_producto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stockUsuarios()
    {
        return $this->hasMany('App\Models\StockUsuarios', 'id_producto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tratamientos()
    {
        return $this->hasMany('App\Models\Tratamiento', 'id_producto');
    }
}
