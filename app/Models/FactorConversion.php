<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_unidad_medida_origen
 * @property integer $id_unidad_medida_destino
 * @property integer $factor_conversion
 * @property string $descripcion
 * @property UnidadMedida $unidadMedidaDestino
 * @property UnidadMedida $unidadMedidaOrigen
 * @property FactorConversionProd[] $factorConversionProds
 */
class FactorConversion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'factor_conversion';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The "timestamps" disabled.
     *
     * @var boolean
     */
    public $timestamps=false;

    /**
     * @var array
     */
    protected $fillable = ['id_unidad_medida_origen', 'id_unidad_medida_destino', 'factor_conversion', 'descripcion','forProduct'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unidadMedidaDestino()
    {
        return $this->belongsTo('App\Models\UnidadMedida', 'id_unidad_medida_destino');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unidadMedidaOrigen()
    {
        return $this->belongsTo('App\Models\UnidadMedida', 'id_unidad_medida_origen');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function factorConversionProds()
    {
        return $this->belongsToMany('App\Models\Producto', 'factor_conversion_prod','id_factor_conversion','id_producto');
    }
}
