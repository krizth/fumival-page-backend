<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trampas extends Model
{
    protected $fillable = ['tratamiento_id', 'campos',"nombre","georeferenciada"];

    public $primaryKey="id";

    public $timestamps=false;

    protected $casts=["campos"=>"array","georeferenciada"=>"boolean"];

    protected $table="trampas";
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoTratamiento()
    {
        return $this->belongsTo('App\Models\TipoTratamiento', 'tratamiento_id');
    }
}
