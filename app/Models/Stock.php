<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_producto
 * @property integer $id_bodega
 * @property integer $cantidad
 * @property string $created_at
 * @property string $updated_at
 * @property Bodega $bodega
 * @property Producto $producto
 */
class Stock extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stock';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_producto', 'id_bodega', 'cantidad', 'lote','caducidad','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bodega()
    {
        return $this->belongsTo('App\Models\Bodega', 'id_bodega');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'id_producto');
    }
}
