<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_glosa
 * @property integer $id_evaluacion
 * @property integer $id_certificado
 * @property Certificado $certificado
 * @property Evaluacion $evaluacion
 * @property GlosaEvaluacion $glosaEvaluacion
 * @method static create(array $array)
 */
class EvaluacionesCertificado extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluaciones_certificado';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps =false;
    /**
     * @var array
     */
    protected $fillable = ['id_glosa', 'id_evaluacion', 'id_certificado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function certificado()
    {
        return $this->belongsTo('App\Models\Certificado', 'id_certificado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evaluacion()
    {
        return $this->belongsTo('App\Models\Evaluacion', 'id_evaluacion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function glosaEvaluacion()
    {
        return $this->belongsTo('App\Models\GlosaEvaluacion', 'id_glosa');
    }
}
