<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nombre
 * @property string $nombre_fantasia
 * @property string $rut
 * @property string $direccion_principal
 * @property Certificado[] $certificados
 * @property ClienteContacto[] $clienteContactos
 * @property Inmueble[] $inmuebles
 * @property tipoCliente[] $tipo_cliente
 */
class Cliente extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cliente';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */

    public $timestamps=false;
    protected $fillable = ['nombre', 'nombre_fantasia', 'rut', 'id_tipo_cliente','id_tipo_contacto', 'direccion_principal','ciudad','fono','email'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certificados()
    {
        return $this->hasMany('App\Models\Certificado', 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clienteContactos()
    {
        return $this->hasMany('App\Models\ClienteContactos', 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function inmuebles()
    {
        return $this->belongsToMany('App\Models\Inmuebles', 'inmuebles_cliente', 'id_cliente', 'id_inmuebles');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rutas()
    {
        return $this->hasMany('App\Models\Ruta', 'id_cliente');
    }
    public function tipoCliente()
    {
        return $this->belongsTo('App\Models\TipoCliente', 'id_tipo_cliente');
    }
}
