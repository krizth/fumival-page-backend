<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $evaluacion_textual
 * @property int $peso
 * @property string $created_at
 * @property string $updated_at
 */
class Evaluacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluacion';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['evaluacion_textual', 'peso'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluacionesCertificados()
    {
        return $this->hasMany('App\Models\EvaluacionesCertificado', 'id_evaluacion');
    }
}
