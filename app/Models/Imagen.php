<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $imagen
 * @property string $created_at
 * @property string $updated_at
 * @property CertificadoImagene[] $certificadoImagenes
 */
class Imagen extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'imagen';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['imagen', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certificadoImagenes()
    {
        return $this->hasMany('App\Models\CertificadoImagenes', 'id_imagen');
    }
}
