<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_producto
 * @property integer $id_compuesto
 * @property int $cantidad
 * @property string $created_at
 * @property string $updated_at
 * @property Producto $producto
 * @property Producto $producto
 */
class Preset extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'preset';

    /**
     * @var array
     */
    protected $fillable = ['id_producto', 'id_compuesto', 'cantidad', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function compuesto()
    {
        return $this->belongsTo('App\Models\Producto', 'id_compuesto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'id_producto');
    }
}
