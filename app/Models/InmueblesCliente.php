<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_inmuebles
 * @property integer $id_cliente
 * @property Cliente $cliente
 * @property Inmueble $inmueble
 */
class InmueblesCliente extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inmuebles_cliente';

    /**
     * @var array
     */
    protected $fillable = ['id_inmuebles', 'id_cliente'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inmueble()
    {
        return $this->belongsTo('App\Models\Inmuebles', 'id_inmuebles');
    }
}
