<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $medio
 */
class MedioPago extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medio_pago';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */

    public $timestamps=false;


    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['medio'];

}
