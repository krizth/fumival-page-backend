<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $criterio
 * @property string $created_at
 * @property string $updated_at
 * @property EvaluacionesCertificado[] $evaluacionesCertificados
 */
class GlosaEvaluacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'glosa_evaluacion';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['criterio', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluacionesCertificados()
    {
        return $this->hasMany('AppModels\EvaluacionesCertificado', 'id_glosa');
    }
}
