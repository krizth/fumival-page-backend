<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_producto
 * @property integer $id_tipo_tratamiento
 * @property string $dosis
 * @property string $total
 * @property string $concentracion
 * @property string $lugares
 * @property string $vigencia
 * @property Producto $producto
 * @property TipoTratamiento $tipoTratamiento
 * @property Certificado[] $certificados
 */
class Tratamiento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tratamiento';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['id_producto', 'id_tipo_tratamiento', 'dosis', 'total', 'concentracion', 'lugares', 'vigencia'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'id_producto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoTratamiento()
    {
        return $this->belongsTo('App\Models\TipoTratamiento', 'id_tipo_tratamiento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function certificados()
    {
        return $this->belongsToMany('App\Models\Certificado', null, 'id_tratamiento', 'id_certificado');
    }
}
