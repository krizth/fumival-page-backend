<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movimientos extends Model
{
    protected $table = 'movimientos';

    protected $fillable = ['id_producto', 'id_usuario_origen', 'id_motivo', 'recuento','lote'];

    public function motivo_movimiento()
    {
        return $this->belongsTo('App\Models\MotivoMovimiento', 'id_motivo');
    }

    public function movimiento_stock_usuarios()
    {
        return $this->hasMany('App\Models\MovimientosStockUsuarios', 'id_movimiento');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'id_usuario_origen');
    }

    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'id_producto');
    }
}
