<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $motivo
 * @property Movimiento[] $movimientos
 */
class MotivoMovimiento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public static $AJUSTE=1;
    public static $PRESTAMO=2;
    public static $TRASPASO=3;
    public static $APLICACION=4;
    public static $DESCOMPOSICION=5;
    public static $PERDIDA=6;
    public static $DEVOLUCION=7;
    public static $INGRESO=8;


    protected $table = 'motivo_movimiento';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['motivo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movimientos()
    {
        return $this->hasMany('App\Models\Movimiento', 'id_motivo');
    }
}
