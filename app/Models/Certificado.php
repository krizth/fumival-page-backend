<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @property integer $id
 * @property integer $id_cliente
 * @property integer $id_empresa
 * @property integer $id_usuario
 * @property integer $folio
 * @property string $fecha
 * @property string $hora_inicio
 * @property string $hora_termino
 * @property integer $valor
 * @property string $cond_pago
 * @property string $observaciones
 * @property string $latitud
 * @property string $longitud
 * @property string $empresa
 * @property string $tratamientos
 * @property string $cliente
 * @property string $usuario
 * @property string $imagenes
 * @property string $evaluaciones
 * @property string $trampas
 * @property string $inmueble
 * @property string $firmas
 * @property string $created_at
 * @property string $updated_at
 * @property Cliente $clientes
 * @property Empresa $empresas
 * @property User $user
 * @property Factura[] $facturas
 * @property Imagen[] $imagens
 * @property Tratamiento[] $tratamiento
 * @property EvaluacionesCertificado[] $evaluacionesCertificados
 */
class Certificado extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'certificado';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    protected $casts = [
        'empresa' => "array",
        'tratamientos' => "array",
        'cliente' => "array",
        'usuario' => "array",
        'imagenes' => "array",
        'evaluaciones' => "array",
        'trampas' => "array",
        'inmueble' => "array",
        'firmas' => "array",
    ];

    /**
     * @var array
     */
    protected $fillable = ['id_cliente', 'id_empresa', 'id_usuario', 'folio', 'fecha', 'hora_inicio', 'hora_termino', 'valor', 'cond_pago', 'observaciones', 'latitud', 'longitud', 'empresa', 'tratamientos', 'cliente', 'usuario', 'imagenes', 'evaluaciones', 'trampas', 'inmueble', 'firmas', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public static function StoreCert($firmas,$date,$empresa,$request){
        return DB::transaction(function () use ($firmas, $date,$empresa,$request) {
            $folio = DB::table('certificado')->lockForUpdate()->latest('id')->first();
            if (empty($folio)) {
                $folio = 0;
            } else {
                $folio = $folio->folio;
            }
            $tratamientos=$request->tratamientos;
            foreach($tratamientos as $k=>$v){
                $vig=Carbon::parse($date->format('d/m/Y'));
                if(strpos($v["vigencia"],':')===false){
                    if($v["vigencia"]>0){
                        $vig->addDays($v["vigencia"]);
                    }
                    $tratamientos[$k]["vigencia"]=$vig->format('d/m/Y');
                }else{$tratamientos[$k]["vigencia"]=explode(':',$v["vigencia"])[0].' Horas';}
            }
            $newCert = Certificado::create([
                "folio" => $folio + 1,
                "fecha" => $date,
                "id_cliente" => $request->cliente["id"],
                "id_empresa" => $empresa->id,
                "id_usuario" => Auth::id(),
                "hora_inicio" => $request->horario["hora_inicio"],
                "hora_termino" => $request->horario["hora_fin"],
                "observaciones" => $request->observaciones,
                "empresa" => $empresa,
                "tratamientos" => $tratamientos,
                "cliente" => $request->cliente,
                "usuario" => Auth::user(),
                "imagenes" => $request->imagenes,
                "evaluaciones" => $request->evaluaciones,
                "trampas" => $request->trampas,
                "inmueble"=>$request->inmueble,
                "cond_pago"=>$request->cond_pago,
                "valor"=>$request->valor,
                "firmas" => $firmas,
                'created_at'=> Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
            if(!$newCert){
                throw new \Exception("Error: No se ingreso el certificado");
            }
            array_map(function ($value) use($newCert) {
                $vigencia=Carbon::parse($newCert->fecha->format('Y-m-d').' '.$newCert->hora_termino);
                if(strpos($value["vigencia"],':')===false){
                    $vigencia->addDays($value["vigencia"]);
                    $vigencia->hour(23)->minute(59)->second(0);
                    $vigencia->format('Y-m-d H:i');
                }else{
                    try{
                        $vigencia->addHours(intval(explode(':',$value["vigencia"])[0]));
                        $vigencia->addMinutes(intval(explode(':',$value["vigencia"])[1]));
                    }catch(\Exception $ex){
                        $vigencia->format('Y-m-d H:i');
                    }
                }
                $tratamientos=Tratamiento::create(
                    [
                        "concentracion"=>$value["concentracion"],
                        "dosis"=>$value["dosis"],
                        "lugares"=>$value["lugares"],
                        "total"=>$value["total"],
                        "vigencia"=>$vigencia,//$value["vigencia"],
                        "id_tipo_tratamiento"=>$value["tipoTratamiento"]["id"],
                        "id_producto"=>$value["producto"]["id"],
                    ]
                );
                if(count($value["factor_conversion"])>0&&$value["producto"]["maquinaria"]!="si"){
                    $stock=StockUsuarios::where([
                        ["id_producto","=",$value["producto"]["id"]],
                        ["id_usuario","=",Auth::id()],
                        ["cantidad",">",0]
                    ])->first();
                    if(!$stock){
                        throw new \Exception("No tiene stock del producto: {$value['producto']['nombre']} , ID: {$value['producto']['id']}");
                    }
                    $transformUsingFactor=($value["total"]*$value["factor_conversion"]["factor_conversion"]);
                    $saldo=$stock->cantidad - $transformUsingFactor;
                    if($saldo<0){
                        throw new \Exception("No tiene saldo para usar en el tratamiento {$value["tipoTratamiento"]["nombre"]}");
                    }
                    $stock->cantidad=$saldo;
                    $stock->save();
                    Movimientos::create([
                        "id_producto"=>$value["producto"]["id"],
                        "id_usuario_origen"=>Auth::id(),
                        "id_motivo"=>MotivoMovimiento::$APLICACION,
                        "recuento"=>$transformUsingFactor,
                        'created_at'=> Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                }
                $newCert->tratamientos()->attach($tratamientos);
            },$request->tratamientos);

            if ($request->imagenes) {
                array_map(function ($val) use ($newCert) {
                    $imagen = Imagen::create(["imagen" => $val]);
                    $newCert->imagenes()->attach($imagen);
                }, $request->imagenes);
            }

            array_map(function($val) use($newCert){
                EvaluacionesCertificado::create([
                    "id_glosa"=>$val["glosa_evaluacion"]["id"],
                    "id_evaluacion"=>$val["id"],
                    "id_certificado"=>$newCert->id
                ]);
            },$request->evaluaciones);
            if($request->id_ruta){
                Rutas::where("id",$request->id_ruta)->update(["id_estado"=>EstadoRuta::$REALIZADA]);
            }
            return $newCert;
        });
    }

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa', 'id_empresa');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_usuario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facturas()
    {
        return $this->belongsToMany('App\Models\Factura', null, 'id_certificado', 'id_factura');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function imagenes()
    {
        return $this->belongsToMany('App\Models\Imagen', 'certificado_imagenes', 'id_certificado', 'id_imagen');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tratamientos()
    {
        return $this->belongsToMany('App\Models\Tratamiento', null, 'id_certificado', 'id_tratamiento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluacionesCertificados()
    {
        return $this->hasMany('App\Models\EvaluacionesCertificado', 'id_certificado');
    }
}
