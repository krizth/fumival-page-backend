<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $num_factura
 * @property integer $iva
 * @property integer $total
 * @property string $fecha
 * @property string $created_at
 * @property string $updated_at
 * @property Certificado[] $certificados
 */
class Factura extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'factura';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['num_factura', 'iva', 'total', 'fecha', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function certificados()
    {
        return $this->belongsToMany('App\Models\Certificado', null, 'id_factura', 'id_certificado');
    }
}
