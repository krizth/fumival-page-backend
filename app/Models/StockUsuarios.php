<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockUsuarios extends Model
{
    protected $table = 'stock_usuarios';

    protected $fillable = ['id_usuario', 'id_producto', 'cantidad','lote','caducidad'];

    public function users()
    {
        return $this->belongsTo('App\User', 'id_usuario');
    }

    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'id_producto');
    }
    public function factorConversions()
    {
        return $this->belongsToMany('App\Models\FactorConversion', 'factor_conversion_prod', 'id_producto', 'id_producto');
    }

}
