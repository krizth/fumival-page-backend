<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovimientosStockUsuarios extends Model
{
    protected $table = 'movimientos_stock_usuarios';

    protected $fillable = ['id_movimiento', 'id_usuario_destino', 'id_stock_usuarios'];

    public $incrementing = false;

    public function users()
    {
        return $this->belongsTo('App\User', 'id_usuario_destino');
    }

    public function stock_usuarios()
    {
        return $this->belongsTo('App\Models\StockUsuarios', 'id_stock_usuarios');
    }

    public function movimientos()
    {
        return $this->belongsTo('App\Models\Movimientos', 'id_movimiento');
    }
}
