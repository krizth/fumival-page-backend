<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $motivo
 * @property Ruta[] $rutas
 */
class MotivoVisita extends Model
{

    public static $COTIZACION=1;
    public  static $SERVICIO=2;
    public static $DISTRIBUCION=3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motivo_visita';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['motivo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rutas()
    {
        return $this->hasMany('App\Models\Ruta', 'id_motivo_visita');
    }
}
