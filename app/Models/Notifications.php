<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $id_user
 * @property mixed $notification
 * @property string $deleted_at
 * @property User $user
 */
class Notifications extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $casts=["notification"=>'array'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    public $timestamps=false;
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'notification'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }
}
