<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $id_autorizador
 * @property integer $id_solicitante
 * @property integer $id_producto
 * @property integer $cantidad
 * @property integer $id_unidad_medida
 * @property integer $id_estado
 * @property string $token
 */
class SolicitudTraspaso extends Model
{
    use SoftDeletes;
    public static $EN_TRAMITE=1;
    public static $ACEPTADA=2;
    public static $RECHAZADA=3;
    public static $CANCELADA=4;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'solicitud_traspaso';
    protected $casts=['minima'=>'boolean'];
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_autorizador','minima', 'id_solicitante', 'id_producto', 'cantidad', 'id_unidad_medida', 'id_estado', 'token','created_at','updated_at'];

    public static function solicitudesUsuarioActual(){
        return SolicitudTraspaso::with(['autorizador'=>function($query){
            return $query->select('id','rut','nombre');
        },'producto','unidad_medida'])->where('id_solicitante',Auth::id())->whereDate('created_at', Carbon::today())->get();
    }

    public static function pendientesAutorizacionUsuarioActual(){
        return SolicitudTraspaso::with(['solicitante'=>function($query){
            return $query->select('id','rut','nombre');
        },'producto','unidad_medida'])->where('id_autorizador',Auth::id())->whereDate('created_at', Carbon::today())->get();
    }

    public static function autorizarTraspaso($token){
        return SolicitudTraspaso::where([['token','=',$token],["id_autorizador","=",Auth::id()]])->update([
            'id_estado'=>self::$ACEPTADA,
            'updated_at'=>Carbon::now()
        ]);
    }

    public static function desautorizarTraspaso($token){
        return SolicitudTraspaso::where([['token','=',$token],["id_autorizador","=",Auth::id()],["id_estado","=",SolicitudTraspaso::$EN_TRAMITE]])->update([
            'id_estado'=>self::$RECHAZADA,
            'updated_at'=>Carbon::now()
        ]);
    }

    public static function cancelarSolicitud($tokens){
        return SolicitudTraspaso::whereIn('token',$tokens)->delete();
    }
    public static function solicitarTraspaso($request){
        return SolicitudTraspaso::create([
            'id_autorizador'=>$request->id_usuario,
            'id_solicitante'=>Auth::id(),
            'id_producto'=>$request->id_producto,
            'id_unidad_medida'=>$request->id_unidad_medida,
            'minima'=>$request->factor_conversion===null?false:$request->factor_conversion['minima'],
            'cantidad'=>$request->cantidad,
            'token'=>(string) Str::orderedUuid(),
            'id_estado'=>self::$EN_TRAMITE,
            'created_at'=>Carbon::now()
        ]);
    }
    public function solicitante(){
        return $this->hasOne('App\User', 'id','id_solicitante');
    }

    public function autorizador(){
        return $this->hasOne('App\User', 'id','id_autorizador');
    }
    public function producto(){
        return $this->hasOne('App\Models\Producto','id','id_producto');
    }
    public function unidad_medida(){
        return $this->hasOne('App\Models\UnidadMedida','id','id_unidad_medida');
    }
}
