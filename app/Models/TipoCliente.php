<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $tipo
 * @property Cliente[] $clientes
 */
class TipoCliente extends Model
{
    public static $PERSONA_NATURAL=1;
    public static $EMPRESA=2;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_cliente';

    public $timestamps=false;

    protected $casts=['restrict_payment'=>'boolean'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['tipo','restrict_payment','color','impuesto'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientes()
    {
        return $this->hasMany('App\Models\Cliente', 'id_tipo_cliente');
    }
}
