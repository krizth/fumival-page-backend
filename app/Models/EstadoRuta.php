<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $estado
 * @property Ruta[] $rutas
 */
class EstadoRuta extends Model
{
    public static $VIGENTE=1;
    public  static $REALIZADA=2;
    public static $EN_TRAMITE=3;
    public static $CANCELADA=4;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estado_ruta';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rutas()
    {
        return $this->hasMany('App\Models\Ruta', 'id_estado');
    }
}
