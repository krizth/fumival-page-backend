<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_usuario
 * @property integer $id_cliente
 * @property integer $id_inmueble
 * @property string $codigo
 * @property integer $id_motivo_visita
 *  * @property integer $frecuency
 * @property string $horario_inicio
 * @property string $horario_fin
 * @property string $horario_llegada
 * @property integer $id_estado
 * @property string $comentario
 * @property string $created_at
 * @property string $updated_at
 * @property Cliente $cliente
 * @property Inmueble $inmueble
 * @property User $user
 */
class Rutas extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     *
     *
     */
    public $timestamps=false;
    protected $fillable = ['id_usuario', 'id_cliente', 'id_inmueble', 'codigo', 'id_motivo_visita', 'horario_inicio', 'horario_fin', 'horario_llegada', 'id_estado', 'comentario', 'frecuency' , 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'id_cliente');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inmueble()
    {
        return $this->belongsTo('App\Models\Inmuebles', 'id_inmueble');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_usuario');
    }

    public function motivoVisita()
    {
        return $this->hasOne('App\Models\MotivoVisita', 'id','id_motivo_visita');
    }
    public function estadoRuta(){
        return $this->hasOne('App\Models\EstadoRuta', 'id','id_estado');
    }

    public static function validateID(){
        return[
            'id'=>'required|exists:rutas,id',
        ];
    }
    public static function Rules(){
        return [
            'id_usuario'=>'numeric|required|exists:users,id',
            'id_cliente'=>'numeric|required|exists:cliente,id',
            'id_inmueble'=>'numeric|required|exists:inmuebles,id',
            'id_motivo_visita'=>'numeric|required|exists:motivo_visita,id',
            'horario_inicio'=>'required|date_format:Y-m-d H:i',
            'horario_fin'=>'required|date_format:Y-m-d H:i',
            'id_estado'=>'required|exists:estado_ruta,id',
        ];
    }
    public static function RulesArray(){
        return [
            'id_usuario'=>'numeric|required|exists:users,id',
            'id_cliente'=>'numeric|required|exists:cliente,id',
            'id_inmueble'=>'numeric|required|exists:inmuebles,id',
            'id_motivo_visita'=>'numeric|required|exists:motivo_visita,id',
            'horario_inicio'=>'required|date_format:H:i',
            'horario_fin'=>'required|date_format:H:i',
            'id_estado'=>'required|exists:estado_ruta,id',
        ];
    }



}
