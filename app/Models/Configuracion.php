<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
    protected $table="configuracion";
    protected $fillable=["menus","default_permissions","iva"];
    protected $casts=["menus"=>"array","default_permissions"=>"array"];
    public $timestamps = false;
}
