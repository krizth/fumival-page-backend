<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nombre
 * @property string $abreviatura
 */
class TipoTratamiento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_tratamiento';
    public $timestamps = false;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'abreviatura','comentario_fijo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certificadoTratamientos()
    {
        return $this->hasMany('App\Models\CertificadoTratamiento', 'id_tipo_tratamiento');
    }
}
