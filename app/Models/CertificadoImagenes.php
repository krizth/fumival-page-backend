<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_certificado
 * @property integer $id_imagen
 * @property string $created_at
 * @property string $updated_at
 * @property Certificado $certificado
 * @property Imagen $imagen
 */
class CertificadoImagenes extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_certificado', 'id_imagen', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function certificado()
    {
        return $this->belongsTo('App\Models\Certificado', 'id_certificado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function imagen()
    {
        return $this->belongsTo('App\Models\Imagen', 'id_imagen');
    }
}
