<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nombre
 * @property string $rut
 * @property string $direccion
 * @property string $correo
 * @property string $fono
 * @property string $logo
 * @property string $pagina_web
 * @property string $otros
 * @property string $created_at
 * @property string $updated_at
 */
class Empresa extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'empresa';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $casts=[
        'location'=>'array'
    ];
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'rut', 'direccion', 'correo', 'fono','celular', 'logo', 'pagina_web', 'otros','repLegal','rutRepLegal', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certificados()
    {
        return $this->hasMany('App\Models\Certificado', 'id_empresa');
    }
}
