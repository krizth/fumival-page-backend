<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table='roles';
    protected $fillable=['role_name','slug','permissions'];
    protected $casts=['permissions'=>'array'];
    public $timestamps = false;
    protected $hidden=[];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_users','role_id','user_id');
    }
    public function rules(){
        return [
            'slug'=>'required|slug|unique:roles',
            'permissions'=> 'required'
        ];
    }
    public static function updateRules(){
        return [
            'id' =>'required|unique:roles',
            'name' =>'required',
            'permissions'=>'required'
        ];
    }
    public static function CreateRules(){
        return [
            'id' =>'required|unique:roles',
            'name' =>'required',
            'slug'=>'required|unique:roles',
            'permissions'=>'required'
        ];
    }
    public function hasAccess(array $permissions) : bool
    {

        foreach ($permissions as $permission) {

            if ($this->hasPermission($permission))
                return true;
        }
        return false;
    }

    private function hasPermission(string $permission) : bool
    {

        // if($this->permissions[$permission]["allowed"]==="false"){
        //     return false;
        // }
        // return true;

        return $this->permissions[$permission]["allowed"];

    }
}
