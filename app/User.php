<?php

namespace App;

use App\Models\Role;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;
    public function username(){
        return 'rut';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','rut', 'email', 'password','firma','tec_a_cargo','location','patente','car_location','fcm_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'location'=>'array'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users','user_id','role_id');
    }

    public function inRole(string $roleSlug)
    {
        return $this->roles()->where('slug', $roleSlug)->count() == 1;
    }

    public function isAdmin(){

        return auth()->user()->isAdmin;
    }

    public function hasAccess(array $permissions) : bool
    {

        foreach ($this->roles as $role) {

            if($role->hasAccess($permissions)) {
                return true;
            }
        }
        return false;
    }

    public function findForPassport($username) {
        return $this->where('rut', $username)->first();
    }

    public function tecnicoACargo(){
        return $this->hasOne('App\User','id','tec_a_cargo');
    }

    public static function validateID(){
        return[
            'id'=>'required|exists:users,id',
        ];
    }
}
