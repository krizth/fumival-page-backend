<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendCert extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $ClientData;
    public $CertData;
    public $FileName;
    public function __construct($ClientData,$CertData,$FileName)
    {
        $this->ClientData=$ClientData;
        $this->CertData=$CertData;
        $this->FileName=$FileName;
    }

    /**
     * Build the message.
     *
     * @return $this
     *
     */
    public function build()
    {

        return $this->view('mail.certificado',['ClientData'=>$this->ClientData])->from('ventas@fumival.cl')->subject("Certificado de Tratamiento")
            ->attachData($this->CertData,$this->FileName);

    }
}
