@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();
        $routes=[
            "delete" => route("delete-inmuebles"),
            "update" => route("update-inmuebles"),
            "store" => route("write-inmuebles"),
            "propetarios" => route("read-clientes")
        ];

        $permissions = [
            "create" => $user->hasAccess(["write-inmuebles"]),
            "edit" => $user->hasAccess(["update-inmuebles"]),
            "delete" => $user->hasAccess(["delete-inmuebles"])
        ];
    @endphp

    <index-inmuebles routes='@json($routes)' inmuebles='@json($inmuebles)' permissions='@json($permissions)'></index-inmuebles>
@endsection

