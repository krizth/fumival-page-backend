<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Fumival</title>
        <link rel="shortcut icon" href="/images/favicon.png">
        <!-- Fonts -->

    </head>
    <body >
    @if (Route::has('login'))
        <div class="top-right links" id="app">

                @php
                    $route=["name"=>"Login","url"=>route('login')];
                @endphp

    @endif

                <index data='@json($route)' login='{{ route('login')}}' old='@json(["rut"=> old('rut')])'/>

        </div>
        @if ($errors->any())
            <div class="alert align-items-center alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </body>


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" ></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

</html>
