@extends('layouts.app')
@section('content')
    @php
        $routes = [
            "store" => route("write-facturas"),
            "update" => route("update-facturas"),
            "delete" => route("delete-facturas"),
            "certificados" => route("read-certificado"),
            "read" => route("read-facturas")
        ];

        $permissions = [
            "create" => route("write-facturas"),
            "edit" => route("update-facturas"),
            "delete" => route("delete-facturas")
        ];
    @endphp

    <index-factura routes='@json($routes)' facturas='@json($facturas)' permissions='@json($permissions)'></index-factura>
@endsection
