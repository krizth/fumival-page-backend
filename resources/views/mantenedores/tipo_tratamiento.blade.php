@extends('layouts.app')
@section('content')
    @php
        $routes=["delete"=>route("delete-tipo_tratamiento"),"update"=>route("update-tipo_tratamiento"),"create"=>route("create-tipo_tratamiento"),"store"=>route("write-tipo_tratamiento")];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-tipo_tratamiento']),
            "edit" => $user->hasAccess(['update-tipo_tratamiento']),
            "delete" => $user->hasAccess(['delete-tipo_tratamiento']),
        );
    @endphp
    <index-tipo-tratamiento routes='@json($routes)' tipo_tratamiento='@json($tipo_tratamiento)' permissions='@json($permisos)'></index-tipo-tratamiento>
@endsection
