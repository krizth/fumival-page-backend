@extends('layouts.app')
@section('content')
    @php
        $routes=["delete"=>route("delete-tipo_cliente"),"update"=>route("update-tipo_cliente"),"store"=>route("write-tipo_cliente")];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-tipo_cliente']),
            "edit" => $user->hasAccess(['update-tipo_cliente']),
            "delete" => $user->hasAccess(['delete-tipo_cliente'])
        );
    @endphp
    <index-tipo-cliente routes='@json($routes)' tipo_cliente='@json($tipo_cliente)' permissions='@json($permisos)'></index-tipo-cliente>
@endsection
