@extends('layouts.app')
@section('content')
    @php
        $routes=[
            "delete"=>route("delete-factor_conversion"),
            "update"=>route("update-factor_conversion"),
            "create"=>route("create-factor_conversion"),
            "store"=>route("write-factor_conversion"),
            "unidades" => route("read-unidad_medida"),
            "productos" => route("read-producto")
        ];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-factor_conversion']),
            "edit" => $user->hasAccess(['update-factor_conversion']),
            "delete" => $user->hasAccess(['delete-factor_conversion'])
        );

    @endphp
    <index-conversion routes='@json($routes)' conversion='@json($factor_conversion)' permissions='@json($permisos)'></index-conversion>
@endsection
