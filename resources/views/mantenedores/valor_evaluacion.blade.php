@extends('layouts.app')
@section('content')
    @php
        $routes=["delete"=>route("delete-evaluacion"),"update"=>route("update-evaluacion"),"create"=>route("write-evaluacion"),"store"=>route("write-evaluacion")];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-evaluacion']),
            "edit" => $user->hasAccess(['update-evaluacion']),
            "delete" => $user->hasAccess(['delete-evaluacion'])
        );
    @endphp
    <index-evaluacion routes='@json($routes)' valor_evaluacion='@json($valor_evaluacion)' permissions='@json($permisos)' ></index-evaluacion>
@endsection
