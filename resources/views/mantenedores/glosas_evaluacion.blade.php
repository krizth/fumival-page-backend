@extends('layouts.app')
@section('content')
    @php
        $routes=["delete"=>route("delete-glosa_evaluacion"),"update"=>route("update-glosa_evaluacion"),"create"=>route("create-glosa_evaluacion"),"store"=>route("write-glosa_evaluacion")];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-glosa_evaluacion']),
            "edit" => $user->hasAccess(['update-glosa_evaluacion']),
            "delete" => $user->hasAccess(['delete-glosa_evaluacion'])
        );
    @endphp
    <index-glosas-evaluacion routes='@json($routes)' glosa_evaluacion='@json($glosa_evaluacion)' permissions='@json($permisos)'></index-glosas-evaluacion>
@endsection
