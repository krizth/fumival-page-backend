@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();
        $routes=["delete"=>route("delete-unidad_medida"),"update"=>route("update-unidad_medida"),"create"=>route("write-unidad_medida"),"store"=>route("write-unidad_medida")];

        $permisos = array(
            "create" => $user->hasAccess(['write-unidad_medida']),
            "edit" => $user->hasAccess(['update-unidad_medida']),
            "delete" => $user->hasAccess(['delete-unidad_medida']),
        );

    @endphp
    <index-unidad-medida routes='@json($routes)' unidad_medida='@json($unidad_medida)' permissions='@json($permisos)'></index-unidad-medida>
@endsection
