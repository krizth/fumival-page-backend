@extends('layouts.app')
@section('content')
    @php
        $routes=["delete"=>route("delete-medio_pago"),"update"=>route("update-medio_pago"),"store"=>route("write-medio_pago")];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-medio_pago']),
            "edit" => $user->hasAccess(['update-medio_pago']),
            "delete" => $user->hasAccess(['delete-medio_pago'])
        );
    @endphp
    <index-medio-pago routes='@json($routes)' medio_pago='@json($medio_pago)' permissions='@json($permisos)'></index-medio-pago>
@endsection
