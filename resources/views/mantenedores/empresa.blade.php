@extends('layouts.app')
@section('content')
    @php
        $routes=["delete"=>route("delete-empresa"),"update"=>route("update-empresa"),"create"=>route("create-empresa"),"store"=>route("write-empresa")];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-empresa']),
            "edit" => $user->hasAccess(['update-empresa']),
            "delete" => $user->hasAccess(['delete-empresa'])
        );
    @endphp
    <index-empresa routes='@json($routes)' empresa='@json($empresa)' permissions='@json($permisos)'></index-empresa>
@endsection
