@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();
        $routes=[
            "delete"=>route("delete-bodega"),
            "update"=>route("update-bodega"),
            "create"=>route("write-bodega"),
            "store"=>route("write-bodega")
        ];

        $permissions = [
            'create'=>$user->hasAccess(['write-bodega']),
            'edit' => $user->hasAccess(['update-bodega']),
            'delete'=>$user->hasAccess(['delete-bodega'])
        ];
    @endphp

    <index-bodega routes='@json($routes)' permissions='@json($permissions)' bodegas='@json($bodegas)'></index-bodega>
@endsection
