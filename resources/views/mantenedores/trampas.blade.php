@extends('layouts.app')
@section('content')
    @php
        $routes=["delete"=>route("delete-trampas"),"update"=>route("update-trampas"),"store"=>route("write-trampas"),"show"=>route("read-trampa")];
        $user = Auth::user();
        $permisos = array(
            "create" => $user->hasAccess(['write-trampas']),
            "edit" => $user->hasAccess(['update-trampas']),
            "delete" => $user->hasAccess(['delete-trampas']),
            "show" =>$user->hasAccess(["read-trampas"])
        );
    @endphp
    <index-trampas routes='@json($routes)' trampas='@json($trampas)' permissions='@json($permisos)'></index-trampas>
@endsection
