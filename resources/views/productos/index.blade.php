@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();
        $routes=[

            "productos" => [
                "delete"=>route("delete-productos"),
                "update"=>route("update-productos"),
                "create"=>route("create-productos"),
                "store"=>route("write-productos"),
                "factor" => route("read-factor_conversion"),
                "unidades" => route("read-unidad_medida"),
            ],
            "preset" => [
                "delete"=>route("delete-preset"),
                "update"=>route("update-preset"),
                "create"=>route("create-preset"),
                "store"=>route("write-preset"),
                "factor" => route("read-factor_conversion"),
                "unidades" => route("read-unidad_medida"),
                "componentes" => url("preset/componentes"),
                "factor" => url("preset/factores")
            ],


        ];

        $permissions=[
            'producto' =>[
                'create'=>$user->hasAccess(['write-productos']),
                'edit'=>$user->hasAccess(['update-productos']),
                'delete'=>$user->hasAccess(['delete-productos'])
            ],
            'preset' => [
                'create'=>$user->hasAccess(['write-preset']),
                'edit'=>$user->hasAccess(['update-preset']),
                'delete'=>$user->hasAccess(['delete-preset'])
            ]
        ];

        $preset = [];
        $producto = [];

        foreach ($productos as $value) {
            if ($value['compuesto'] === 1) {
                $preset[] = $value;
                continue;
            }

            $producto[] = $value;

        }


    @endphp
    <index-productos routes='@json($routes)' productos='@json($producto)' presets='@json($preset)' permissions='@json($permissions)'></index-productos>
@endsection
