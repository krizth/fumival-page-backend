<!DOCTYPE html>
<html>
<head>
    <title>Fumival</title>
</head>
<body class="fonted">
<div class="center background">
    <div class="banner">
        <img  src="http://www.fumival.cl/wordpress/wp-content/uploads/2020/11/cropped-FUMIVAL_hor-01.png" />
        <div>
            <h3>Gracias por preferir nuestros servicios!</h3>
        </div>
    </div>
      <div class="mail-body">
         <p>Estimado Cliente: {{$ClientData["nombre"]}}</p>
          <p>Se adjunta certificado por servicios prestados el día: {{$ClientData["fecha"]}}</p>
      </div>
</div>
</body>
</html>
<style>
    .center {
        margin: auto;
        padding: 30px;
    }
    .banner{
        text-align: center;
    }
    .fonted{
        font-family: -webkit-pictograph;
    }
    .mail-body{
        text-align: center;
        font-weight: bold;

    }
    .background{
        background-position: center center;
        background: linear-gradient(rgba(255,255,255,.7), rgba(255,255,255,.7)), url("http://www.fumival.cl/wordpress/wp-content/uploads/2020/04/plagas-1.jpg");
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
