
@extends('layouts.app')

@section('content')
@php
    $user = Auth::user();
    $permissions = [
        'create'=>$user->hasAccess(['write-users']),
        'edit'=>$user->hasAccess(['update-users']),
        'delete'=>$user->hasAccess(['delete-users'])
    ];
    $routes=[
    "delete"=>route("delete-users"),
    "update"=>route("update-users"),
    "create"=>route("create-users"),
    "store"=>route("write-users"),
    "read" => route("read-users")
    ];
@endphp

<index-users users='@json($users)' routes='@json($routes)' permissions = '@json($permissions)'></index-users>

@endsection
