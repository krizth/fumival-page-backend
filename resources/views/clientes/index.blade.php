@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();

        $routes=[
            "delete"=>route("delete-clientes"),
            "update"=>route("update-clientes"),
            "store"=>route("write-clientes"),
            "read" =>route("read-clientes"),
            "inmuebles"=>route("read-inmuebles")
        ];

        $permissions=['create'=>$user->hasAccess(['write-clientes']), 'edit'=>$user->hasAccess(['write-clientes']), 'delete'=>$user->hasAccess(['delete-clientes'])];
    @endphp
    <index-clientes routes='@json($routes)' clientes='@json($clientes)' permissions='@json($permissions)'></index-clientes>
@endsection
