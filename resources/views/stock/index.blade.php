@extends('layouts.app')

@section('content')

@php
    $user = Auth::user();
    $routes=[

        "stock" => [
            "delete" => route("delete-stock"),
            "update" => route("update-stock"),
            "store" => route("write-stock"),
            "bodegas" => route("read-bodega"),
            "productos" => route("read-producto"),
            "producto" => url("/producto")
        ],
        "stockUsuarios" => [
                "delete" => route("delete-stock_usuarios"),
                "update" => route("update-stock_usuarios"),
                "store" => route("write-stock_usuarios"),
                "bodegas" => route("read-bodega"),
                "usuarios" => route("read-users"),
                "productos" => route("read-producto"),
                "producto" => url("/producto")
        ],
    ];
    $permissions= [
        'productos' => [
            'create' => $user->hasAccess(['write-stock']),
            'edit' => $user->hasAccess(['update-stock']),
            'delete' => $user->hasAccess(['delete-stock'])
        ],
        'usuarios' => [
            'create' => $user->hasAccess(['write-stock_usuarios']),
            'edit' => $user->hasAccess(['update-stock_usuarios']),
            'delete' => $user->hasAccess(['delete-stock_usuarios'])
        ]
    ];

@endphp

<index-stock routes='@json($routes)' stock='@json($stock)' user='{{$user->nombre}}' permissions='@json($permissions)'>
</index-stock>
@endsection
