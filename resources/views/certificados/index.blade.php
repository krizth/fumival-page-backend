@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();

        $routes=["delete"=>route("delete-certificado"),"update"=>route("update-clientes"),"create"=>route("write-certificado"),"store"=>route("write-clientes")];

        $permissions=['info'=>$user->hasAccess(['read-certificado']),'create'=>$user->hasAccess(['write-certificado'])];
    @endphp
    <index-certificados routes='@json($routes)' certificados='@json($certificados)' permissions='@json($permissions)'></index-certificados>
@endsection
