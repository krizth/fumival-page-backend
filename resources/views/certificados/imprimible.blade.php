<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        a {
            text-decoration: none
        }


    </style>
    <style type="text/css">

        .footer {
            -pdf-frame-content: footerContent;
            bottom: 2cm;
            margin-left: 1cm;
            margin-right: 1cm;
            height: 1cm;
        }
        @media print {
            @page  {
                size: A4;
                page-break-after: always;
            }
            .pagebreak { max-width: available }
            thead { display: table-header-group; }
            tfoot { display: table-row-group;}
            tr { page-break-inside: avoid; }
        }
    </style>
</head>

<body text="#000000" link="#000000" alink="#000000" vlink="#000000">
<table  cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td width="50%">&nbsp;</td>
        <td align="center">

                <a name="JR_PAGE_ANCHOR_0_1"></a>
                <table class="jrPage" data-jr-height="801" cellpadding="0" cellspacing="0" border="0"
                    style="empty-cells: show; width: 612px; border-collapse: collapse; background-color: white;">
                    <tr valign="top" style="height:0">
                        <td style="width:20px"></td>
                        <td style="width:40px"></td>
                        <td style="width:10px"></td>
                        <td style="width:30px"></td>
                        <td style="width:70px"></td>
                        <td style="width:20px"></td>
                        <td style="width:40px"></td>
                        <td style="width:1px"></td>
                        <td style="width:29px"></td>
                        <td style="width:40px"></td>
                        <td style="width:98px"></td>
                        <td style="width:1px"></td>
                        <td style="width:41px"></td>
                        <td style="width:1px"></td>
                        <td style="width:9px"></td>
                        <td style="width:16px"></td>
                        <td style="width:100px"></td>
                        <td style="width:24px"></td>
                        <td style="width:1px"></td>
                        <td style="width:1px"></td>
                        <td style="width:20px"></td>
                    </tr>
                    <tr valign="top" style="height:5px">
                        <td>
                        </td>
                        <td colspan="3" rowspan="2" style=" font-family:sans-serif; color: #000000; font-size: 14px; line-height: 1.2578125; text-indent: 0px;  vertical-align: bottom;text-align: left;">
                            {{isset($emp->nombre)?$emp->nombre:""}}
                        </td>
                        <td colspan="6" rowspan="2" style="text-indent: 0px;  vertical-align: bottom;text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Servicios
                                Profesionales E.I.R.L
                            </span></td>
                    <td colspan="12">
                    </td>
                </tr>
                    <tr valign="top" >
                    <td>
                    </td>
                    <td colspan="4">
                    </td>
                    <td colspan="6" rowspan="5">
                        <img src="{{isset($emp->logo)?$emp->logo:""}}" style="height: 65px;align-content: center" alt="" /></td>
                    <td>
                    </td>
                </tr>
                    <tr valign="top">
                    <td>
                    </td>
                    <td colspan="9" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:9px;  text-align: left;">
                          R.U.T.{{isset($emp->rut)?$emp->rut:""}}
                   <br/>
                        Rep.Leg.:{{isset($emp->repLegal)?$emp->repLegal:""}} - R.U.T.: {{isset($emp->rutRepLegal)?$emp->rutRepLegal:""}}
                        <br/>
                        {{isset($emp->direccion)?$emp->direccion:""}}
                        <br/>
                        Fono: {{isset($emp->fono)?$emp->fono:""}}
                        <br/>
                        Cel.: {{isset($emp->celular)?$emp->celular:""}}
                        <br/>
                        {{isset($emp->correo)?$emp->correo:""}} - {{isset($emp->pagina_web)?$emp->pagina_web:""}}
                        <br/>
                        {{isset($emp->otros)?$emp->otros:""}}
                    </td>
                        <td colspan="9">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" >
                        <td>
                        </td>
                        <td colspan="8" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:1px; text-align: left;">
                         </td>
                        <td colspan="4">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" >
                        <td>
                        </td>
                        <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                        <td colspan="4">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" >
                        <td>
                        </td>
                        <td colspan="8" style="text-indent: 0px;  text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                        <td colspan="4">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" >
                        <td>
                        </td>
                        <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                        <td colspan="7">
                        </td>
                        <td rowspan="3">
                            <div style=" height: 100%; position: relative;">
                                <div style="position: absolute; overflow: hidden;  height: 100%; ">
                                    <table cellpadding="0" cellspacing="0" border="0"

                                        style="empty-cells: show; border:black solid 1px; border-radius: 20px;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:96px"></td>
                                        </tr>
                                        <tr valign="top" style="height:25px">
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="position: relative;  height: 100%; pointer-events: none; ">
                                    <table cellpadding="0" border="0"
                                        style="empty-cells: show; ">
                                        <tr valign="top" style="height:0">
                                            <td style="width:96px"></td>
                                        </tr>
                                        <tr valign="top" style="height:27px;">
                                            <td
                                                style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: center;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 18px; line-height: 1;">{{$certificado->folio}}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="4">
                        </td>
                    </tr>
                    <tr valign="top" style="height:14px">
                        <td colspan="3">
                        </td>
                        <td colspan="12" rowspan="2"
                            style="text-indent: 0px;  vertical-align: middle;text-align: center;">
                            <div style="padding-left:20px;"><span
                                    style="font-family: Arial Black; color: #000000; font-size: 16px; line-height: 1; *line-height: normal;">CERTIFICADO
                                    DE TRATAMIENTO</span></div>
                        </td>
                        <td>
                        </td>
                        <td colspan="4">
                        </td>
                    </tr>


                    <tr valign="top" style="height:11px">
                        <td colspan="3">
                        </td>
                        <td colspan="6">
                        </td>
                    </tr>
                    <tr valign="top" style="height:15px">
                        <td>
                        </td>
                        <td colspan="9" style="text-indent: 0px; text-align: left;">
                            <span style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Fecha: {{\Carbon\Carbon::createFromFormat('Y-m-d',$certificado->fecha)->format('d/m/Y')}}</span></td>
                        <td colspan="11">
                        </td>
                    </tr>
                    <tr valign="top" style="height:3px">
                        <td colspan="21">
                        </td>
                    </tr>
                    <tr valign="top" style="height:143px">
                        <td>
                        </td>
                        <td colspan="17">
                            <div style=" height: 100%; position: relative;">
                                <div style="position: absolute; overflow: hidden;  height: 100%; ">
                                </div>
                                <div style="position: relative;  height: 100%; pointer-events: none; ">
                                    <table cellpadding="0"  border="0"
                                        style="empty-cells: show; border:1px solid black; border-radius: 10px;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:180px"></td>
                                            <td style="width:198px"></td>
                                            <td style="width:192px"></td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="3"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family: sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Inmueble
                                                            Tratado:</b> {{$certificado->inmueble['nombre']}}</span></div>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="2"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Dirección</b>:
                                                        {{$certificado->inmueble['direccion']}}</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b>Ciudad</b>:
                                                    {{$certificado->inmueble['ciudad']}}</span>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="2"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Propietario:</b>
                                                        {{$certificado->inmueble['propietario']['nombre']}} </span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b>R.U.T.:</b>
                                                    {{$certificado->inmueble['propietario']['rut']}}</span>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="2"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Dirección:</b>
                                                        {{$certificado->inmueble['propietario']['direccion_principal']}}</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b>Ciudad:</b>
                                                    {{$certificado->inmueble['propietario']['ciudad']}}</span>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="2"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Encomienda
                                                            Trabajo:</b> {{$certificado->cliente['nombre']}}</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b>R.U.T.:</b>
                                                    {{$certificado->cliente['rut']}}</span>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="2"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Dirección:</b>
                                                        {{$certificado->cliente['direccion_principal']}}</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b>Ciudad:</b>
                                                    {{$certificado->cliente['ciudad']}}</span>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:18px">
                                            <td
                                                style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Fono:</b>
                                                        {{isset($certificado->cliente['fono'])?$certificado->cliente['fono']:'N/A'}}</span></div>
                                            </td>
                                            <td colspan="2"
                                                style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b>E-mail:</b>
                                                    {{isset($certificado->cliente['email'])?$certificado->cliente['email']:'N/A'}}</span>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:1px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr valign="top" style="height:11px">
                        <td colspan="21">
                        </td>
                    </tr>
                    @foreach ($certificado->tratamientos as $tratamiento)
                    <tr valign="top" style="height:60px">
                        <td>
                        </td>
                        <td>
                            <div style=" height: 100%; position: relative;">
                                <div style="position: absolute; overflow: hidden;  height: 100%; ">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                        style="empty-cells: show;  border-collapse: collapse;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:40px"></td>
                                        </tr>
                                        <tr valign="top" style="height:60px">
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="position: relative;  height: 100%; pointer-events: none; ">
                                    <table cellpadding="0"  border="0"

                                        style="empty-cells: show;  border: 1px solid black; border-radius: 10px; ">
                                        <tr valign="top" style="height:0">
                                            <td style="width:40px"></td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td
                                                style="pointer-events: auto;  text-indent: 0px;  vertical-align: middle;text-align: center; ">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;">
                                                        <b style=" padding:3px;">{{$tratamiento['tipoTratamiento']['abreviatura']}}</b></span></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="16">
                            <div style=" height: 100%; position: relative;">
                                <div style="position: absolute; overflow: hidden;  height: 100%; ">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                        style="empty-cells: show;  border-collapse: collapse;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:530px"></td>
                                        </tr>
                                        <tr valign="top" style="height:60px">
                                            <td>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="position: relative;  height: 100%; pointer-events: none; ">
                                    <table cellpadding="0"  border="0"
                                        style="empty-cells: show;  border: 1px solid black; border-radius: 10px;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:210px"></td>
                                            <td style="width:90px"></td>
                                            <td style="width:20px"></td>
                                            <td style="width:80px"></td>
                                            <td style="width:130px"></td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Producto:</b>
                                                        {{$tratamiento['producto']['nombre']}}</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style=" display:inline-block;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 9px; line-height: 1; *line-height: normal; display: flex;"><b>Reg.
                                                            ISP:</b> {{$tratamiento['producto']['regISP']}}</span></div>
                                            </td>
                                            <td colspan="2"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Dosis:</b>
                                                        {{$tratamiento['dosis']}}</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Total:</b>
                                                        {{$tratamiento['total']}} {{$tratamiento["factor_conversion"]["descripcion"]}}</span></div>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="2"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Ingrediente
                                                            Activo:</b>
                                                        {{$tratamiento['producto']['ing_activo']}}</span></div>
                                            </td>
                                            <td colspan="3"
                                                style="pointer-events: auto; border-bottom: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Concentración:</b>
                                                        {{$tratamiento['concentracion']}}</span></div>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height:20px">
                                            <td colspan="3"
                                                style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Lugares:</b>
                                                        {{$tratamiento['lugares']}}</span></div>
                                            </td>
                                            <td colspan="2"
                                                style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Fecha
                                                            Termino:</b> {{$tratamiento['vigencia']}}</span></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr valign="top" style="height:4px">
                        <td colspan="21">
                        </td>
                    </tr>
                    @endforeach
                    <tr valign="top" style="height:8px">
                        <td colspan="22">
                        </td>
                    </tr>
                    <tr valign="top" style="height:20px">
                        <td>
                        </td>
                        <td colspan="12" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Evaluaciones</span>
                        </td>
                        <td colspan="9">
                        </td>
                    </tr>

                    <tr valign="center" style="height:18px">
                        <td>
                        </td>
                        <td colspan="11" style="padding-bottom: 20px">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                           style="border:1px solid black; border-radius: 10px"
                                        style="empty-cells: show; ">
                                        @foreach ($certificado->evaluaciones as $evaluacion)
                                        <tr valign="center" style="height:0">
                                            <td style="width:176px"></td>
                                            <td style="width:203px"></td>
                                        </tr>
                                        <tr valign="center" style="height:18px">
                                            @if($loop->index!=0)
                                            <td style="pointer-events: auto; text-indent: 5px; text-align: left; border-top: 1px solid black">

                                                @else
                                                <td style="pointer-events: auto; text-indent: 5px; text-align: left; ">
                                                    @endif
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b></b>
                                                    {{$evaluacion['glosa_evaluacion']['criterio']}}</span>
                                            </td>
                                                @if($loop->index!=0)
                                                    <td style="pointer-events: auto; text-indent: 5px; text-align: left; border-top: 1px solid black">
                                                @else
                                                    <td style="pointer-events: auto; text-indent:5px; text-align: left; ">
                                                        @endif
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">
                                                    {{$evaluacion['evaluacion_textual']}}</span>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>

                        </td>
                        <td colspan="10">
                        </td>
                    </tr>
                    <tr valign="center" >
                        <td>
                        </td>
                        <td colspan="18">
                                    <table style="float: right;display: flex;align-items: center;word-break:break-all;margin: 0 0 1em 1em; empty-cells: show; width: 100%;  border: 1px solid black; border-radius: 10px;">
                                        <tr valign="center" style="height:20px ;">
                                            <td style="pointer-events: auto; text-indent: 0px; text-align: left;">
                                               <span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; line-height: normal;">Observaciones - Recomendaciones:
                                                        @if((count($certificado->tratamientos)>=3)&&(substr_count($certificado->observaciones,"\n")<=10))
                                                           <p>{!! nl2br(trim($certificado->observaciones)) !!}</p>
                                                        @else
                                                            @if((count($certificado->tratamientos)==2)&&(substr_count($certificado->observaciones,"\n")<=20))
                                                            <p>{!! nl2br($certificado->observaciones) !!}</p>
                                                           @else
                                                               @if((count($certificado->tratamientos)==1)&&(substr_count($certificado->observaciones,"\n")<=30))
                                                               <p>{!! nl2br($certificado->observaciones) !!}</p>
                                                               @else
                                                               <p>Se adjunta documentacion en la siguiente página</p>

                                                               @endif
                                                           @endif
                                                        @endif
                                                   @if(isset($certificado->imagenes))
                                                       <p>Se adjuntan imagenes</p>
                                                   @endif
                                                    </span>

                                            </td>
                                            <td>
                                            </td>
                                        </tr>

                                        <!--<tr valign="top" style="height:132px">
                                            <td colspan="3">
                                            </td>
                                        </tr>-->

                                    </table>


                        </td>
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr valign="top" style="height:20px">
                        <td>
                        </td>
                        <td colspan="6" style="text-indent: 0px;  vertical-align: middle;text-align: left;">
                            <div style="padding-left:5px;"><span
                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Hora
                                        Inicio:</b> {{$certificado->hora_inicio}}</span></div>
                        </td>
                        <td colspan="6" style="text-indent: 0px;  vertical-align: middle;text-align: left;">
                            <div style="padding-left:5px;"><span
                                    style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;"><b>Hora
                                        Termino:</b> {{$certificado->hora_termino}}</span></div>
                        </td>
                        <td>
                        </td>
                        <td colspan="5" style="text-indent: 0px; text-align: center;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">@if($certificado->folio!=null)
                                    {{$certificado->firmas['firmante']["nombre"]}}
                                @else
                                {{'**Nombre Firmante**'}}
                                @endif
                            </span>
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr valign="top" style="height:20px">
                        <td>
                        </td>
                        <td colspan="4" rowspan="2">
                            <div style=" height: 100%; position: relative;">
                                <div style="position: absolute; overflow: hidden;  height: 100%; ">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                        style="empty-cells: show;  border-collapse: collapse;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:150px"></td>
                                        </tr>
                                        <tr valign="top" style="height:27px">
                                            <td>
                                                <svg height="30" width="150">
                                                    <rect x="0.5" y="0.5" rx="10" ry="10" height="29.0" width="149.0"
                                                        style="fill:#FFFFFF;stroke:#000000;stroke-width:1.0;" /></svg>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="position: relative;  height: 100%; pointer-events: none; ">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                        style="empty-cells: show;  border-collapse: collapse;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:40px"></td>
                                            <td style="width:110px"></td>
                                        </tr>
                                        <tr valign="top" style="height:30px">
                                            <td
                                                style="pointer-events: auto; border-right: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;">Valor</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;">{{$certificado->valor}}</span></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td>
                        </td>
                        <td colspan="5" rowspan="2">
                            <div style=" height: 100%; position: relative;">
                                <div style="position: absolute; overflow: hidden;  height: 100%; ">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                        style="empty-cells: show;  border-collapse: collapse;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:208px"></td>
                                        </tr>
                                        <tr valign="top" style="height:30px">
                                            <td>
                                                <svg height="30" width="208">
                                                    <rect x="0.5" y="0.5" rx="10" ry="10" height="29.0" width="207.0"
                                                        style="fill:#FFFFFF;stroke:#000000;stroke-width:1.0;" /></svg>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="position: relative;  height: 100%; pointer-events: none; ">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                        style="empty-cells: show;  border-collapse: collapse;">
                                        <tr valign="top" style="height:0">
                                            <td style="width:110px"></td>
                                            <td style="width:98px"></td>
                                        </tr>
                                        <tr valign="top" style="height:30px">
                                            <td
                                                style="pointer-events: auto; border-right: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;">Condición
                                                        de Pago</span></div>
                                            </td>
                                            <td
                                                style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: left;">
                                                <div style="padding-left:5px;"><span
                                                        style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; *line-height: normal;">{{$certificado->cond_pago}}</span></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td colspan="3">
                        </td>
                        <td colspan="5" style="border-top: 1px solid #000000; text-indent: 0px; text-align: center;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Nombre
                                Cliente</span></td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr valign="top" style="height:10px">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td colspan="3">
                        </td>
                        <td colspan="5" rowspan="2" style="text-align: center;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;"><b>R.U.T.:</b>
                                @if($certificado->folio!=null)
                                {{$certificado->firmas['firmante']["rut"]}}
                                @else
                                {{'**RUT Firmante**'}}
                                @endif
                            </span>
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr valign="top" style="height:10px">
                        <td colspan="14">
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr valign="top" style="height:10px ">
                        <td>
                        </td>
                        <td colspan="4" rowspan="2" style="text-align: center;">

                            @if($certificado->folio!=null)
                            <img src="{{$certificado->firmas['tecnico']}}" style="height: 50px ;text-align: center;"  alt="" />
                            @else
                                <span>
                                    {{'**Firma Tecnico**'}}

                            </span>
                            @endif
                        </td>
                        <td colspan="16">
                        </td>
                    </tr>
                    <tr valign="top" style="height:40px; text-align: center;">
                        <td>
                        </td>
                        <td colspan="3">
                        </td>
                        <td colspan="4" rowspan="2" style="text-align: center;">
                            @if($certificado->folio!=null)
                            <img src="{{$certificado->firmas['responsable']}}" style="height: 50px; align-content: center" alt="" />
                        @else
                            <span>

                                {{'**Firma R. Tecnico**'}}
                            </span>
                        @endif
                        </td>
                        <td colspan="2">
                        </td>
                        <td colspan="6" rowspan="2" style="text-align: center;">
                            @if($certificado->folio!=null)
                            <img src="{{$certificado->firmas['cliente']}}" style="height: 50px" alt="" />
                            @else
                                <span>
                                    {{'**Firma Cliente**'}}
                            </span>
                            @endif
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" style="height:10px">
                        <td>
                        </td>
                        <td colspan="4" rowspan="3" style="text-indent: 0px; text-align: center;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">{{$certificado->usuario['nombre']}}</span>
                        </td>
                        <td colspan="3">
                        </td>
                        <td colspan="2">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" style="height:2px">
                        <td>
                        </td>
                        <td colspan="8">
                        </td>
                        <td colspan="7" rowspan="4"
                            style="border-top: 1px solid #000000; text-indent: 0px; text-align: center;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Firma
                                Cliente.</span></td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" style="height:8px">
                        <td>
                        </td>
                        <td colspan="2">
                        </td>
                        <td colspan="4" rowspan="3" style=" text-indent: 0px;">
                            <span style=" text-align: center; font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">
                                {{$tecnico_cargo}}
                            </span>
                            <span style=" font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Responsable Técnico</span>
                        </td>
                        <td colspan="2">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" style="height:20px">
                        <td>
                        </td>
                        <td colspan="4" rowspan="2"
                            style="border-top: 1px solid #000000; text-indent: 0px; text-align: center;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Nombre
                                y Firma Tecnico</span></td>
                        <td colspan="2">
                        </td>
                        <td colspan="2">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr valign="top" style="height:2px">
                        <td>
                        </td>
                        <td colspan="2">
                        </td>
                        <td colspan="10">
                        </td>
                    </tr>
                    <tr valign="top" style="height:7px">
                        <td colspan="21">
                        </td>
                    </tr>
                </table>

            </td>
            <td width="50%">&nbsp;</td>
        </tr>

            @if((count($certificado->tratamientos)>=3)&&(substr_count($certificado->observaciones,"\n")<=10))

            @else
                @if((count($certificado->tratamientos)==2)&&(substr_count($certificado->observaciones,"\n")<=20))

                @else
                    @if((count($certificado->tratamientos)==1)&&(substr_count($certificado->observaciones,"\n")<=30))

                    @else
                <tr style="page-break-before: always;">
                    <td width="50%">&nbsp;</td>
                        <td  style="page-break-inside:auto; page-break-after:always; margin-bottom: 50px; position:center; ">
                            <table
                                style="empty-cells: show; width: 612px; border-collapse: collapse; background-color: white;">
                                <tr valign="top" style="height:0">
                                    <td style="width:20px"></td>
                                    <td style="width:40px"></td>
                                    <td style="width:10px"></td>
                                    <td style="width:30px"></td>
                                    <td style="width:70px"></td>
                                    <td style="width:20px"></td>
                                    <td style="width:40px"></td>
                                    <td style="width:1px"></td>
                                    <td style="width:29px"></td>
                                    <td style="width:40px"></td>
                                    <td style="width:98px"></td>
                                    <td style="width:1px"></td>
                                    <td style="width:41px"></td>
                                    <td style="width:1px"></td>
                                    <td style="width:9px"></td>
                                    <td style="width:16px"></td>
                                    <td style="width:100px"></td>
                                    <td style="width:24px"></td>
                                    <td style="width:1px"></td>
                                    <td style="width:1px"></td>
                                    <td style="width:20px"></td>
                                </tr>
                                <tr valign="top" style="height:5px">
                                    <td>
                                    </td>
                                    <td colspan="3" rowspan="2" style=" font-family:sans-serif; color: #000000; font-size: 14px; line-height: 1.2578125; text-indent: 0px;  vertical-align: bottom;text-align: left;">
                                        {{isset($emp->nombre)?$emp->nombre:""}}
                                    </td>
                                    <td colspan="6" rowspan="2" style="text-indent: 0px;  vertical-align: bottom;text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Servicios
                                Profesionales E.I.R.L
                            </span></td>
                                    <td colspan="12">
                                    </td>
                                </tr>
                                <tr valign="top" >
                                    <td>
                                    </td>
                                    <td colspan="4">
                                    </td>
                                    <td colspan="6" rowspan="5">
                                        <img src="{{isset($emp->logo)?$emp->logo:""}}" style="height: 65px;align-content: center" alt="" /></td>
                                    <td>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>
                                    </td>
                                    <td colspan="9" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:9px;  text-align: left;">
                                        R.U.T.{{isset($emp->rut)?$emp->rut:""}}
                                        <br/>
                                        Rep.Leg.:{{isset($emp->repLegal)?$emp->repLegal:""}} - R.U.T.: {{isset($emp->rutRepLegal)?$emp->rutRepLegal:""}}
                                        <br/>
                                        {{isset($emp->direccion)?$emp->direccion:""}}
                                        <br/>
                                        Fono: {{isset($emp->fono)?$emp->fono:""}}
                                        <br/>
                                        Cel.: {{isset($emp->celular)?$emp->celular:""}}
                                        <br/>
                                        {{isset($emp->correo)?$emp->correo:""}} - {{isset($emp->pagina_web)?$emp->pagina_web:""}}
                                        <br/>
                                        {{isset($emp->otros)?$emp->otros:""}}
                                    </td>
                                    <td colspan="9">
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr valign="top" >
                                    <td>
                                    </td>
                                    <td colspan="8" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:1px; text-align: left;">
                                    </td>
                                    <td colspan="4">

                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr valign="top" >
                                    <td>
                                    </td>
                                    <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                                    <td colspan="4">
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr valign="top" >
                                    <td>
                                    </td>
                                    <td colspan="8" style="text-indent: 0px;  text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                                    <td colspan="4">
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr valign="top" >
                                    <td>
                                    </td>
                                    <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                                    <td colspan="7">
                                    </td>
                                    <td rowspan="3">
                                        <div style=" height: 100%; position: relative;">
                                            <div style="position: absolute; overflow: hidden;  height: 100%; ">
                                                <table cellpadding="0" cellspacing="0" border="0"

                                                       style="empty-cells: show; border:black solid 1px; border-radius: 20px;">
                                                    <tr valign="top" style="height:0">
                                                        <td style="width:96px"></td>
                                                    </tr>
                                                    <tr valign="top" style="height:25px">
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="position: relative;  height: 100%; pointer-events: none; ">
                                                <table cellpadding="0" border="0"
                                                       style="empty-cells: show; ">
                                                    <tr valign="top" style="height:0">
                                                        <td style="width:96px">

                                                        </td>
                                                    </tr>
                                                    <tr valign="top" style="height:27px;">
                                                        <td
                                                            style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: center;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 18px; line-height: 1;">{{$certificado->folio}}</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="4">
                                    </td>
                                </tr>
                                <tr valign="top" style="height:14px">
                                    <td colspan="3">
                                    </td>
                                    <td colspan="12" rowspan="2"
                                        style="text-indent: 0px;  vertical-align: middle;text-align: center;">
                                    </td>
                                    <td>
                                    </td>
                                    <td colspan="4">
                                    </td>
                                </tr>
                            </table>
                            <div>

                                <span style="font-family:sans-serif; color: #000000; font-size: 10px; position:absolute">Fecha: {{\Carbon\Carbon::createFromFormat('Y-m-d',$certificado->fecha)->format('d/m/Y')}}</span>
                            </div>
                            <table style="float: right;display: flex;align-items: center;word-break:break-all;margin: 1em 0 1em 1em; empty-cells: show; width: 100%;  border: 1px solid black; border-radius: 10px;">

                                <tr valign="center" style="height:20px; ">
                                    <td style="pointer-events: auto; text-indent: 0px; text-align: left;">
                                               <span
                                                   style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1; line-height: normal;">Observaciones - Recomendaciones:

                                                       <p>{!! nl2br($certificado->observaciones) !!}</p>

                                                    </span>

                                    </td>
                                    <td >
                                    </td>
                                </tr>
                                    <tr valign="top" style="height:132px;  ">
                                        <td colspan="2">
                                        </td>
                                    </tr>

                            </table>
                        </td>
                </tr>
                    @endif
                @endif
            @endif






        <tr >

            <td width="50%">&nbsp;</td>
            <td style="   margin-bottom: 50px; position:center; ">


            </td>
        </tr>




    </table>
<div
class="pagebreak"></div>
@if (isset($certificado->imagenes))
    <div style="  page-break-before: always; align-content: center; text-align: center; margin-left: auto;
	margin-right: auto;

	float: none;">
        <table
            style="margin: auto; page-break-after: always;  empty-cells: show; width: 612px;  background-color: white;">

            <tr valign="top" style="height:0">
                <td style="width:20px"></td>
                <td style="width:40px"></td>
                <td style="width:10px"></td>
                <td style="width:30px"></td>
                <td style="width:70px"></td>
                <td style="width:20px"></td>
                <td style="width:40px"></td>
                <td style="width:1px"></td>
                <td style="width:29px"></td>
                <td style="width:40px"></td>
                <td style="width:98px"></td>
                <td style="width:1px"></td>
                <td style="width:41px"></td>
                <td style="width:1px"></td>
                <td style="width:9px"></td>
                <td style="width:16px"></td>
                <td style="width:100px"></td>
                <td style="width:24px"></td>
                <td style="width:1px"></td>
                <td style="width:1px"></td>
                <td style="width:20px"></td>
            </tr>
            <tr valign="top" style="height:5px">
                <td >
                </td>
                <td colspan="3" rowspan="2" style=" font-family:sans-serif; color: #000000; font-size: 14px; line-height: 1.2578125; text-indent: 0px;  vertical-align: bottom;text-align: left;">
                    {{isset($emp->nombre)?$emp->nombre:""}}
                </td>
                <td colspan="6" rowspan="2" style="text-indent: 0px;  vertical-align: bottom;text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Servicios
                                Profesionales E.I.R.L
                            </span></td>
                <td colspan="12">
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="4">
                </td>
                <td colspan="6" rowspan="5">
                    <img src="{{isset($emp->logo)?$emp->logo:""}}" style="height: 65px;align-content: center" alt="" /></td>
                <td>
                </td>
            </tr>
            <tr valign="top">
                <td>
                </td>
                <td colspan="9" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:9px;  text-align: left;">
                    R.U.T.{{isset($emp->rut)?$emp->rut:""}}
                    <br/>
                    Rep.Leg.:{{isset($emp->repLegal)?$emp->repLegal:""}} - R.U.T.: {{isset($emp->rutRepLegal)?$emp->rutRepLegal:""}}
                    <br/>
                    {{isset($emp->direccion)?$emp->direccion:""}}
                    <br/>
                    Fono: {{isset($emp->fono)?$emp->fono:""}}
                    <br/>
                    Cel.: {{isset($emp->celular)?$emp->celular:""}}
                    <br/>
                    {{isset($emp->correo)?$emp->correo:""}} - {{isset($emp->pagina_web)?$emp->pagina_web:""}}
                    <br/>
                    {{isset($emp->otros)?$emp->otros:""}}
                </td>
                <td colspan="9">
                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:1px; text-align: left;">
                </td>
                <td colspan="4">

                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                <td colspan="4">
                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px;  text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                <td colspan="4">
                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                <td colspan="7">
                </td>
                <td rowspan="3">
                    <div style=" height: 100%; position: relative;">
                        <div style="position: absolute; overflow: hidden;  height: 100%; ">
                            <table cellpadding="0" cellspacing="0" border="0"

                                   style="empty-cells: show; border:black solid 1px; border-radius: 20px;">
                                <tr valign="top" style="height:0">
                                    <td style="width:96px"></td>
                                </tr>
                                <tr valign="top" style="height:25px">
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="position: relative;  height: 100%; pointer-events: none; ">
                            <table cellpadding="0" border="0"
                                   style="empty-cells: show; ">
                                <tr valign="top" style="height:0">
                                    <td style="width:96px">

                                    </td>
                                </tr>
                                <tr valign="top" style="height:27px;">
                                    <td
                                        style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: center;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 18px; line-height: 1;">{{$certificado->folio}}</span>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </td>
                <td colspan="4">
                </td>
            </tr>
            <tr valign="top" style="height:14px">
                <td colspan="3">

                </td>
                <td colspan="12" rowspan="2"
                    style="text-indent: 0px;  vertical-align: middle;text-align: center;">

                </td>
                <td>
                    <!-- imagenes-->

                </td>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td colspan="5" style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125; ">
                    Imagenes:
                </td>
            </tr>
            <tr>
                <td colspan="20" style=" page-break-after: auto;   align-items: center;word-break:break-all;margin: 1em; empty-cells: show; width: 100%;  border: 1px solid black; border-radius: 10px;" >



                           @foreach ($certificado->imagenes as $imagen)
                                @if($loop->index<=3)

                               <img src="{{$imagen}}" style="max-width:285px; margin-top: 20px;" />
                               @endif
                           @endforeach
                </td>

            </tr>

        </table>
        @if(sizeof($certificado->imagenes)>4)
            <div
                class="pagebreak"></div>
        <table
            style="margin: auto;  empty-cells: show; width: 612px;  background-color: white;">

            <tr valign="top" style="height:0">
                <td style="width:20px"></td>
                <td style="width:40px"></td>
                <td style="width:10px"></td>
                <td style="width:30px"></td>
                <td style="width:70px"></td>
                <td style="width:20px"></td>
                <td style="width:40px"></td>
                <td style="width:1px"></td>
                <td style="width:29px"></td>
                <td style="width:40px"></td>
                <td style="width:98px"></td>
                <td style="width:1px"></td>
                <td style="width:41px"></td>
                <td style="width:1px"></td>
                <td style="width:9px"></td>
                <td style="width:16px"></td>
                <td style="width:100px"></td>
                <td style="width:24px"></td>
                <td style="width:1px"></td>
                <td style="width:1px"></td>
                <td style="width:20px"></td>
            </tr>
            <tr valign="top" style="height:5px">
                <td >
                </td>
                <td colspan="3" rowspan="2" style=" font-family:sans-serif; color: #000000; font-size: 14px; line-height: 1.2578125; text-indent: 0px;  vertical-align: bottom;text-align: left;">
                    {{isset($emp->nombre)?$emp->nombre:""}}
                </td>
                <td colspan="6" rowspan="2" style="text-indent: 0px;  vertical-align: bottom;text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125;">Servicios
                                Profesionales E.I.R.L
                            </span></td>
                <td colspan="12">
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="4">
                </td>
                <td colspan="6" rowspan="5">
                    <img src="{{isset($emp->logo)?$emp->logo:""}}" style="height: 65px;align-content: center" alt="" /></td>
                <td>
                </td>
            </tr>
            <tr valign="top">
                <td>
                </td>
                <td colspan="9" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:9px;  text-align: left;">
                    R.U.T.{{isset($emp->rut)?$emp->rut:""}}
                    <br/>
                    Rep.Leg.:{{isset($emp->repLegal)?$emp->repLegal:""}} - R.U.T.: {{isset($emp->rutRepLegal)?$emp->rutRepLegal:""}}
                    <br/>
                    {{isset($emp->direccion)?$emp->direccion:""}}
                    <br/>
                    Fono: {{isset($emp->fono)?$emp->fono:""}}
                    <br/>
                    Cel.: {{isset($emp->celular)?$emp->celular:""}}
                    <br/>
                    {{isset($emp->correo)?$emp->correo:""}} - {{isset($emp->pagina_web)?$emp->pagina_web:""}}
                    <br/>
                    {{isset($emp->otros)?$emp->otros:""}}
                </td>
                <td colspan="9">
                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px; font-family:sans-serif; color: #000000; font-size: 8px; line-height:1px; text-align: left;">
                </td>
                <td colspan="4">

                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                <td colspan="4">
                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px;  text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                <td colspan="4">
                </td>
                <td>
                </td>
            </tr>
            <tr valign="top" >
                <td>
                </td>
                <td colspan="8" style="text-indent: 0px; text-align: left;">
                            <span
                                style="font-family:sans-serif; color: #000000; font-size: 8px; line-height: 1px;"></span></td>
                <td colspan="7">
                </td>
                <td rowspan="3">
                    <div style=" height: 100%; position: relative;">
                        <div style="position: absolute; overflow: hidden;  height: 100%; ">
                            <table cellpadding="0" cellspacing="0" border="0"

                                   style="empty-cells: show; border:black solid 1px; border-radius: 20px;">
                                <tr valign="top" style="height:0">
                                    <td style="width:96px"></td>
                                </tr>
                                <tr valign="top" style="height:25px">
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="position: relative;  height: 100%; pointer-events: none; ">
                            <table cellpadding="0" border="0"
                                   style="empty-cells: show; ">
                                <tr valign="top" style="height:0">
                                    <td style="width:96px">

                                    </td>
                                </tr>
                                <tr valign="top" style="height:27px;">
                                    <td
                                        style="pointer-events: auto; text-indent: 0px;  vertical-align: middle;text-align: center;">
                                                <span
                                                    style="font-family:sans-serif; color: #000000; font-size: 18px; line-height: 1;">{{$certificado->folio}}</span>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </td>
                <td colspan="4">
                </td>
            </tr>
            <tr valign="top" style="height:14px">
                <td colspan="3">

                </td>
                <td colspan="12" rowspan="2"
                    style="text-indent: 0px;  vertical-align: middle;text-align: center;">

                </td>
                <td>
                    <!-- imagenes-->

                </td>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td colspan="5" style="font-family:sans-serif; color: #000000; font-size: 10px; line-height: 1.2578125; ">
                    Imagenes:
                </td>
            </tr>
            <tr>
                <td colspan="20" style=" page-break-after: auto;   align-items: center;word-break:break-all;margin: 1em; empty-cells: show; width: 100%;  border: 1px solid black; border-radius: 10px;" >



                    @foreach ($certificado->imagenes as $imagen)
                        @if($loop->index>3)

                            <img src="{{$imagen}}" style="max-width:285px; margin-top: 20px;" />
                        @endif
                    @endforeach
                </td>
            </tr>

        </table>
        @endif
    </div>
    <div
        class="pagebreak"></div>



@endif
</body>

</html>
