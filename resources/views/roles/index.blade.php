@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();
        $routes=["delete"=>route("delete-roles"),"update"=>route("update-roles"),"create"=>route("create-roles"),"store"=>route("write-roles")];
        $permissions=[
            'create'=>$user->hasAccess(['write-roles']),
            'edit'=>$user->hasAccess(['update-roles']),
            'delete'=>$user->hasAccess(['delete-roles'])
        ];
    @endphp
    <index-roles routes='@json($routes)' roles='@json($roles)' permissions='@json($permissions)'></index-roles>
    @endsection
