<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fumival</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>

        @guest
        @php
        $config=["home"=> url("/"),"appname"=>config("app.name", "Laravel"),"route_login"=> route("login") ];
        @endphp
        <div id="app">

            <navbar config='@json($config)'/>
        </div>
        @endguest
        @php


                   $config=["home"=> url("/"),"has_registered"=>Route::has("register"),"appname"=>config("app.name", "Laravel"),"route_login"=> route("login") ];

                   $auth=["user"=> Auth::user(),"route_logout"=> route("logout"),"menu"=>Session::get("menu")];
        @endphp

        <div id="app">
            <navbar data='@json($auth)' :logo='@json(Session::get("logo"))' config='@json($config)'></navbar>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

</body>

</html>
