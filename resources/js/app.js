import 'mdbvue/lib/css/mdb.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css';
import autofocus from 'vue-autofocus-directive';
import Vuex from 'vuex';
import Vue from 'vue';
import {store} from './store';
import navbar from './navbar.vue';
import dayjs from "dayjs";
import weekday from "dayjs/plugin/weekday";
import weekOfYear from "dayjs/plugin/weekOfYear";
import esUs from 'dayjs/locale/es-us';
dayjs.extend(weekday);
dayjs.extend(weekOfYear);
dayjs.locale(esUs);
window.Vue = require('vue');
Vue.directive('autofocus', autofocus);
//Vue.component('App', require('./App.vue').default);
Vue.component('index', require('./components/home/index.vue').default);
Vue.component('index-users', require('./components/users/index.vue').default);
Vue.component('index-glosas-evaluacion', require('./components/mantenedores/glosas-evaluacion/index.vue').default);
Vue.component('index-evaluacion', require('./components/mantenedores/valor-evaluacion/index.vue').default);
Vue.component('index-unidad-medida', require('./components/mantenedores/unidad-medida/index.vue').default);
Vue.component('index-conversion', require('./components/mantenedores/conversion/index.vue').default);
Vue.component('index-tipo-tratamiento', require('./components/mantenedores/tipo-tratamiento/index.vue').default);
Vue.component('index-bodega', require('./components/mantenedores/bodega/index.vue').default);
Vue.component('index-empresa', require('./components/mantenedores/empresa/index.vue').default);
Vue.component('index-tipo-cliente', require('./components/mantenedores/tipo_cliente/index.vue').default);
Vue.component('index-medio-pago', require('./components/mantenedores/medio_pago/index.vue').default);
Vue.component('index-trampas', require('./components/mantenedores/trampas/index.vue').default);
Vue.component('index-clientes', require('./components/clientes/index.vue').default);
Vue.component('index-productos', require('./components/productos/index.vue').default);
Vue.component('index-stock', require('./components/stock/index.vue').default);
Vue.component('index-roles', require('./components/roles/index.vue').default);
Vue.component('index-certificados', require('./components/certificados/index.vue').default);
Vue.component('index-inmuebles', require('./components/inmuebles/index.vue').default);
Vue.component('index-factura', require('./components/factura/index.vue').default);
Vue.component('index-calendario', require('./components/rutas/index.vue').default);
Vue.component('dashboard', require('./components/dashboard/index.vue').default);
const storeVuex = new Vuex.Store(store);

Vue.prototype.$firebaseServerKey="AAAAUjJ8hLA:APA91bFZ-pC1DDxc_mc22YkNWWgWFF7UDras_pQvEXN_QtrPwQPfSvHBFPcwlZ_hYBegqjgkF5xWYvrvN43Dm-PqLpj0t4BGo3uoGtiF40NCxVvXrHl2mXFpwYx2iCkwOPfF0vMCGYNY";
Vue.prototype.$GoogleMapsApiKey="AIzaSyAwe06tC00qDE-pY43LkmH2Filw97eckUs"
window.dayjs=dayjs;

new Vue({
    store:storeVuex,
    components:{
        navbar
    },
    el: '#app',
});

