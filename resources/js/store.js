import axios from 'axios';
export const store = {
    state: {
        evaluationCriteria:[],
        evaluations:[],
        Tratamientos:{
          show:false,
          payload:{}
        },
        TipoCliente:{
          show:false,
          payload:{}
        },
        MedioPago:{
          show:false,
          payload:{}
        },
        Trampas:{
          show:false,
          payload:{}
        },
        FactureCerts:[],
        createContact:{
            show:false,
            payload:{}
        },
        createFactura: {
            show: false,
            payload: {}
        },
        editFactura: {
            show: false,
            payload: {}
        },
        infoCertificado: {
            show: false,
            payload: {}
        },
        createCertificado:{
          show: false,
          payload: {}
        },
        editPerfil: {
            show: false,
            payload: {}
        },
        createPerfil: {
            show: false,
            payload: {}
        },
        editUser: {
            show: false,
            payload: {}
        },
        createUser: {
            show: false,
            payload: {}
        },
        editGlosaEvaluacion: {
            show: false,
            payload: {}
        },
        createGlosaEvaluacion: {
            show: false,
            payload: {}
        },
        createValorEvaluacion: {
            show: false,
            payload: {}
        },
        infoCliente:{
          show:false,
          payload:{}
        },
        editValorEvaluacion: {
            show: false,
            payload: {}
        },
        createConversion: {
            show: false,
            payload: {}
        },
        editConversion: {
            show: false,
            payload: {}
        },
        createTipoTratamiento: {
            show: false,
            payload: {}
        },
        editTipoTratamiento: {
            show: false,
            payload: {}
        },
        createEmpresa: {
            show: false,
            payload: {}
        },
        editEmpresa: {
            show: false,
            payload: {}
        },
        createProducto: {
            show: false,
            payload: {}
        },
        editProducto: {
            show: false,
            payload: {}
        },
        createPresets: {
            show: false,
            payload: {}
        },
        editPresets: {
            show: false,
            payload: {}
        },
        createUnidadMedida: {
            show: false,
            payload: {}
        },
        editUnidadMedida: {
            show: false,
            payload: {}
        },
        createClientes: {
            show: false,
            payload: {}
        },
        editClientes: {
            show: false,
            payload: {}
        },
        createProductos: {
            show: false,
            payload: {}
        },
        editProductos: {
            show: false,
            payload: {}
        },
        createBodega: {
            show: false,
            payload: {}
        },
        editBodega: {
            show: false,
            payload: {}
        },
        createStock: {
            show: false,
            payload: {}
        },
        editStock: {
            show: false,
            payload: {}
        },
        createStockUsuarios: {
            show: false,
            payload: {}
        },
        editStockUsuarios: {
            show: false,
            payload: {}
        },
        stockProductos: {
            data: [],
            selected: {}
        },
        createInmuebles: {
            show: false,
            payload: {}
        },
        editInmuebles: {
            show: false,
            payload: {}
        },

        componentePreset: {
            show: false,
            payload: {}
        },
        compuestosProducto: [],

        isValidRut: true,
        username: ""

    },
    mutations: {
        FactureCerts(state,target){
          state.FactureCerts=target;
        },
        createContact(state,target){
            state.createContact=target;
        },
        createTipoCliente(state,target){
            state.TipoCliente.payload=target;
            state.TipoCliente.show=true;
        },
        editTipoCliente(state,target){
            state.TipoCliente.payload=target;
            state.TipoCliente.show=true;
        },
        hideCreateTipoCliente(state,target){
            state.TipoCliente.payload=target;
          state.TipoCliente.show=false;
        },
        hideEditTipoCliente(state,target){
            state.TipoCliente.payload=target;
            state.TipoCliente.show=false;
        },
        createMedioPago(state,target){
          state.MedioPago.payload=target;
            state.MedioPago.show=true;


        },
        editMedioPago(state,target){
            state.MedioPago.payload=target;
            state.MedioPago.show=true;
        },
        hideMedioPago(state,target){
            state.MedioPago.payload=target;
            state.MedioPago.show=false;
        },
        createTrampas(state,target){
            state.Trampas.payload=target;
            state.Trampas.show=true;
        },
        hideCreateTrampas(state,target){
            state.Trampas.payload=target;
            state.Trampas.show=false;
        },
        editTrampas(state,target){
            state.Trampas.payload=target;
            state.Trampas.show=true;
        },
        hideEditTrampas(state,target){
            state.Trampas.payload=target;
            state.show=false;
        },
        setUser(state, target) {
            state.username = target
        },
        infoCertificado(state, target) {
            state.infoCertificado.payload = target;
            state.infoCertificado.show = true;
        },
        hideTratamientos(state){
          state.Tratamientos.show=false;
        },
        Tratamientos(state,target){
            state.Tratamientos=target;
        },
        hideInfoCertificado(state) {
            state.infoCertificado.show = false;
        },
        createCertificado(state,target){
          state.createCertificado.payload=target;
          state.createCertificado.show=true;
        },
        hideCreateCertificado(state) {
            state.createCertificado.show = false;
        },
        editPerfil(state, target) {
            state.editPerfil.payload = target;
            state.editPerfil.show = true;

        },
        setEvaluationCriteria(state,target){
          state.evaluationCriteria=target;
        },
        setEvaluations(state,target){
            state.evaluations=target;
        },
        createPerfil(state, target) {
            state.createPerfil.show = true;
            state.createPerfil.payload = target;
        },
        hideEditPerfil(state) {
            state.editPerfil.show = false;
        },
        hideCreatePerfil(state) {
            state.createPerfil.show = false;
        },
        editUser(state, target) {
            state.editUser.payload = target;
            state.editUser.show = true;
        },
        createUser(state, target) {
            state.createUser.payload = target;
            state.createUser.show = true;
        },
        hideEditUser(state) {
            state.editUser.show = false;
        },
        hideCreateUser(state) {
            state.createUser.show = false;
        },
        hideCreateGlosaEvaluacion(state) {
            state.createGlosaEvaluacion.show = false;
        },
        hideEditGlosaEvaluacion(state) {
            state.editGlosaEvaluacion.show = false;
        },
        editGlosaEvaluacion(state, target) {
            state.editGlosaEvaluacion.payload = target;
            state.editGlosaEvaluacion.show = true;
        },
        createGlosaEvaluacion(state, target) {
            state.createGlosaEvaluacion.payload = target;
            state.createGlosaEvaluacion.show = true;
        },
        createValorEvaluacion(state, target) {
            state.createValorEvaluacion.payload = target;
            state.createValorEvaluacion.show = true;
        },
        editValorEvaluacion(state, target) {
            state.editValorEvaluacion.payload = target;
            state.editValorEvaluacion.show = true;
        },
        hideCreateValorEvaluacion(state) {
            state.createValorEvaluacion.show = false;
        },
        hideEditValorEvaluacion(state) {
            state.editValorEvaluacion.show = false;
        },
        createUnidadMedida(state, target) {
            state.createUnidadMedida.payload = target;
            state.createUnidadMedida.show = true;
        },
        editUnidadMedida(state, target) {
            state.editUnidadMedida.payload = target;
            state.editUnidadMedida.show = true;
        },
        hideCreateUnidadMedida(state) {
            state.createUnidadMedida.show = false;
        },
        hideEditUnidadMedida(state) {
            state.editUnidadMedida.show = false;
        },
        createConversion(state, target) {
            state.createConversion.payload = target;
            state.createConversion.show = true;
        },
        editConversion(state, target) {
            state.editConversion.payload = target;
            state.editConversion.show = true;
        },
        hideCreateConversion(state) {
            state.createConversion.show = false;
        },
        hideEditConversion(state) {
            state.editConversion.show = false;
        },
        createTipoTratamiento(state, target) {
            state.createTipoTratamiento.payload = target;
            state.createTipoTratamiento.show = true;
        },
        editTipoTratamiento(state, target) {
            state.editTipoTratamiento.payload = target;
            state.editTipoTratamiento.show = true;
        },
        hideCreateTipoTratamiento(state) {
            state.createTipoTratamiento.show = false;
        },
        hideEditTipoTratamiento(state) {
            state.editTipoTratamiento.show = false;
        },
        createEmpresa(state, target) {
            state.createEmpresa.payload = target;
            state.createEmpresa.show = true;
        },
        editEmpresa(state, target) {
            state.editEmpresa.payload = target;
            state.editEmpresa.show = true;
        },
        hideCreateEmpresa(state) {
            state.createEmpresa.show = false;
        },
        hideEditEmpresa(state) {
            state.editEmpresa.show = false;
        },
        hideCreateClientes(state) {
            state.createClientes.show = false;
        },
        hideEditClientes(state) {
            state.editClientes.show = false;
        },
        createClientes(state, target) {
            state.createClientes.payload = target;
            state.createClientes.show = true;
        },
        editClientes(state, target) {
            state.editClientes.payload = target;
            state.editClientes.show = true;
        },
        hideCreateProductos(state) {
            state.createProductos.show = false;
        },
        hideEditProductos(state) {
            state.editProductos.show = false;
        },
        createProductos(state, target) {
            state.createProductos.payload = target;
            state.createProductos.show = true;
        },
        editProductos(state, target) {
            state.editProductos.payload = target;
            state.editProductos.show = true;
        },
        hideCreatePresets(state) {
            state.createPresets.show = false;
        },
        hideEditPresets(state) {
            state.editPresets.show = false;
        },
        createPresets(state, target) {
            state.createPresets.payload = target;
            state.createPresets.show = true;
        },
        editPresets(state, target) {
            state.editPresets.payload = target;
            state.editPresets.show = true;
        },
        createBodega(state, target) {
            state.createBodega.payload = target;
            state.createBodega.show = true;
        },
        hideCreateBodega(state) {
            state.createBodega.show = false;
        },
        editBodega(state, target) {
            state.editBodega.payload = target;
            state.editBodega.show = true;
        },
        hideEditBodega(state) {
            state.editBodega.show = false;
        },
        createStock(state, target) {
            state.createStock.payload = target;
            state.createStock.show = true;
        },
        hideCreateStock(state) {
            state.createStock.show = false;
        },
        editStock(state, target) {
            state.editStock.payload = target;
            state.editStock.show = true;
        },
        hideEditStock(state) {
            state.editStock.show = false;
        },
        saveStocks(state, data) {
            state.stockProductos.data = data;
        },
        createStockUsuarios(state, target) {
            state.createStockUsuarios.payload = target;
            state.createStockUsuarios.show = true;
        },
        hideCreateStockUsuarios(state) {
            state.createStockUsuarios.show = false;
        },
        editStockUsuarios(state, target) {
            state.editStockUsuarios.payload = target
            state.editStockUsuarios.show = true
        },
        hideEditStockUsuarios(state) {
            state.editStockUsuarios.show = false
        },
        createInmuebles(state, target) {
            state.createInmuebles.payload = target
            state.createInmuebles.show = true
        },
        hideCreateInmuebles(state) {
            state.createInmuebles.show = false
        },
        editInmuebles(state, target) {
            state.editInmuebles.payload = target
            state.editInmuebles.show = true
        },
        hideEditInmuebles(state) {
            state.editInmuebles.show = false
        },
        hideComponentePreset(state) {
            state.componentePreset.show = false
        },
        createComponentePreset(state, target) {
            state.componentePreset.show = true;
                state.componentePreset.payload = target;
        },
        editComponentePreset(state, target) {
            state.componentePreset.show = true;
            state.componentePreset.payload = target;
        },
        setCompuestosProducto(state, compuestos) {
            state.compuestosProducto = compuestos
        },
        createFactura(state, target) {
            state.createFactura.payload = target
            state.createFactura.show = true
        },
        infoCliente(state,target){
            state.infoCliente.payload=target;
            state.infoCliente.show=true;
        },
        hideInfoCliente(state,target){
            state.infoCliente.payload=target;
            state.infoCliente.show=false;
        },
        hideCreateFactura(state) {
            state.createFactura.show = false
            state.createFactura.payload={};
        },
        editFactura(state, target) {
            state.editFactura.payload = target
            state.editFactura.show = true
        },
        hideEditFactura(state) {
            state.editFactura.show = false
        },
        setRutValidation(state,target){
            state.isValidRut=target;
        }
    },
    actions: {
        validRut({commit},rut) {
            let valor = rut.replace(/\./g,'');
            valor = valor.replace('-','');
            const cuerpo = valor.slice(0,-1);
            let dv = valor.slice(-1).toUpperCase();
            rut = cuerpo + '-'+ dv
            if(undefined===dv)return false;
            if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rut )) return false;
            if(cuerpo.length < 7) return false;
            let suma = 0;
            let multiplo = 2;
            for(let i=1;i<=cuerpo.length;i++) {
                const index = multiplo * valor.charAt(cuerpo.length - i);
                suma = suma + index;
                if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
            }
            const dvEsperado = 11 - (suma % 11);
            dv = (dv === 'K')?10:dv;
            dv = (dv === '0')?11:dv;
            dv=Number.parseInt(dv);
             commit('setRutValidation',dvEsperado === dv);
        },
        async  onUpdateToken({state},payload) {
                await axios.post('/user/notifications/update-token',{
                    fcm_token:payload
                });
        },
       async printCert({state},data){
            const html2pdf = require('html2pdf.js');
            const filename=+ new Date()+'.pdf';
            const html=data.data;
           const WinPrint = window.open('', 'download', 'visible=none,toolbar=0,scrollbars=0,status=0,margin=0');
            WinPrint.document.write(html);
            const element=WinPrint.document.body;
            WinPrint.close();
               html2pdf().from(element).set({
                   margin:0,
                   filename:filename,
                   imageTimeout:15000,
                   html2canvas: { scale: 1,
                       allowTaint:true,
                       letterRendering: true ,imageTimeout:15000},
                   jsPDF: { unit: 'in', format: 'a4', orientation: 'portrait'},
                   image:{type: 'jpeg', quality: 0.95},
               }).toPdf().save();
           }


    }
};




