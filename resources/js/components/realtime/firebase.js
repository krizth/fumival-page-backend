import firebase from 'firebase/app'
import 'firebase/messaging';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyCqYLKD8Esj9PVNI7zh52h1pTqyK-jrYGM",
    authDomain: "fumival-dev.firebaseapp.com",
    databaseURL: "https://fumival-dev-default-rtdb.firebaseio.com",
    projectId: "fumival-dev",
    storageBucket: "fumival-dev.appspot.com",
    messagingSenderId: "353034339504",
    appId: "1:353034339504:web:1e3c3cbab6782ff4942775",
    measurementId: "G-4BQSMCZZ5H"
};
firebase.initializeApp(config);
if (!firebase.apps.length) {firebase.initializeApp(config);}
let messaging;
if (firebase.messaging.isSupported()) {
    messaging = firebase.messaging();
}
export default {
    messaging
}
