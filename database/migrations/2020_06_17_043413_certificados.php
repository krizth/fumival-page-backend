<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Certificados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificado', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('folio')->unique();
            $table->date('fecha');
            $table->bigInteger("id_cliente")->unsigned();
            $table->bigInteger("id_empresa")->unsigned();
            $table->bigInteger("id_usuario")->unsigned();
            $table->time("hora_inicio");
            $table->time("hora_termino");
            $table->bigInteger("valor")->unsigned()->nullable();
            $table->string("cond_pago",100)->nullable();
            $table->text("observaciones")->nullable();
            $table->string("latitud",100)->nullable();
            $table->string("longitud",100)->nullable();
            $table->json("empresa")->nullable();
            $table->json("tratamientos")->nullable();
            $table->json("cliente")->nullable();
            $table->json("usuario")->nullable();
            $table->json("imagenes")->nullable();
            $table->json("evaluaciones")->nullable();
            $table->json("trampas")->nullable();
            $table->json("inmueble")->nullable();
            $table->json("firmas")->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("id_empresa")->references("id")->on("empresa");
            $table->foreign("id_cliente")->references("id")->on("cliente");
            $table->foreign("id_usuario")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificado');
    }
}
