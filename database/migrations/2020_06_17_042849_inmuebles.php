<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Inmuebles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmuebles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_propietario')->unsigned();
            $table->string('nombre',200);
            $table->string('direccion',400);
            $table->string("ciudad",200)->nullable();
            $table->decimal('latitude',10,6)->nullable();
            $table->decimal('longitude',10,6)->nullable();
            $table->timestamps();
            $table->foreign("id_propietario")->references("id")->on("cliente")->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmuebles');
    }
}
