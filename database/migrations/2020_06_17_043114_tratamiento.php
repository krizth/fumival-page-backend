<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tratamiento', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_producto")->unsigned();
            $table->bigInteger("id_tipo_tratamiento")->unsigned();
            $table->string("dosis",200);
            $table->string("total",200);
            $table->string("concentracion",200);
            $table->string("lugares",400);
            $table->date("vigencia");
            $table->foreign("id_producto")->references("id")->on("producto");
            $table->foreign("id_tipo_tratamiento")->references("id")->on("tipo_tratamiento");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tratamiento');
    }
}
