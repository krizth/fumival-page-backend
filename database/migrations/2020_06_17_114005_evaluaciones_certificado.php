<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EvaluacionesCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluaciones_certificado', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_glosa")->unsigned();
            $table->bigInteger("id_evaluacion")->unsigned();
            $table->bigInteger("id_certificado")->unsigned();
            $table->foreign("id_glosa")->references("id")->on("glosa_evaluacion");
            $table->foreign("id_evaluacion")->references("id")->on("evaluacion");
            $table->foreign("id_certificado")->references("id")->on("certificado");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluaciones_certificado');
    }
}
