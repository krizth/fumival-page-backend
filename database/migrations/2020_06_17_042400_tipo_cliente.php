<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TipoCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_cliente', function (Blueprint $table) {
            $table->id();
            $table->boolean('restrict_payment')->default(0);
            $table->string('tipo',200);
            $table->bigInteger('impuesto')->default(0);
            $table->string('color',200);
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_cliente');
    }
}
