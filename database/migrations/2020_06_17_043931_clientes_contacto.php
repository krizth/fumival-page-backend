<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClientesContacto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_contactos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_cliente")->unsigned();
            $table->bigInteger("id_contacto")->unsigned();
            $table->boolean("predeterminado")->default(false);
            $table->foreign("id_cliente")->references("id")->on("cliente");
            $table->foreign("id_contacto")->references("id")->on("contactos");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes_contacto');
    }
}
