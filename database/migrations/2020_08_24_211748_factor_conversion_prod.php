<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FactorConversionProd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factor_conversion_prod', function (Blueprint $table) {
            $table->bigInteger("id_producto")->unsigned();
            $table->bigInteger("id_factor_conversion")->unsigned();
            $table->foreign("id_producto")->references("id")->on("producto");
            $table->foreign("id_factor_conversion")->references("id")->on("factor_conversion")->cascadeOnDelete();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factor_conversion_prod');
    }
}
