<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_unidad_minima")->unsigned();
            $table->string("nombre",300);
            $table->string("codigo",50)->unique()->nullable();
            $table->integer("precio_compra")->nullable()->unsigned();
            $table->integer("precio_distribucion")->nullable()->unsigned();
            $table->string("ing_activo",200);
            $table->tinyInteger("compuesto")->nullable()->default(0);
            $table->string("regISP")->nullable();
            $table->string("marca",255)->nullable();
            $table->tinyInteger("maquinaria")->default(0);
            $table->text("descripcion")->nullable();
            $table->string("ubicacion",255)->nullable();
            $table->foreign("id_unidad_minima")->references("id")->on("unidad_medida");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');
    }
}
