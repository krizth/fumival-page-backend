<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Trampas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trampas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tratamiento_id')->unsigned()->nullable();
            $table->string("nombre",255);
            $table->json("campos");
            $table->boolean("georeferenciada")->default(false);
            $table->foreign('tratamiento_id')->references('id')->on('tipo_tratamiento')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trampas');
    }
}
