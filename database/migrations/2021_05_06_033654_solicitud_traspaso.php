<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SolicitudTraspaso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_traspaso', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_autorizador')->unsigned();
            $table->bigInteger('id_solicitante')->unsigned();
            $table->bigInteger('id_producto')->unsigned();
            $table->bigInteger('cantidad')->unsigned();
            $table->bigInteger('id_unidad_medida')->unsigned();
            $table->boolean('minima')->default(false);
            $table->bigInteger('id_estado')->unsigned();
            $table->string("token");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trampas');
    }
}
