<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MovimientosStockUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos_stock_usuarios', function (Blueprint $table) {
            $table->bigInteger("id_movimiento")->unsigned();
            $table->bigInteger("id_usuario_destino")->unsigned();
            $table->bigInteger("id_stock_usuarios")->unsigned();
            $table->timestamps();
            $table->foreign("id_movimiento")->references("id")->on("movimientos");
            $table->foreign("id_usuario_destino")->references("id")->on("users");
            $table->foreign("id_stock_usuarios")->references("id")->on("stock_usuarios");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos_stock_usuarios');
    }
}
