<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FactorConversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factor_conversion', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_unidad_medida_origen")->unsigned();
            $table->bigInteger("factor_conversion")->unsigned();
            $table->bigInteger("id_unidad_medida_destino")->unsigned();
            $table->tinyInteger("forProduct")->default(0);
            $table->string("descripcion",200)->nullable();
            $table->foreign("id_unidad_medida_origen")->references("id")->on("unidad_medida");
            $table->foreign("id_unidad_medida_destino")->references("id")->on("unidad_medida");


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factor_conversion');
    }
}
