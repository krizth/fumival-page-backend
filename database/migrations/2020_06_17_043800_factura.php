<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Factura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura', function (Blueprint $table) {
            $table->id();
            //$table->bigInteger("id_certificado")->unsigned();
            $table->string("num_factura",20);
            $table->bigInteger("iva")->unsigned()->nullable();
            $table->bigInteger("total")->unsigned()->nullable();
            $table->date("fecha")->nullable();
            $table->timestamps();
            //$table->foreign("id_certificado")->references("id")->on("certificado");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura');
    }
}
