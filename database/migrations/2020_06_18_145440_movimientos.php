<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Movimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_producto")->unsigned();
            $table->bigInteger("id_usuario_origen")->unsigned();
            $table->bigInteger("id_motivo")->unsigned();
            $table->bigInteger("recuento");
            $table->string("lote",100)->nullable();
            $table->timestamps();
            $table->foreign("id_producto")->references("id")->on("producto");
            $table->foreign("id_motivo")->references("id")->on("motivo_movimiento");
            $table->foreign("id_usuario_origen")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
