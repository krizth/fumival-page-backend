<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InmueblesClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmuebles_cliente', function (Blueprint $table) {
            $table->bigInteger("id_inmuebles")->unsigned();
            $table->bigInteger("id_cliente")->unsigned();
            $table->foreign("id_cliente")->references("id")->on("cliente")->onDelete('no action')->onUpdate('no action');
            $table->foreign("id_inmuebles")->references("id")->on("inmuebles")->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmuebles_clientes');
    }
}
