<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CertificadosTratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificado_tratamiento', function (Blueprint $table) {
            $table->bigInteger("id_tratamiento")->unsigned();
            $table->bigInteger("id_certificado")->unsigned();
            $table->foreign("id_certificado")->references("id")->on("certificado");
            $table->foreign("id_tratamiento")->references("id")->on("tratamiento");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificado_tratamiento');
    }
}
