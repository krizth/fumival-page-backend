<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Empresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('repLegal',250);
            $table->string("rutRepLegal",15);
            $table->string('rut')->unique();
            $table->string("direccion",300);
            $table->string("correo",100);
            $table->string("fono",20);
            $table->string("celular",20);
            $table->json('location')->nullable();
            $table->string("logo",300)->nullable();
            $table->string("pagina_web",100);
            $table->text("otros");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
