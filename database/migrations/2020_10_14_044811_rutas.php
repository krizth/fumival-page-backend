<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rutas extends Migration
{

    public function up()
    {
        Schema::create('rutas', function (Blueprint $table){
            $table->id();
            $table->string('codigo',200)->nullable();
            $table->bigInteger("id_usuario")->unsigned();
            $table->bigInteger("id_cliente")->unsigned();
            $table->bigInteger("id_inmueble")->unsigned();
            $table->bigInteger("id_motivo_visita")->unsigned();
            $table->datetime("horario_inicio")->nullable();
            $table->datetime("horario_fin")->nullable();
            $table->datetime("horario_llegada")->nullable();
            $table->bigInteger("id_estado")->unsigned()->nullable();
            $table->bigInteger("frecuency")->unsigned()->nullable();
            $table->string("comentario")->nullable();
            $table->timestamps();
            $table->foreign("id_cliente")->references("id")->on("cliente")->onDelete('no action')->onUpdate('no action');
            $table->foreign("id_inmueble")->references("id")->on("inmuebles")->onDelete('cascade')->onUpdate('no action');
            $table->foreign("id_usuario")->references("id")->on("users")->onDelete('no action')->onUpdate('no action');
            $table->foreign("id_motivo_visita")->references("id")->on("motivo_visita")->onDelete('no action')->onUpdate('no action');
            $table->foreign("id_estado")->references("id")->on("estado_ruta")->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rutas');
    }
}

