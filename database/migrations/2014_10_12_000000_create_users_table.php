<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('email');
            $table->boolean('isAdmin')->default(false);
            $table->string("rut")->unique();
            $table->string("patente",10)->unique()->nullable();
            $table->longText("firma")->nullable();
            $table->string('password');
            $table->bigInteger("tec_a_cargo")->unsigned()->nullable();
            $table->json("location")->nullable();
            $table->json("car_location")->nullable();
            $table->string('fcm_token',255)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('tec_a_cargo')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
