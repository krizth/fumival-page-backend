<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Presets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preset', function (Blueprint $table) {
            $table->bigInteger('id_producto')->unsigned();
            $table->bigInteger('id_compuesto')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->timestamps();
            $table->foreign("id_producto")->references("id")->on("producto");
            $table->foreign("id_compuesto")->references("id")->on("producto");
            $table->primary(['id_producto','id_compuesto']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preset');
    }
}
