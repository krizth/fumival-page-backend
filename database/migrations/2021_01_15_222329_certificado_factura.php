<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CertificadoFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificado_factura', function (Blueprint $table) {
            $table->bigInteger('id_certificado')->unsigned();
            $table->bigInteger('id_factura')->unsigned();
            $table->primary(['id_certificado', 'id_factura']);
            $table->foreign('id_certificado')->references('id')->on('certificado');
            $table->foreign('id_factura')->references('id')->on('factura');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificado_factura');
    }
}
