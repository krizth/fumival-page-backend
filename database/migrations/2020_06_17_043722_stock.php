<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Stock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("id_producto")->unsigned();
            $table->bigInteger("id_bodega")->unsigned();
            $table->bigInteger("cantidad")->unsigned();
            $table->string("lote",100)->nullable();
            $table->date("caducidad")->nullable();
            $table->timestamps();
            $table->foreign("id_producto")->references("id")->on("producto");
            $table->foreign("id_bodega")->references("id")->on("bodega");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
