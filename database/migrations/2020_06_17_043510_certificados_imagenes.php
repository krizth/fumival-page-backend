<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CertificadosImagenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificado_imagenes', function (Blueprint $table) {
            $table->bigInteger("id_certificado")->unsigned();
            $table->bigInteger("id_imagen")->unsigned();
            $table->foreign("id_certificado")->references("id")->on("certificado");
            $table->foreign("id_imagen")->references("id")->on("imagen");
            //$table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificado_imagenes');
    }
}
