<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->id();
            $table->string("nombre",400);
            $table->unsignedBigInteger('id_tipo_cliente')->nullable();
            $table->string("nombre_fantasia",400);
            $table->string("ciudad")->nullable();
            $table->string("rut",15)->unique();
            $table->string("direccion_principal",400);
            $table->string("fono",14)->nullable();
            $table->string("email",55)->nullable();
            $table->foreign("id_tipo_cliente")->references("id")->on("tipo_cliente")->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');

    }
}
