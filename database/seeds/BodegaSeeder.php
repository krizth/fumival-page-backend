<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BodegaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bodega')->insert([
            ["nombre" => "Bodega A"],
            ["nombre" => "Bodega B"]
        ]);
    }
}
