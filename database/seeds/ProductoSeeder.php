<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('producto')->insert([
            [
                "id_unidad_minima" => 1,
                "nombre" => "Alcohol Gel",
                "codigo" => 123,
                "precio_compra" => 2000,
                "precio_distribucion" => 8000,
                "ing_activo" => "S/A",
                "regISP" => "S/R",
                "maquinaria" => 0
            ]
        ]);
    }
}
