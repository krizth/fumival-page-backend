<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Configuracion::create(['iva'=>0.19,'default_permissions'=>
            [
                "read-users"=>["allowed"=>true,"descripcion"=>"Leer todos los usuarios del sistema"],
                "write-users"=>["allowed"=>true,"descripcion"=>"Actualizar o crear usuarios"],
                "update-users"=>["allowed"=>true,"descripcion"=>"Actualizar usuarios"],
                "delete-users"=>["allowed"=>true,"descripcion"=>"Eliminar usuarios"],
                "read-roles"=>["allowed"=>true,"descripcion"=>"Leer todos los roles"],
                "write-roles"=>["allowed"=>true,"descripcion"=>"Actualizar roles"],
                "update-roles"=>["allowed"=>true,"descripcion"=>"Actualizar o crear roles"],
                "delete-roles"=>["allowed"=>true,"descripcion"=>"Eliminar roles"],
                "assign-roles"=>["allowed"=>true,"descripcion"=>"Asignar rol a usuario"],
                "unassign-roles"=>["allowed"=>true,"descripcion"=>"Quitar rol a usuario"],
                "write-clientes"=>["allowed"=>true,"descripcion"=>"Crear clientes"],
                "update-clientes"=>["allowed"=>true,"descripcion"=>"Actualiza clientes"],
                "read-clientes"=>["allowed"=>true,"descripcion"=>"Leer clientes"],
                "delete-clientes"=>["allowed"=>true,"descripcion"=>"Eliminar clientes"],
                "read-own-certificado"=>["allowed"=>true,"descripcion"=>"El usuario puede ver sus certificados"],
                "read-certificado"=>["allowed"=>true,"descripcion"=>"Crear certificados"],
                "delete-certificado"=>["allowed"=>true,"descripcion"=>"Eliminar certificados"],
                "write-certificado"=>["allowed"=>true,"descripcion"=>"Crear certificados"],
                "update-certificado"=>["allowed"=>"false","descripcion"=>"Editar certificados"],
                "read-facturas"=>["allowed"=>true,"descripcion"=>"Leer todas las facturas del sistema"],
                "write-facturas"=>["allowed"=>true,"descripcion"=>"Crear facturas"],
                "update-facturas"=>["allowed"=>true,"descripcion"=>"Actualizar o crear facturas"],
                "delete-facturas"=>["allowed"=>true,"descripcion"=>"Eliminar facturas del sistema"],
                "assign-facturas"=>["allowed"=>true,"descripcion"=>"Asignar facturas a certificados"],
                "unassign-facturas"=>["allowed"=>true,"descripcion"=>"Quitar facturas asignadas a certificados"],
                "write-tipo_inmueble"=>["allowed"=>true,"descripcion"=>"Actualizar o crear tipo de inmueble"],
                "update-tipo_inmueble"=>["allowed"=>true,"descripcion"=>"Actualizar tipo de inmueble"],
                "delete-tipo_inmueble"=>["allowed"=>true,"descripcion"=>"Eliminar tipo de inmueble"],
                "assign-inmuebles"=>["allowed"=>true,"descripcion"=>"Asignar inmueble a cliente"],
                "unassign-inmuebles"=>["allowed"=>true,"descripcion"=>"Quitar inmueble asignado a cliente"],
                "read-stock"=>["allowed"=>true,"descripcion"=>"Leer stock disponible"],
                "write-stock"=>["allowed"=>true,"descripcion"=>"Crear stock"],
                "update-stock"=>["allowed"=>true,"descripcion"=>"Actualizar stock"],
                "delete-stock"=>["allowed"=>true,"descripcion"=>"Eliminar stock"],
                "assign-stock"=>["allowed"=>true,"descripcion"=>"Asignar stock"],
                "unassign-stock"=>["allowed"=>true,"descripcion"=>"Quitar stock"],
                "write-stock_usuarios"=>["allowed"=>true,"descripcion"=>"Crear stock de usuarios"],
                "delete-stock_usuarios" => ["allowed" => true, "descripcion" => "Elimina stock de usuarios"],
                "update-stock_usuarios"=>["allowed"=>true,"descripcion"=>"Actualizar stock de usuarios"],
                "read-stock_usuarios"=>["allowed"=>true,"descripcion"=>"Leer stock de usuarios"],
                "assign-stock_usuarios"=>["allowed"=>true,"descripcion"=>"Asignar stock de usuarios"],
                "unassign-stock_usuarios"=>["allowed"=>true,"descripcion"=>"Quitar stock de usuarios"],
                "read-stock_usuario"=>["allowed"=>true,"descripcion"=>"Usuario puede ver su stock"],
                "write-stock_usuario"=>["allowed"=>true,"descripcion"=>"Usuario puede actualizar su stock"],
                "update-stock_usuario"=>["allowed"=>true,"descripcion"=>"Usuario puede actualizar su stock"],
                "read-productos"=>["allowed"=>true,"descripcion"=>"Leer productos"],
                "write-productos"=>["allowed"=>true,"descripcion"=>"Crear un producto"],
                "update-productos" => ["allowed" => true, "descripcion" => "Actualizar un producto"],
                "delete-productos"=>["allowed"=>true,"descripcion"=>"Eliminar un producto"],
                "write-preset" => ["allowed" => true, "descripcion" => "Crear un preset"],
                "update-preset" => ["allowed" => true, "descripcion" => "Actualizar un preset"],
                "delete-preset" => ["allowed" => true, "descripcion" => "Eliminar un preset"],
                "read-inmuebles" => ["allowed" => true, "descripcion" => "Leer Inmuebles"],
                "write-inmuebles" => ["allowed" => true, "descripcion" => "Crear un inmueble"],
                "delete-inmuebles" => ["allowed" => true, "descripcion" => "Eliminar un inmueble"],
                "update-inmuebles" => ["allowed" => true, "descripcion" => "Actualiza un inmueble"],
                "read-unidad_medida"=>["allowed"=>true,"descripcion"=>"Leer unidades de medida"],
                "write-unidad_medida"=>["allowed"=>true,"descripcion"=>"Crear unidad de medida"],
                "update-unidad_medida"=>["allowed"=>true,"descripcion"=>"Actualiza unidad de medida"],
                "delete-unidad_medida"=>["allowed"=>true,"descripcion"=>"Eliminar unidad de medida"],
                "read-glosa_evaluacion"=>["allowed"=>true,"descripcion"=>"Leer glosas de evaluacion"],
                "write-glosa_evaluacion"=>["allowed"=>true,"descripcion"=>"Crear glosa de evaluacion"],
                "update-glosa_evaluacion" => ["allowed" => true, "descripcion" => "Actualiza glosa de evaluacion"],
                "delete-glosa_evaluacion"=>["allowed"=>true,"descripcion"=>"Eliminar glosa de evaluacion"],
                "read-evaluacion"=>["allowed"=>true,"descripcion"=>"Leer Evaluaciones"],
                "delete-evaluacion"=>["allowed"=>true,"descripcion"=>"Eliminar una evaluacion textual"],
                "update-evaluacion"=>["allowed"=>true,"descripcion"=>"Actualiza una evaluacion textual"],
                "write-evaluacion"=>["allowed"=>true,"descripcion"=>"Crear una evaluacion textual"],
                "assing-factor_conversion" =>["allowed"=>true,"descripcion"=>"Asigna un factor de conversion para un producto"],
                "write-factor_conversion"=>["allowed"=>true,"descripcion"=>"Crear un factor de conversion"],
                "read-factor_conversion"=>["allowed"=>true,"descripcion"=>"Leer factores de conversion"],
                "update-factor_conversion"=>["allowed"=>true,"descripcion"=>"Actualiza factores de conversion"],
                "delete-factor_conversion"=>["allowed"=>true,"descripcion"=>"Eliminar factores de conversion"],
                "write-tipo_contacto"=>["allowed"=>true,"descripcion"=>"Crear un tipo de contacto"],
                "write-tipo_tratamiento"=>["allowed"=>true,"descripcion"=>"Crear un tipo de tratamiento"],
                "read-tipo_tratamiento"=>["allowed"=>true,"descripcion"=>"Leer tipos de tratamiento"],
                "update-tipo_tratamiento"=>["allowed"=>true,"descripcion"=>"Actualizar tipos de tratamiento"],
                "delete-tipo_tratamiento"=>["allowed"=>true,"descripcion"=>"Eliminar tipos de tratamiento"],
                "write-empresa"=>["allowed"=>true,"descripcion"=>"Crear empresa"],
                "read-empresa"=>["allowed"=>true,"descripcion"=>"Leer empresa"],
                "update-empresa"=>["allowed"=>true,"descripcion"=>"Actualiza empresa"],
                "delete-empresa"=>["allowed"=>true,"descripcion"=>"Eliminar empresa"],
                "unassign-factor_conversion"=>["allowed"=>true,"descripcion"=>"Quitar factor de conversion a un producto"],
                "write-bodega"=>["allowed"=>true,"descripcion"=>"Crear bodega"],
                "read-bodega"=>["allowed"=>true,"descripcion"=>"Leer bodega"],
                "update-bodega"=>["allowed"=>true,"descripcion"=>"Actualiza bodega"],
                "delete-bodega"=>["allowed"=>true,"descripcion"=>"Eliminar empresa"],
                "read-trampas"=>["allowed"=>true,"descripcion"=>"Leer tipos de trampa"],
                "write-trampas"=>["allowed"=>true,"descripcion"=>"Crear tipos de trampa"],
                "update-trampas"=>["allowed"=>true,"descripcion"=>"Actualiza tipos de trampas"],
                "delete-trampas"=>["allowed"=>true,"descripcion"=>"Eliminar tipos de trampas"],
                "read-tipo_cliente"=>["allowed"=>true,"descripcion"=>"Leer tipos de cliente"],
                "write-tipo_cliente"=>["allowed"=>true,"descripcion"=>"Crear tipos de cliente"],
                "update-tipo_cliente"=>["allowed"=>true,"descripcion"=>"Actualiza tipos de cliente"],
                "delete-tipo_cliente"=>["allowed"=>true,"descripcion"=>"Eliminar tipos de cliente"],
                "read-medio_pago"=>["allowed"=>true,"descripcion"=>"Leer medios de pago"],
                "write-medio_pago"=>["allowed"=>true,"descripcion"=>"Crear medios de pago"],
                "update-medio_pago"=>["allowed"=>true,"descripcion"=>"Actualiza medios de pago"],
                "delete-medio_pago"=>["allowed"=>true,"descripcion"=>"Eliminar medios de pago"],
                "read-rutas"=>["allowed"=>true,"descripcion"=>"Leer Rutas"],
                "update-rutas"=>["allowed"=>true,"descripcion"=>"Actualizar Rutas"],
                "delete-rutas"=>["allowed"=>true,"descripcion"=>"Eliminar Rutas"],
                "receive-cert-notifications"=>["allowed"=>true,"descripcion"=>"Recibir notificaciones de certificados creados"]
            ],'menus'=>[
            "Usuarios"=>"read-users",
            "Roles"=>"read-roles",
            "Clientes"=>"read-clientes",
            "Inmuebles"=>"read-inmuebles",
            "Certificados"=>"read-certificado",
            "Rutas"=>"read-rutas",
            "Facturas"=>"read-facturas",
            "Glosas de evaluacion"=>"read-glosa_evaluacion",
            "Unidad de Medida"=>"read-unidad_medida",
            "Conversiones"=>"read-factor_conversion",
            "Valor de evaluaciones"=>"read-evaluacion",
            "Tipos de tratamiento"=>"read-tipo_tratamiento",
            "Datos de empresa"=>"read-empresa",
            "Bodega"=>"read-bodega",
            "Productos"=>"read-productos",
            "Stock"=>"read-stock",
            "Trampas"=>"read-trampas",
            "Tipos de Cliente"=>"read-tipo_cliente",
            "Medios de pago"=>"read-medio_pago"
        ]
        ]);
        $config=App\Models\Configuracion::first();
       $user= User::create([
            "nombre"=>"Jaime Céspedes Honorato",
            "email"=>"fumival@gmail.com",
            "isAdmin"=>1,
            "rut"=>"10772554-7",
            "password"=>Hash::make("passw0rd"),

        ]);
        $Admin=Role::create([
            "role_name"=>"Administrador",
            "slug"=>"admin",
            "permissions"=>$config['default_permissions'],
        ]);
        $user->roles()->attach($Admin->id);

    }
}
