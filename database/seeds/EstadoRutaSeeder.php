<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\EstadoRuta;
class EstadoRutaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_ruta')->insert([
           ["id"=>EstadoRuta::$VIGENTE,'estado'=>'Vigente'],
            ['id'=>EstadoRuta::$REALIZADA,'estado'=>'Realizada'],
            ['id'=>EstadoRuta::$EN_TRAMITE,'estado'=>'En Tramite'],
            ['id'=>EstadoRuta::$CANCELADA,'estado'=>'Cancelada'],
        ]);
    }
}
