<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
        $this->call(EvaluacionSeeder::class);
        $this->call(GlosaEvaluacionSeeder::class);
        $this->call(TipoTratamientoSeeder::class);
        $this->call(UnidadMedidaSeeder::class);
        $this->call(TipoContactoSeeder::class);
        $this->call(ClienteSeeder::class);
        $this->call(EstadoRutaSeeder::class);
        $this->call(MotivoVisitaSeeder::class);
        $this->call(ProductoSeeder::class);
        $this->call(BodegaSeeder::class);
        $this->call(MotivoMovimientoSeeder::class);
        $this->call(EmpresaSeeder::class);
        $this->call(tipoClienteSeeder::class);
    }
}
