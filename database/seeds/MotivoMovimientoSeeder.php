<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\MotivoMovimiento as motivos;
class MotivoMovimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {

        DB::table("motivo_movimiento")->insert([
            ["motivo"=>"Ajuste","id"=>motivos::$AJUSTE],
            ["motivo"=>"Prestamo","id"=>motivos::$PRESTAMO],
            ["motivo"=>"Traspaso","id"=>motivos::$TRASPASO],
            ["motivo"=>"Aplicacion","id"=>motivos::$APLICACION],
            ["motivo"=>"Descomposicion","id"=>motivos::$DESCOMPOSICION],
            ["motivo"=>"Perdida","id"=>motivos::$PERDIDA],
            ["motivo"=>"Devolucion","id"=>motivos::$DEVOLUCION],
            ["motivo"=>"Ingreso","id"=>motivos::$INGRESO]
        ]);
    }
}
