<?php

use Illuminate\Database\Seeder;
use App\Models\TipoCliente;

class tipoClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       TipoCliente::insert([
       ["id"=>TipoCliente::$PERSONA_NATURAL,"restrict_payment"=>1,"tipo"=>"Persona Natural","color"=>"black","impuesto"=>14],
           ["id"=>TipoCliente::$EMPRESA,"restrict_payment"=>0,"tipo"=>"Empresa","color"=>"green","impuesto"=>10],
       ]);
    }
}
