<?php

use Illuminate\Database\Seeder;

class UnidadMedidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\UnidadMedida::insert([
            ["nombre"=>"Litros","abreviatura"=>"Lt"],
            ["nombre"=>"Gramos","abreviatura"=>"Gr"],
            ["nombre"=>"Kilogramos","abreviatura"=>"Kg"]
        ]);
    }
}
