<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MotivoVisitaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('motivo_visita')->insert([
            ['motivo'=>'Cotizacion','id'=>\App\Models\MotivoVisita::$COTIZACION],
            ['motivo'=>'Servicio','id'=>\App\Models\MotivoVisita::$SERVICIO],
            ['motivo'=>'Distribucion','id'=>\App\Models\MotivoVisita::$DISTRIBUCION]
        ]);


    }
}
