<?php

use Illuminate\Database\Seeder;

class GlosaEvaluacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\GlosaEvaluacion::insert([
            ["Criterio"=>"Areas Verdes"],
            ["Criterio"=>"Manejo de Basura"]
        ]);
    }
}
