<?php

use Illuminate\Database\Seeder;

class EvaluacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Evaluacion::insert([
           ["evaluacion_textual"=>"Bueno","peso"=>3],
            ["evaluacion_textual"=>"Regular","peso"=>2],
            ["evaluacion_textual"=>"Malo","peso"=>1],
        ]);
    }
}
