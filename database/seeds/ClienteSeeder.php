<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cliente')->insert([
            ['nombre'=>"Claudio Cantero",
            'nombre_fantasia'=>"AKC Producciones",
            'rut'=>'8489413-3',
            'direccion_principal'=>'Poblacion Marina Mercante Numero 10'],
            ['nombre'=>"Gabriel Cantero",
                'nombre_fantasia'=>"WorkFlow Solutions SPA",
                'rut'=>'20124578-8',
                'direccion_principal'=>'Poblacion Marina Mercante Numero 11']
        ]);
        DB::table('contactos')->insert([
           ['contacto' =>'+56948950090',
            'id_tipo_contacto'=>2],
            ['contacto'=>'632471634',
                'id_tipo_contacto'=>2]
        ]);
        DB::table('cliente_contactos')->insert([
            ['id_cliente'=>1,"id_contacto"=>1],
            ['id_cliente'=>2,"id_contacto"=>2]
        ]);
        DB::table('inmuebles')->insert([
            ['nombre'=>'Casa del sonido 1',
                'direccion'=>"Poblacion marina mercante numero 10",
                'ciudad'=>'Valdivia','latitude'=>-39.890330,'longitude'=>-73.433870,'id_propietario'=>1],
            ['nombre'=>'Casa del sonido 2',
                'direccion'=>"Poblacion marina mercante numero 11",
                'ciudad'=>'Valdivia','latitude'=>-39.890338,'longitude'=>-73.433875,'id_propietario'=>1],
            ['nombre'=>'Casa del sonido 3',
                'direccion'=>"Poblacion marina mercante numero 12",
               'ciudad'=>'Valdivia','latitude'=>-39.890342,'longitude'=>-73.433881,'id_propietario'=>1],
            ]);

        DB::table('inmuebles_cliente')->insert([
            ['id_inmuebles'=>1,'id_cliente'=>1],
            ['id_inmuebles'=>2,'id_cliente'=>1],
            ['id_inmuebles'=>3,'id_cliente'=>2]
        ]);
        DB::table('medio_pago')->insert([
            ['id'=>1,'medio'=>"Efectivo"],
            ['id'=>2,'medio'=>"Cheque"],
            ['id'=>3,'medio'=>"Transferencia"],
        ]);
        DB::table('trampas')->insert([
            'campos'=>json_encode([
                ["name"=>"etiqueta","type"=>'text',"label"=>"Etiqueta"],
                ["name"=>"uid","type"=>"text","disabled"=>true],
            ])
        ]);

    }
}
