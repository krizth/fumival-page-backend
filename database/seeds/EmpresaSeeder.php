<?php

use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresa')->insert([
            'nombre'=>'FUMIVAL',
            'rut'=>'76.052.804-8',
            'repLegal'=>"Jaime Céspedes Honorato",
            "rutRepLegal"=>"10.772.554-7",
            "direccion"=>"Errazuriz 1783",
            "fono"=>"632 521 691",
            "celular"=>"950 00 77 06",
            "correo"=>"consultas@fumival.cl",
            "pagina_web"=>"www.fumival.cl",
            "logo"=>"images/xSGRmNHBCqGPW2JFwp9TiGPsBDJAyGRtS6CdFcal.png",
            "otros"=>"Res. N° 1567 MINSAL, Valdivia, 28/07/10",
            "location"=>json_encode(["lat"=> -39.8276959892902, "lng"=> -73.22451036456516])
        ]);
    }
}
