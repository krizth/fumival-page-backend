<?php

use Illuminate\Database\Seeder;

class TipoTratamientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \App\Models\TipoTratamiento::create([
           "nombre"=>"Desratizacion",
           "abreviatura"=>"DR",
       ]);
        \App\Models\TipoTratamiento::create([
            "nombre"=>"Desinsectacion",
            "abreviatura"=>"DS",
            "comentario_fijo"=>"NO INGRESAR AL LUGAR ANTES DE 2 HORAS DE TERMINADO EL SERVICIO PREVIA VENTILACION DE 45 MINUTOS"
        ]);
        \App\Models\TipoTratamiento::create([
            "nombre"=>"Sanitizacion",
            "abreviatura"=>"SN",
            "comentario_fijo"=>"NO INGRESAR AL LUGAR ANTES DE 1 HORA DE TERMINADO EL SERVICIO PREVIA VENTILACION DE 45 MINUTOS"
        ]);

    }
}
