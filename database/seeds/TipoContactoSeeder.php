<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TipoContactoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_contacto')->insert([
           ['id'=>\App\Models\TipoContacto::$EMAIL,'descripcion'=>"E-mail"],
        ['id'=>\App\Models\TipoContacto::$FONO,'descripcion'=>"Fono"]
        ]);
    }
}
